DROP TABLE IF EXISTS account_packages CASCADE;

CREATE TABLE account_packages
(
  id serial NOT NULL,
  name character varying(200) NOT NULL,
  description character varying(200),
  createddate timestamp without time zone DEFAULT now(),
  displayorder integer,
  CONSTRAINT acccount_packages_pkey PRIMARY KEY (id),
  CONSTRAINT acccount_packages_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_packages
  OWNER TO postgres;
