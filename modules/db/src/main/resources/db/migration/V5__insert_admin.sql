INSERT INTO roles (name, description) VALUES ('Admin', 'Admin');

INSERT INTO role_rights (roleId, rightName) VALUES ((SELECT
                                                       id
                                                     FROM roles
                                                     WHERE name = 'Admin'), 'MANAGE_ROLES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'ADMINISTRATION'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_USERS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_ACCOUNT_PACKAGES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_CLIENT_TYPES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_RIGHTS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_GEO_LEVELS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_GEO_ZONES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_REPORTS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_SMS'),
  ((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'SYSTEM_SETTINGS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_EXTENSIONS');

INSERT INTO users (userName, password, facilityId, firstName, lastName, email, verified, active, restrictLogin) VALUES
('Admin123', 'TQskzK3iiLfbRVHeM1muvBCiiKriibfl6lh8ipo91hb74G3OvsybvkzpPI4S3KIeWTXAiiwlUU0iiSxWii4wSuS8mokSAieie', null, 'John', 'Doe', 'John_Doe@openlmis.com', TRUE, TRUE, FALSE);

INSERT INTO role_assignments (userId, roleId) VALUES
((SELECT
    id
  FROM users
  WHERE userName = 'Admin123'), (SELECT
                                   id
                                 FROM roles
                                 WHERE name = 'Admin'));



