INSERT INTO rights (name, rightType, description) VALUES

('MANAGE_ROLES', 'ADMIN', 'Permission to create and edit roles in the system'),
('ADMINISTRATION', 'ADMIN', 'Permission to create and edit schedules in the system'),
('MANAGE_USERS', 'ADMIN', 'Permission to create and view users'),
('MANAGE_REPORTS', 'ADMIN', 'Permission to create and view users'),
('MANAGE_ACCOUNT_PACKAGES', 'ADMIN', 'Permission to create and view users'),
('MANAGE_CLIENT_TYPES', 'ADMIN', 'Permission to manage reports'),
('MANAGE_RIGHTS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_GEO_LEVELS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_GEO_ZONES', 'ADMIN', 'Permission to manage reports'),
('MANAGE_SMS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_EXTENSIONS', 'ADMIN', 'Permission to manage reports'),
('SYSTEM_SETTINGS', 'ADMIN', 'Permission to configure Electronic Data Interchange (EDI)')
;
