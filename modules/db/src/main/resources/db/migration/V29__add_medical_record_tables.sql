﻿DROP TABLE IF EXISTS medical_record_field_value;
DROP TABLE IF EXISTS medical_records;
DROP TABLE IF EXISTS medical_record_type_field_options;
DROP TABLE IF EXISTS medical_record_type_fields;
DROP TABLE IF EXISTS medical_record_type_field_groups;
DROP TABLE IF EXISTS medical_record_types;


CREATE TABLE medical_record_types
(
  id serial NOT NULL,
  name character varying(100),
  displayorder integer,
  code character varying(200),
  CONSTRAINT md_record_types_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_types
  OWNER TO postgres;



CREATE TABLE medical_record_type_field_groups
(
  id serial NOT NULL,
  name character varying(225),
  medicalrecordtypeid integer,
  displayorder integer,
  CONSTRAINT md_type_group_pk PRIMARY KEY (id),
  CONSTRAINT md_reocrd_type_group_fk FOREIGN KEY (medicalrecordtypeid)
      REFERENCES medical_record_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_type_field_groups
  OWNER TO postgres;


CREATE TABLE medical_record_type_fields
(
  id serial NOT NULL,
  medicalrecordtypeid integer,
  fieldname character varying(100),
  fieldtype character varying(100),
  displayorder integer,
  fieldgroupid integer,
  style text,
  CONSTRAINT medical_record_type_field_pk PRIMARY KEY (id),
  CONSTRAINT field_group_fkey FOREIGN KEY (fieldgroupid)
      REFERENCES medical_record_type_field_groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT md_reord_type_field_type_fk FOREIGN KEY (medicalrecordtypeid)
      REFERENCES medical_record_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_type_fields
  OWNER TO postgres;




CREATE TABLE medical_record_type_field_options
(
  id serial NOT NULL,
  fieldid integer,
  name character varying(100),
  CONSTRAINT md_record_field_option_pk PRIMARY KEY (id),
  CONSTRAINT md_record_field_option_type_fk FOREIGN KEY (fieldid)
      REFERENCES medical_record_type_fields (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_type_field_options
  OWNER TO postgres;




CREATE TABLE medical_records
(
  id serial NOT NULL,
  userid integer,
  recordtypeid integer,
  consultationid integer,
  CONSTRAINT md_record_pk PRIMARY KEY (id),
  CONSTRAINT consultaion_record_id FOREIGN KEY (consultationid)
      REFERENCES consultations (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT md_record_type_fk FOREIGN KEY (recordtypeid)
      REFERENCES medical_record_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_records
  OWNER TO postgres;


CREATE TABLE medical_record_field_value
(
  id serial NOT NULL,
  medicalrecordid integer NOT NULL,
  medicalrecordfieldid integer NOT NULL,
  fieldvalue text,
  CONSTRAINT md_record_field_value_pk PRIMARY KEY (id),
  CONSTRAINT md_reord_field_fk FOREIGN KEY (medicalrecordfieldid)
      REFERENCES medical_record_type_fields (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT md_reord_value_fk FOREIGN KEY (medicalrecordid)
      REFERENCES medical_records (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_field_value
  OWNER TO postgres;

  