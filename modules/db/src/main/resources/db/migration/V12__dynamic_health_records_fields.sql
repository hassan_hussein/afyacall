DROP TABLE IF EXISTS medical_record_field_value;
DROP TABLE IF EXISTS medical_records;
DROP TABLE IF EXISTS medical_record_type_field_options;
DROP TABLE IF EXISTS medical_record_type_fields;
DROP TABLE IF EXISTS medical_record_types;

CREATE TABLE medical_record_types
(
  id serial NOT NULL,
  name character varying(100),
  CONSTRAINT md_record_types_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_record_types
  OWNER TO postgres;


 CREATE TABLE medical_record_type_fields
 (
   id serial NOT NULL,
   medicalrecordtypeid integer,
   fieldname character varying(100),
   fieldtype character varying(100),
   CONSTRAINT medical_record_type_field_pk PRIMARY KEY (id),
   CONSTRAINT md_reord_type_field_type_fk FOREIGN KEY (medicalrecordtypeid)
       REFERENCES medical_record_types (id) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION
 )
 WITH (
   OIDS=FALSE
 );
 ALTER TABLE medical_record_type_fields
   OWNER TO postgres;




   CREATE TABLE medical_record_type_field_options
   (
     id serial NOT NULL,
     fieldtypeid integer,
     optionname character varying(100),
     CONSTRAINT md_record_field_option_pk PRIMARY KEY (id),
     CONSTRAINT md_record_field_option_type_fk FOREIGN KEY (fieldtypeid)
         REFERENCES medical_record_type_fields (id) MATCH SIMPLE
         ON UPDATE NO ACTION ON DELETE NO ACTION
   )
   WITH (
     OIDS=FALSE
   );
   ALTER TABLE medical_record_type_field_options
     OWNER TO postgres;




CREATE TABLE medical_records
(
  id serial NOT NULL,
  userid integer,
  recordtypeid integer,
  CONSTRAINT md_record_pk PRIMARY KEY (id),
  CONSTRAINT md_record_type_fk FOREIGN KEY (recordtypeid)
      REFERENCES medical_record_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT md_reocrd_user_fk FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE medical_records
  OWNER TO postgres;


  CREATE TABLE medical_record_field_value
  (
    id serial NOT NULL,
    medicalrecordid integer NOT NULL,
    medicalrecordfieldid integer NOT NULL,
    fieldvalue text,
    CONSTRAINT md_record_field_value_pk PRIMARY KEY (id),
    CONSTRAINT md_reord_field_fk FOREIGN KEY (medicalrecordfieldid)
        REFERENCES medical_record_type_fields (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT md_reord_value_fk FOREIGN KEY (medicalrecordid)
        REFERENCES medical_records (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
  )
  WITH (
    OIDS=FALSE
  );
  ALTER TABLE medical_record_field_value
    OWNER TO postgres;