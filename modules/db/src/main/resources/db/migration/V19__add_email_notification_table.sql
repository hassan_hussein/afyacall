
DROP TABLE IF EXISTS email_notifications;
CREATE TABLE email_notifications
(
  id serial NOT NULL,
  receiver character varying(250) NOT NULL,
  subject text,
  content text,
  sent boolean NOT NULL DEFAULT false,
  createddate timestamp without time zone DEFAULT now(),
  ishtml boolean NOT NULL DEFAULT false,
  CONSTRAINT email_notifications_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE email_notifications
  OWNER TO postgres;


 DROP TABLE IF EXISTS email_attachments;

CREATE TABLE email_attachments
(
  id serial NOT NULL,
  attachmentname character varying(255) NOT NULL,
  attachmentpath character varying(510) NOT NULL,
  attachmentfiletype character varying(255) NOT NULL,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT email_attachments_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE email_attachments
  OWNER TO postgres;





DROP TABLE IF EXISTS email_attachments_relation;

CREATE TABLE email_attachments_relation
(
  emailid integer NOT NULL,
  attachmentid integer NOT NULL,
  CONSTRAINT email_attachments_relation_attachmentid_fkey FOREIGN KEY (attachmentid)
      REFERENCES email_attachments (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT email_attachments_relation_emailid_fkey FOREIGN KEY (emailid)
      REFERENCES email_notifications (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE email_attachments_relation
  OWNER TO postgres;

-- Index: i_email_attachment_emailid

-- DROP INDEX i_email_attachment_emailid;

CREATE INDEX i_email_attachment_emailid
  ON email_attachments_relation
  USING btree
  (emailid);
