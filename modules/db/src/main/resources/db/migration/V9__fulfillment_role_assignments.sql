DROP TABLE IF EXISTS fulfillment_role_assignments;

CREATE TABLE fulfillment_role_assignments
(
  userid integer NOT NULL,
  roleid integer NOT NULL,
  facilityid integer NOT NULL,
  createdby integer,
  createddate timestamp(6) without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp(6) without time zone DEFAULT now(),
  CONSTRAINT fulfillment_role_assignments_facilityid_fkey FOREIGN KEY (facilityid)
      REFERENCES facilities (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fulfillment_role_assignments_roleid_fkey FOREIGN KEY (roleid)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fulfillment_role_assignments_userid_fkey FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_fulfillment_role_assignments UNIQUE (userid, roleid, facilityid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fulfillment_role_assignments
  OWNER TO postgres;
