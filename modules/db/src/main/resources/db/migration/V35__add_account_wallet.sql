﻿DROP TABLE IF EXISTS account_wallet;
CREATE TABLE account_wallet
(
  id serial NOT NULL,
  accountid integer NOT NULL,
  balance numeric(10,2) DEFAULT 0.00,
  CONSTRAINT account_wallet_pk PRIMARY KEY (id),
  CONSTRAINT account_wallet_fk FOREIGN KEY (accountid)
      REFERENCES accounts (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_wallet
  OWNER TO postgres;