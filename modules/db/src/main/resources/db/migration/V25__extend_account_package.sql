DO $$
    BEGIN
        BEGIN
            ALTER TABLE account_packages ADD COLUMN terminationDate timestamp without time zone;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column Termination already exists in users';
        END;
    END;
$$;

DO $$
    BEGIN
        BEGIN
            ALTER TABLE account_packages ADD COLUMN status VARCHAR(250);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column status already exists in users';
        END;
    END;
$$