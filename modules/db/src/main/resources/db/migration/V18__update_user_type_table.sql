DROP TABLE  IF EXISTS user_types;
CREATE TABLE user_types
(
  id serial NOT NULL,
  usertype character varying(250),
  description character varying(200),
  displayorder integer,
  maximumMembers Integer,
  extraFieldName character varying(200),

  CONSTRAINT user_types_pkey PRIMARY KEY (id),
  CONSTRAINT users_types_key UNIQUE (usertype)
)