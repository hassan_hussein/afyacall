﻿delete from field_types;
insert into field_types(name) 
values ('TEXT'),
       ('SELECT'),
       ('RICHTEXT'),
       ('RADIO'),
       ('CHECKBOX');

delete from user_groups cascade;
insert into user_groups(name) 
values ('DOCTOR'),
       ('NURSE'),
       ('AGENT'),
       ('ADMIN');
       