DROP TABLE IF EXISTS user_types CASCADE;

CREATE TABLE user_types
(
  id serial NOT NULL,
  usertype CHARACTER VARYING (250),
  CONSTRAINT user_types_pkey PRIMARY KEY (id),
  CONSTRAINT users_types_key UNIQUE (usertype)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_types
  OWNER TO postgres;