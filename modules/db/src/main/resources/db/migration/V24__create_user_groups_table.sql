DO $$
    BEGIN
        BEGIN
            ALTER TABLE users ADD COLUMN groupId Integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column groupId already exists in users';
        END;
    END;
$$;


DROP TABLE IF EXISTS user_groups;

CREATE TABLE user_groups
(
  id serial NOT NULL,
  name character varying(100),
  CONSTRAINT user_groups_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_groups
  OWNER TO postgres;

ALTER TABLE users
ADD FOREIGN KEY (groupId) REFERENCES user_groups(id);