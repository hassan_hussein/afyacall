ALTER TABLE users DROP COLUMN IF EXISTS landingPageId RESTRICT;

ALTER TABLE users
  ADD COLUMN landingPageId integer;
ALTER TABLE users
  ADD CONSTRAINT user_landing_page_fk FOREIGN KEY (landingPageId) REFERENCES user_landing_pages(id) ON UPDATE NO ACTION ON DELETE NO ACTION;
