ALTER TABLE account_members
  ADD COLUMN expirationdate timestamp without time zone;
ALTER TABLE account_members
  ADD COLUMN activationdate timestamp without time zone;