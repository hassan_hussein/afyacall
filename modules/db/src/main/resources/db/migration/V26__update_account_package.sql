DO $$
    BEGIN
        BEGIN
            ALTER TABLE account_packages ADD COLUMN period INTEGER NOT NULL DEFAULT 0;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column Period already exists in account_packages';
        END;
    END;
$$;

DO $$
    BEGIN
        BEGIN
            ALTER TABLE account_packages ADD COLUMN price NUMERIC(10,2) NOT NULL DEFAULT 0.00;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column price already exists in account_packages';
        END;
    END;
$$