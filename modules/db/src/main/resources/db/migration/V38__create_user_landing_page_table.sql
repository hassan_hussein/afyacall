DROP TABLE IF EXISTS user_landing_pages;

CREATE TABLE user_landing_pages
(
  id serial NOT NULL,
  name character varying(100),
  url character varying(250),
  CONSTRAINT user_landing_pages_pk PRIMARY KEY (id)
)
