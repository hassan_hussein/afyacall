DO $$
    BEGIN
        BEGIN
            ALTER TABLE users ADD COLUMN extensionNumberId Integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column extension_number already exists in users';
        END;
    END;
$$;

ALTER TABLE users
ADD FOREIGN KEY (extensionNumberId) REFERENCES extension_numbers(id);