﻿ALTER TABLE users
  ADD COLUMN geographiczoneid integer;
ALTER TABLE users
  ADD CONSTRAINT user_zone_fk FOREIGN KEY (geographiczoneid) REFERENCES geographic_zones (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
