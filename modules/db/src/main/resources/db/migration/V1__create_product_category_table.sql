CREATE TABLE product_categories
(
  id serial NOT NULL,
  code character varying(50) NOT NULL,
  name character varying(100) NOT NULL,
  displayorder integer NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT product_categories_pkey PRIMARY KEY (id),
  CONSTRAINT product_categories_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE product_categories
  OWNER TO postgres;