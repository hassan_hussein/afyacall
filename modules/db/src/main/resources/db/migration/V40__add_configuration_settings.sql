
DROP TABLE IF EXISTS configuration_settings;

CREATE TABLE configuration_settings
(
  id serial NOT NULL,
  key character varying(250) NOT NULL,
  value character varying(2000),
  name character varying(250) NOT NULL,
  description character varying(1000),
  groupname character varying(250) NOT NULL DEFAULT 'General'::character varying,
  displayorder integer NOT NULL DEFAULT 1,
  valuetype character varying(250) NOT NULL DEFAULT 'TEXT'::character varying,
  valueoptions character varying(1000),
  isconfigurable boolean DEFAULT true,
  CONSTRAINT configuration_settings_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE configuration_settings
  OWNER TO postgres;




delete from configuration_settings where key = 'SMS_USER_NAME';
insert into configuration_settings(key,value,name,description,groupname,displayorder,valuetype,isconfigurable)
values('SMS_USER_NAME','sihebs01','Username for sms gateway','Used for sms gateway authentication','SMS',1,'TEXT',true);


delete from configuration_settings where key = 'SMS_PASSWORD';
insert into configuration_settings(key,value,name,description,groupname,displayorder,valuetype,isconfigurable)
values('SMS_PASSWORD','Sihebs02','Password for sms gateway','Used for sms gateway authentication','SMS',2,'TEXT',true);

delete from configuration_settings where key = 'SMS_URL';
insert into configuration_settings(key,value,name,description,groupname,displayorder,valuetype,isconfigurable)
values('SMS_URL','https://api.infobip.com/sms/1/text/single','Url for sms gateway','Used for sms gateway authentication','SMS',3,'TEXT',true);