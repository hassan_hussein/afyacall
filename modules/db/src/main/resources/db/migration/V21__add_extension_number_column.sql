DO $$
    BEGIN
        BEGIN
            ALTER TABLE users ADD COLUMN isAgent boolean DEFAULT false;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column isAgent already exists in users';
        END;
    END;
$$;

DO $$
    BEGIN
        BEGIN
            ALTER TABLE users ADD COLUMN extension_number Integer;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column extension_number already exists in users';
        END;
    END;
$$