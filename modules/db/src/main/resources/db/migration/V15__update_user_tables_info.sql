
DROP TABLE IF EXISTS user_types;

CREATE TABLE user_types
(
  id serial NOT NULL,
  name usertype,
  CONSTRAINT user_types_pkey PRIMARY KEY (id),
  CONSTRAINT users_types_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_types
  OWNER TO postgres;


 DROP TABLE IF EXISTS account_packages;

CREATE TABLE account_packages
(
  id serial NOT NULL,
  name character varying(200) NOT NULL,
  description character varying(200),
  createddate timestamp without time zone DEFAULT now(),
  displayorder integer,
  CONSTRAINT acccount_packages_pkey PRIMARY KEY (id),
  CONSTRAINT acccount_packages_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account_packages
  OWNER TO postgres;



DROP TABLE IF EXISTS users CASCADE ;

CREATE TABLE users
(
  id serial NOT NULL,
  username character varying(50) NOT NULL,
  password character varying(128) DEFAULT 'not-in-use'::character varying,
  firstname character varying(50) NOT NULL,
  lastname character varying(50) NOT NULL,
  employeeid character varying(50),
  jobtitle character varying(50),
  primarynotificationmethod character varying(50),
  officephone character varying(30),
  cellphone character varying(30),
  email character varying(50),
  supervisorid integer,
  facilityid integer,
  verified boolean DEFAULT false,
  active boolean DEFAULT true,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  restrictlogin boolean DEFAULT false,
  ismobileuser boolean DEFAULT false,
  usertypeid integer,
  packageid integer,
  date_of_birth timestamp without time zone,
  usernumber character varying(250),
  altenativemobilenumber integer,
  gender character varying(50),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_packageid_fkey FOREIGN KEY (packageid)
      REFERENCES account_packages (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT users_usertypeid_fkey FOREIGN KEY (usertypeid)
      REFERENCES user_types (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_user_number UNIQUE (usernumber)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;


DROP table IF EXISTS  rights CASCADE;
CREATE TABLE rights
(
  name character varying(200) NOT NULL,
  righttype character varying(20) NOT NULL,
  description character varying(200),
  createddate timestamp without time zone DEFAULT now(),
  displayorder integer,
  displaynamekey character varying(150),
  CONSTRAINT rights_pkey PRIMARY KEY (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rights
  OWNER TO postgres;

DROP TABLE IF EXISTS roles CASCADE ;
CREATE TABLE roles
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  description character varying(250),
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  modifiedby integer,
  modifieddate timestamp without time zone DEFAULT now(),
  CONSTRAINT roles_pkey PRIMARY KEY (id),
  CONSTRAINT roles_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO postgres;

DROP TABLE IF EXISTS role_rights;
CREATE TABLE role_rights
(
  roleid integer NOT NULL,
  rightname character varying NOT NULL,
  createdby integer,
  createddate timestamp without time zone DEFAULT now(),
  CONSTRAINT role_rights_rightname_fkey FOREIGN KEY (rightname)
      REFERENCES rights (name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT role_rights_roleid_fkey FOREIGN KEY (roleid)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_role_right UNIQUE (roleid, rightname)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role_rights
  OWNER TO postgres;

DROP TABLE IF EXISTS role_assignments;
CREATE TABLE role_assignments
(
  userid integer NOT NULL,
  roleid integer NOT NULL,
  CONSTRAINT role_assignments_roleid_fkey FOREIGN KEY (roleid)
      REFERENCES roles (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT role_assignments_userid_fkey FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_role_assignment UNIQUE (userid, roleid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role_assignments
  OWNER TO postgres;

INSERT INTO rights (name, rightType, description) VALUES

('MANAGE_ROLES', 'ADMIN', 'Permission to create and edit roles in the system'),
('ADMINISTRATION', 'ADMIN', 'Permission to create and edit schedules in the system'),
('MANAGE_USERS', 'ADMIN', 'Permission to create and view users'),
('MANAGE_REPORTS', 'ADMIN', 'Permission to create and view users'),
('MANAGE_ACCOUNT_PACKAGES', 'ADMIN', 'Permission to create and view users'),
('MANAGE_CLIENT_TYPES', 'ADMIN', 'Permission to manage reports'),
('MANAGE_RIGHTS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_GEO_LEVELS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_GEO_ZONES', 'ADMIN', 'Permission to manage reports'),
('MANAGE_SMS', 'ADMIN', 'Permission to manage reports'),
('MANAGE_EXTENSIONS', 'ADMIN', 'Permission to manage reports'),
('SYSTEM_SETTINGS', 'ADMIN', 'Permission to configure Electronic Data Interchange (EDI)');

INSERT INTO roles (name, description) VALUES ('Admin', 'Admin');

INSERT INTO role_rights (roleId, rightName) VALUES ((SELECT
                                                       id
                                                     FROM roles
                                                     WHERE name = 'Admin'), 'MANAGE_ROLES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'ADMINISTRATION'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_USERS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_ACCOUNT_PACKAGES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_CLIENT_TYPES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_RIGHTS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_GEO_LEVELS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_GEO_ZONES'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_REPORTS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_SMS'),
  ((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'SYSTEM_SETTINGS'),
((SELECT
    id
  FROM roles
  WHERE name = 'Admin'), 'MANAGE_EXTENSIONS');

INSERT INTO users (userName, password, facilityId, firstName, lastName, email, verified, active, restrictLogin) VALUES
('Admin123', 'TQskzK3iiLfbRVHeM1muvBCiiKriibfl6lh8ipo91hb74G3OvsybvkzpPI4S3KIeWTXAiiwlUU0iiSxWii4wSuS8mokSAieie', null, 'John', 'Doe', 'John_Doe@openlmis.com', TRUE, TRUE, FALSE);

INSERT INTO role_assignments (userId, roleId) VALUES
((SELECT
    id
  FROM users
  WHERE userName = 'Admin123'), (SELECT
                                   id
                                 FROM roles
                                 WHERE name = 'Admin'));


