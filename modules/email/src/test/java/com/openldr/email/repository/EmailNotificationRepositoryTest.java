package com.openldr.email.repository;

import com.openldr.db.categories.UnitTests;
import com.openldr.email.domain.EmailAttachment;
import com.openldr.email.domain.EmailMessage;
import com.openldr.email.repository.mapper.EmailNotificationMapper;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@Category(UnitTests.class)
@RunWith(MockitoJUnitRunner.class)
public class EmailNotificationRepositoryTest {

  @Mock
  EmailNotificationMapper mapper;

  @InjectMocks
  EmailNotificationRepository repository;

  @Test
  public void shouldQueueMessage() throws Exception {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setTo("test@gmail.com");
    message.setSubject("the subject");
    message.setText("The main message.");
    repository.queueMessage(message);
    verify(mapper).insert(anyString(), anyString(), anyString(), anyBoolean());
  }

  @Test
  public void shouldInsertEmailMessageWhenNoAttachments() throws Exception {
    EmailMessage message = generateEmailMessage();
    repository.queueEmailMessage(message);
    verify(mapper).insertEmailMessage(message);

  }

  @Test
  public void shouldInsertEmailMessageAndRelationWhenHasAttachments() throws Exception {
    EmailMessage message = generateEmailMessage();
    message.addEmailAttachment(generateEmailAttachment());
    message.addEmailAttachment(generateEmailAttachment());

    repository.queueEmailMessage(message);
    verify(mapper).insertEmailMessage(message);
    verify(mapper, times(2)).insertEmailAttachmentsRelation(anyLong(), anyLong());

  }

  private EmailMessage generateEmailMessage() {
    EmailMessage message = new EmailMessage();
    message.setTo("test@dev.org");
    message.setText("The Test Message");
    message.setSubject("test");
    return message;
  }

  private EmailAttachment generateEmailAttachment() {
    EmailAttachment attachment = new EmailAttachment();
    attachment.setAttachmentName("test file");
    attachment.setAttachmentPath("/path");
    return attachment;
  }
}