
package com.openldr.email.exception;

/**
 * This is an exception that can be thrown during email sending process.

 */

public class EmailException extends RuntimeException {

  public EmailException(String message){
    super(message);
  }

}
