package com.openldr.email.mapper;

import com.openldr.email.domain.EmailMessage;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * It maps each row of database representation of OpenLdrEmailMessage to corresponding OpenldrEmailMessage entity.

 */

@Component
public class EmailMessageRowMapper implements RowMapper<EmailMessage> {

  @Override
  public EmailMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
    EmailMessage emailMessage = new EmailMessage();
    emailMessage.setTo(rs.getString("receiver"));
    emailMessage.setSubject(rs.getString("subject"));
    emailMessage.setText(rs.getString("content"));
    emailMessage.setHtml(rs.getBoolean("isHtml"));
    emailMessage.setId(rs.getLong("id"));
    return emailMessage;
  }
}
