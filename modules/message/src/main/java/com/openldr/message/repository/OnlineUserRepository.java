package com.openldr.message.repository;

import com.openldr.message.domain.ConnectedAgent;
import com.openldr.message.repository.mapper.OnlineUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 7/29/16.
 */
@Repository
public class OnlineUserRepository {

    @Autowired
    OnlineUserMapper mapper;

    public void saveConnectedUser(ConnectedAgent agent) {
        mapper.saveConnectedUser(agent);
    }

    public void updateUserStatus(ConnectedAgent agent) {
        mapper.updateUserStatus(agent);
    }

    public ConnectedAgent getByUserId(Long userId) {
        return mapper.getByUserId(userId);
    }

    public List<ConnectedAgent> getAll() {
        return mapper.getAll();
    }
}
