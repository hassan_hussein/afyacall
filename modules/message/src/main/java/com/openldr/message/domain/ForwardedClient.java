package com.openldr.message.domain;

public class ForwardedClient {

    private String content;

    public ForwardedClient(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
