package com.openldr.message.service;

import com.openldr.medical_record.domain.AuthenticatedPin;
import com.openldr.medical_record.service.ConsultationService;
import org.asteriskjava.manager.*;
import org.asteriskjava.manager.event.BridgeEvent;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.event.VarSetEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * Created by chrispinus on 7/28/16.
 */

@Component
public class AsteriskManager extends Thread implements ManagerEventListener {

    @Autowired
    ConsultationService consultationService;
    private ManagerConnection managerConnection;

    @PostConstruct
    public void run() {
        ManagerConnectionFactory factory = new ManagerConnectionFactory("192.168.1.4", "afyacall", "123qaz");
        this.managerConnection = factory.createManagerConnection();
        managerConnection.addEventListener(this);
        try {
            managerConnection.login();
        } catch (IOException e) {
            System.out.println("Socket Connection failed due to :" + e.getMessage());
        } catch (AuthenticationFailedException e) {
            System.out.println("Authentication Failed Due to:" + e.getMessage());
        } catch (org.asteriskjava.manager.TimeoutException e) {
            System.out.println("Asterisk server timeout due to:" + e.getMessage());
        }
    }

    @Override
    public void onManagerEvent(ManagerEvent managerEvent) {

        if (managerEvent.getClass() == org.asteriskjava.manager.event.BridgeEvent.class) {
            BridgeEvent bridgeEvent = (BridgeEvent) managerEvent;
//
            AuthenticatedPin pin = consultationService.getPIN(bridgeEvent.getUniqueId1().trim());
            System.out.println("____________PIN TO CHECK:" + pin.getPin());

            consultationService.initiateCallConsultation(bridgeEvent.getCallerId1(), bridgeEvent.getCallerId2(), bridgeEvent.getBridgeState(), pin.getPin());


        }
        if (managerEvent.getClass() == org.asteriskjava.manager.event.VarSetEvent.class) {
            VarSetEvent varSetEvent = (VarSetEvent) managerEvent;

            if (varSetEvent.getVariable().trim().equals("PIN")) {
                consultationService.savePIN(varSetEvent.getValue().trim(), varSetEvent.getUniqueId().trim());
//                System.out.println("******************PIN:"+varSetEvent.getValue());
//                System.out.println("******************PIN:"+varSetEvent.getUniqueId());

            }
        }
    }

    @PreDestroy
    public void cleanUp() throws Exception {
        this.managerConnection.logoff();

    }

    //TODO Impliment Scheduling
    public void CheckConnection() {
        if (this.managerConnection == null || this.managerConnection.getState() == ManagerConnectionState.DISCONNECTED) {
            run();
        }
    }
}
