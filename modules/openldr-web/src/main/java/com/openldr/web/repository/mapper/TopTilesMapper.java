package com.openldr.web.repository.mapper;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface TopTilesMapper {

    @Select("Select * from ")
    Map<String, String> getTopTiles();

}
