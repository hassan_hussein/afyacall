
package com.openldr.web.controller.medical_record;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.domain.MedicalRecordType;
import com.openldr.medical_record.service.MedicalRecordTypeService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/medical-record-type")
public class MedicalRecordTypeController extends BaseController {

    @Autowired
    MedicalRecordTypeService service;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody MedicalRecordType medicalRecordType) {
        service.save(medicalRecordType);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("medicalRecordType", medicalRecordType);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-all", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll() {

        List<MedicalRecordType> recordTypes = service.getAll();
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("recordTypes", recordTypes);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-id", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        MedicalRecordType recordType = service.getById(id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("recordType", recordType);
        return openLdrResponse.response(OK);
    }

}