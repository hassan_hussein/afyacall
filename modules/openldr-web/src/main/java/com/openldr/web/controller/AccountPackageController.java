package com.openldr.web.controller;

import com.openldr.core.domain.AccountPackage;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.AccountPackageService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.error;
import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by hassan on 7/9/16.
 */
@RequestMapping(value = "/accountPackage")
@Controller
@NoArgsConstructor
public class AccountPackageController extends BaseController {

    @Autowired
    private AccountPackageService service;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll() {
        return OpenLdrResponse.response("allPackages", service.getAll());
    }

    @RequestMapping(value = "/GetById/{id}", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllById(@PathVariable Long id) {
        return OpenLdrResponse.response("allById", service.getAllById(id));
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody AccountPackage accountPackage) {
        try {
            service.Insert(accountPackage);
            return success("Data served Successful", accountPackage.getName());

        } catch (DataException ex) {
            return error(ex, HttpStatus.CONFLICT);
        }

    }


    @RequestMapping(value = "/accountPackages", method = POST, headers = ACCEPT_JSON)
    // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> save(@RequestBody AccountPackage accountPackage,
                                                HttpServletRequest request) {
        try {
            service.save(accountPackage);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.account.package.save.success", accountPackage.getName()));
        success.getBody().addData("packages", accountPackage);
        return success;
    }


    @RequestMapping(value = "/accountPackages/{id}", method = PUT, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> update(@RequestBody AccountPackage accountPackage,
                                                  @PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        Long userId = loggedInUserId(request);
        accountPackage.setId(id);
        try {
            service.save(accountPackage);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.account.package.updated.success", accountPackage.getName()));
        success.getBody().addData("packages", accountPackage);
        return success;
    }


    @RequestMapping(value = "/delete/{id}", method = DELETE, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
    public ResponseEntity<OpenLdrResponse> delete(@PathVariable("id") Long id,
                                                  HttpServletRequest request) {
        AccountPackage accountPackage = service.getAllById(id);
        try {

            service.delete(id);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }

        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.account.package.deleted.success", accountPackage.getName()));
        success.getBody().addData("packages", accountPackage);
        return success;
    }

}
