package com.openldr.web.controller;

import com.openldr.core.domain.Right;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.RightService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by hassan on 7/17/16.
 */

@Controller
@NoArgsConstructor
public class RightsController extends BaseController {

    public static final String RIGHTS = "rights";
    public static final String RIGHT_BY_NAME = "rights";

    @Autowired
    private RightService service;

    @RequestMapping(value = "/rights/allRights", method = GET)
    //  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_ROLE')")
    public ResponseEntity<OpenLdrResponse> getAllRights() {
        return response(RIGHTS, service.getAll());
    }

    @RequestMapping(value = "/rights/getByName/{name}", method = GET, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> getByName(
            @PathVariable("name") String name,
            HttpServletRequest request) {
        ResponseEntity<OpenLdrResponse> response = response(RIGHT_BY_NAME, service.getByName(name));
        return response;
    }


    @RequestMapping(value = "/rights/{name}", method = PUT, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> update(@RequestBody Right right,
                                                  @PathVariable("name") String name,
                                                  HttpServletRequest request) {
        try {
            right.setName(name);
            service.update(right);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }

        return success("Right Updated Successfully");

    }

    @RequestMapping(value = "/rights/save", method = POST, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> save(@RequestBody Right right,
                                                HttpServletRequest request) {
        try {
            service.insertRight(right);
        } catch (DataException e) {
            return error(e, BAD_REQUEST);
        }


        return success("Right Created Successfully");
    }

    @RequestMapping(value = "/rights/delete/{name}", method = DELETE, headers = ACCEPT_JSON)
    //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_USER')")
    public ResponseEntity<OpenLdrResponse> delete(@PathVariable String name,
                                                  HttpServletRequest request) {
        try {
            service.delete(name);
        } catch (DataException e) {
            return error(e, BAD_REQUEST);
        }

        return success("Right Deleted Successfully");
    }


}
