
package com.openldr.web.controller.user_cv;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.domain.UserCVType;
import com.openldr.medical_record.service.UserCVTypeService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/user-cv-type")
public class UserCVTypeController extends BaseController {

    @Autowired
    UserCVTypeService service;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody UserCVType userCVType) {
        service.save(userCVType);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("userCVType", userCVType);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-all", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll() {

        List<UserCVType> cvTypes = service.getAll();
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("cvTypes", cvTypes);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-id", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getById(@Param("id") Long id) {

        UserCVType userCVType = service.getById(id);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("cvType", userCVType);
        return openLdrResponse.response(OK);
    }

}