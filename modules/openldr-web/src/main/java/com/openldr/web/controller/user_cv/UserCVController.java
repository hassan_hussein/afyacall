
package com.openldr.web.controller.user_cv;

import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.medical_record.domain.UserCV;
import com.openldr.medical_record.service.UserCVService;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@NoArgsConstructor
@RequestMapping(value = "/user-cv")
public class UserCVController extends BaseController {

    @Autowired
    UserCVService service;

    @RequestMapping(value = "/save", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody UserCV userCV) {
        service.save(userCV);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("UserCV", userCV);
        return openLdrResponse.response(OK);
    }


    @RequestMapping(value = "/get-by-user", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByUserId(@Param("userId") Long userId) {

        List<UserCV> userCVs = service.getByUserId(userId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("userCV", userCVs);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/get-by-user-and-type", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByUserAndType(@Param("userId") Long userId, @Param("recordTypeId") Long recordTypeId) {

        List<UserCV> userCVs = service.getByUserAndType(userId, recordTypeId);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("userCV", userCVs);
        return openLdrResponse.response(OK);
    }

}