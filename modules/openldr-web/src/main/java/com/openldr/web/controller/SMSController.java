package com.openldr.web.controller;

import com.openldr.core.service.SMSManagementService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by hassan on 7/26/16.
 */


@Controller
@NoArgsConstructor
public class SMSController extends BaseController {

    @Autowired
    private SMSManagementService smsService;

    @RequestMapping(value = "/public/sms", method = RequestMethod.GET)
    public void IncomingSMS(@RequestParam(value = "message") String message, @RequestParam(value = "phone_no") String phoneNumber) {
        smsService.SaveIncomingSMSMessage(message, phoneNumber);
    }

    @RequestMapping(value = "/sms/setDetails", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public void getParameterForSendingSms(@RequestParam("content") String message, @RequestParam("mobile") String phoneNumber) {
        try {
           // smsService.sendSMS(message, phoneNumber);
            smsService.SaveIncomingSMSMessage(message,phoneNumber);
        } catch (Exception e) {
            System.out.print(e.fillInStackTrace());
        }
    }

    @RequestMapping(value = "/getSMS", method = RequestMethod.GET)
    public void IncomingMessage(@RequestParam("content") String message, @RequestParam("mobile") String phoneNumber) {
        smsService.SaveIncomingSMSMessage(message, phoneNumber);
    }

    @RequestMapping(value = "/sms/MessageList", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request) {
        return OpenLdrResponse.response("sms", smsService.getSmsMessages());
    }

    @RequestMapping(value = "/sms/MessagesForMobile", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getMessagesForMobilePhone(@RequestParam("mobile") String mobile) {
        return OpenLdrResponse.response("sms", smsService.getMessagesForMobile(mobile));
    }

 /*   @RequestMapping(value = "/sms/MessagesForMobile", method = RequestMethod.GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getTestMessage(@RequestParam("mobile") String mobile) {
        return OpenLdrResponse.response("sms", smsService.getMessagesForMobile(mobile));
    }
*/
}
