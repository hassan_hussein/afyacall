package com.openldr.web.controller;

import com.openldr.core.service.ConfigurationSettingService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.web.model.ConfigurationDTO;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@NoArgsConstructor
public class SettingController extends BaseController {

  public static final String SETTINGS = "settings";

  @Autowired
  private ConfigurationSettingService configurationService;


  @RequestMapping(value = "/settings", method = RequestMethod.GET, headers = "Accept=application/json")
/*
  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SETTING')")
*/
  public ResponseEntity<OpenLdrResponse> getAll() {
    ConfigurationDTO dto = new ConfigurationDTO();
    dto.setList( configurationService.getConfigurations() );
    return OpenLdrResponse.response(SETTINGS, dto );
  }

  @RequestMapping(value = "/settings/{key}",  method = RequestMethod.GET, headers = "Accept=application/json")
  public ResponseEntity<OpenLdrResponse> getByKey(@PathVariable(value = "key") String key) {
    return OpenLdrResponse.response(SETTINGS, configurationService.getByKey(key) );
  }

  @RequestMapping(value = "/saveSettings", method = RequestMethod.POST, headers = "Accept=application/json")
/*
  @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_SETTING')")
*/
  public ResponseEntity<OpenLdrResponse> updateSettings(@RequestBody ConfigurationDTO settings) {
    configurationService.update(settings.getList());
    return OpenLdrResponse.response(SETTINGS, "success");
  }

}
