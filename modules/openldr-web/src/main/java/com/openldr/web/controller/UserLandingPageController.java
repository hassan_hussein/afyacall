package com.openldr.web.controller;

import com.openldr.core.domain.UserLandingPage;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.UserLandingPageService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.openldr.core.web.OpenLdrResponse.success;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;


@Controller
@NoArgsConstructor
public class UserLandingPageController extends BaseController {

    @Autowired
    private UserLandingPageService service;

    @RequestMapping(value = "/landingPages", method = POST, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> save(@RequestBody UserLandingPage landingPage,
                                                HttpServletRequest request) {
        try {
            service.save(landingPage);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.landing.page.created.success", landingPage.getUrl()));
        success.getBody().addData("landingPages", landingPage);
        return success;
    }

    @RequestMapping(value = "/landingPages/{id}", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> update(@RequestBody UserLandingPage landingPage,
                                                  HttpServletRequest request, @PathVariable Long id) {
        Long userId = loggedInUserId(request);
        landingPage.setUserId(userId);
        landingPage.setId(id);
        try {
            service.save(landingPage);
        } catch (DataException e) {
            return OpenLdrResponse.error(e, BAD_REQUEST);
        }
        ResponseEntity<OpenLdrResponse> success = success(
                messageService.message("message.landing.page.updated.success", landingPage.getUrl()));
        success.getBody().addData("landingPages", landingPage);
        return success;
    }

    @RequestMapping(value = "/landingPageGetBy/{id}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAllBy(@PathVariable("id") Long id, HttpServletRequest request) {
        return OpenLdrResponse.response("geo_levels", service.getLandingPageById(id));
    }

    @RequestMapping(value = "/getByUser/{id}", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getByUser(@PathVariable("id") Long id, HttpServletRequest request) {
        return OpenLdrResponse.response("allByUser", service.getByUserId(id));
    }

    @RequestMapping(value = "/landingPages/getAll", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request) {
        return OpenLdrResponse.response("landingPages", service.getAll());
    }

    @RequestMapping(value = "/landingPages/delete/{id}", method = DELETE, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> getAll(HttpServletRequest request, @PathVariable Long id) {
        return OpenLdrResponse.response("landingPages", service.delete(id));
    }


}
