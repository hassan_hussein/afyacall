package com.openldr.web.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.openldr.upload.Importable;
import com.openldr.upload.RecordHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by hassan on 8/16/16.
 */
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
public class UploadBean {

    @Setter
    private String displayName;

    @Setter
    @Getter
    private RecordHandler recordHandler;

    @Setter
    @Getter
    private Class<? extends Importable> importableClass;

    @JsonProperty
    @SuppressWarnings("unused")
    public String getDisplayName() {
        return displayName;
    }
}
