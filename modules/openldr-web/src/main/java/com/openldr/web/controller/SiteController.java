
package com.openldr.web.controller;

import com.openldr.core.domain.User;
import com.openldr.core.service.UserService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handles endpoints to related to user management like resetting password, disabling a user, etc.
 */

@Controller
@NoArgsConstructor
@RequestMapping(value = "/site")
public class SiteController extends BaseController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/register/client", method = PUT, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> client(@RequestBody User user) {

        String verificationCode = userService.createSiteUser(user);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("code", verificationCode);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/client/get-by-username", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> clientGetByUsername(@Param("userName") String userName) {

        User user = userService.getByUserName(userName);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("user", user);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/client/get-by-cellphone", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> clientGetByCellPhone(@Param("cellPhone") String cellPhone) {

        User user = userService.getByCellPhone(cellPhone);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("user", user);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/client/get-by-email", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> clientGetByEmail(@Param("email") String email) {

        User user = userService.getByUserName(email);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("user", user);
        return openLdrResponse.response(OK);
    }

    @RequestMapping(value = "/client/verify", method = GET, headers = ACCEPT_JSON)
    public ResponseEntity<OpenLdrResponse> verify(@Param("token") String token) {

        Integer verified = userService.verify(token);
        OpenLdrResponse openLdrResponse = new OpenLdrResponse("verification", "SUCCESS");
        return openLdrResponse.response(OK);
    }


}