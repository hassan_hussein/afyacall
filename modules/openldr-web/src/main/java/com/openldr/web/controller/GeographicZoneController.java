
package com.openldr.web.controller;

import com.openldr.core.domain.GeographicLevel;
import com.openldr.core.domain.GeographicZone;
import com.openldr.core.domain.Pagination;
import com.openldr.core.exception.DataException;
import com.openldr.core.service.GeographicLevelService;
import com.openldr.core.service.GeographicZoneService;
import com.openldr.core.web.OpenLdrResponse;
import com.openldr.core.web.controller.BaseController;
import com.openldr.web.model.GeoZoneInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.openldr.core.web.OpenLdrResponse.success;
import static java.lang.Integer.parseInt;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * This controller handles endpoint related to GET, PUT, POST and other operations on geographicZones.
 */

@Controller
public class GeographicZoneController extends BaseController {

  public static final String GEO_ZONES = "geoZones";
  @Autowired
  private GeographicZoneService service;

  @Autowired
  private GeographicLevelService geographicLevelService;

  @RequestMapping(value = "/geographicZones/{id}", method = GET, headers = ACCEPT_JSON)
  // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_DISTRIBUTION, MANAGE_GEOGRAPHIC_ZONE')")
  public ResponseEntity<OpenLdrResponse> get(@PathVariable Long id) {
    return OpenLdrResponse.response("geoZone", service.getById(id));
  }

  @RequestMapping(value = "/geographicZones", method = POST, headers = ACCEPT_JSON)
  // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public ResponseEntity<OpenLdrResponse> insert(@RequestBody GeographicZone geographicZone,
                                                HttpServletRequest request) {
    Long userId = loggedInUserId(request);
    geographicZone.setCreatedBy(userId);
    geographicZone.setModifiedBy(userId);
    try {
      service.save(geographicZone);
    } catch (DataException e) {
      return OpenLdrResponse.error(e, BAD_REQUEST);
    }
    ResponseEntity<OpenLdrResponse> success = success(
            messageService.message("message.geo.zone.created.success", geographicZone.getName()));
    success.getBody().addData("geoZone", geographicZone);
    return success;
  }

  @RequestMapping(value = "/geographicZones/{id}", method = PUT, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public ResponseEntity<OpenLdrResponse> update(@RequestBody GeographicZone geographicZone,
                                                @PathVariable("id") Long id,
                                                HttpServletRequest request) {
    Long userId = loggedInUserId(request);
    geographicZone.setId(id);
    geographicZone.setModifiedBy(userId);
    try {
      service.save(geographicZone);
    } catch (DataException e) {
      return OpenLdrResponse.error(e, BAD_REQUEST);
    }
    ResponseEntity<OpenLdrResponse> success = success(
            messageService.message("message.geo.zone.updated.success", geographicZone.getName()));
    success.getBody().addData("geoZone", geographicZone);
    return success;
  }

  @RequestMapping(value = "/geographicZones", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public ResponseEntity<OpenLdrResponse> search(@RequestParam(value = "searchParam") String searchParam,
                                                @RequestParam(value = "columnName") String columnName,
                                                @RequestParam(value = "page", defaultValue = "1") Integer page) {
    Pagination pagination = service.getPagination(page);
    pagination.setTotalRecords(service.getTotalSearchResultCount(searchParam, columnName));
    List<GeographicZone> geographicZones = service.searchBy(searchParam, columnName, page);
    ResponseEntity<OpenLdrResponse> response = OpenLdrResponse.response(GEO_ZONES, geographicZones);
    response.getBody().addData("pagination", pagination);
    return response;
  }

  @RequestMapping(value = "/geographicLevels", method = GET, headers = ACCEPT_JSON)
  // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public List<GeographicLevel> getAllGeographicLevels() {
    return service.getAllGeographicLevels();
  }

  @RequestMapping(value = "/parentGeographicZones/{geoLevelCode}", method = GET, headers = ACCEPT_JSON)
  // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public List<GeographicZone> getAllGeographicZonesAbove(@PathVariable("geoLevelCode") String geographicLevelCode) {
    GeographicLevel geographicLevel = new GeographicLevel(geographicLevelCode, null, null);
    return service.getAllGeographicZonesAbove(geographicLevel);
  }

  @RequestMapping(value = "/filtered-geographicZones", method = GET, headers = ACCEPT_JSON)
  //@PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE, MANAGE_SUPERVISORY_NODE, MANAGE_REQUISITION_GROUP, MANAGE_SUPPLY_LINE, MANAGE_USER')")
  public ResponseEntity<OpenLdrResponse> getGeographicZoneByCodeOrName(@RequestParam(value = "searchParam") String searchParam,
                                                                       @Value("${search.results.limit}") String searchLimit) {
    Integer count = service.getGeographicZonesCountBy(searchParam);
    if (count <= parseInt(searchLimit)) {
      List<GeographicZone> geographicZones = service.getGeographicZonesByCodeOrName(searchParam);
      return OpenLdrResponse.response(GEO_ZONES, geographicZones);
    } else {
      return OpenLdrResponse.response("message", messageService.message("too.many.results.found"));
    }
  }

  @RequestMapping(value = "/geographic-zone/save-gis", method = POST, headers = ACCEPT_JSON)
  // @PreAuthorize("@permissionEvaluator.hasPermission(principal,'MANAGE_GEOGRAPHIC_ZONE')")
  public ResponseEntity<OpenLdrResponse> saveGeographicZoneGIS(@RequestBody GeoZoneInfo geoZoneGeometries, HttpServletRequest request) {
    service.saveGisInfo(geoZoneGeometries.getFeatures(), loggedInUserId(request));
    return OpenLdrResponse.response("status", true);
  }

  @RequestMapping(value = "/geographic-zone/get-countries", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getCountries() {
    return OpenLdrResponse.response("countries", service.getAllCountries());
  }

  @RequestMapping(value = "/geographic-zone/get-regions", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getRegions(@RequestParam("countryId") Long countryId) {
    return OpenLdrResponse.response("regions", service.getRegionsByCountry(countryId));
  }

  @RequestMapping(value = "/geographic-zone/get-districts", method = GET, headers = ACCEPT_JSON)
  public ResponseEntity<OpenLdrResponse> getDistricts(@RequestParam("regionId") Long regionId) {
    return OpenLdrResponse.response("districts", service.getDistrictsByRegion(regionId));
  }



}
