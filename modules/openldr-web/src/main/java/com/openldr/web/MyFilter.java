package com.openldr.web;

import com.openldr.web.controller.ProductCategoryController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by chrispinus on 3/17/16.
 */
public class MyFilter implements javax.servlet.Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Logger logger = LoggerFactory.getLogger(ProductCategoryController.class);
        logger.info("Pass through the filter: " + request.toString());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
