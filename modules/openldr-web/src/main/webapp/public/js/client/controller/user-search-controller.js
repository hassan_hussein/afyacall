
function UserSearchController($scope,$filter,$timeout,user,SendInvoiceSMS,SendPINSMS,accountTypes,GetRegions,Members, GetDistricts,countries,accountPackages,organizations,$location,CreateAccount, Account, navigateBackService) {


    $scope.searchOptions = [
        {value: "account", name: "Account",placeholder:"Enter Reference Number"},
        {value: "member", name: "Member",placeholder:"Enter first name, last name or pin"}
    ];
    $scope.selectedSearchOption = {value:"",name:"Select Option",placeholder:""};
    $scope.selectSearchType = function (searchOption) {
        $scope.selectedSearchOption = searchOption;

    };

    $scope.search = function () {
       $scope.account=undefined;
       $scope.members=undefined;
       if($scope.selectedSearchOption.value==='account' && $scope.query !== undefined){
           $scope.getAccount($scope.query);
       }
       else if($scope.selectedSearchOption.value==='member' && $scope.query !== undefined)
       {
            $scope.getMember($scope.query);
       }
     };


    $scope.accountTypes = accountTypes;
    $scope.countries=countries;
    $scope.accountPackages = accountPackages;

    $scope.showBalanceModal=function(){
        $scope.balanceModal=true;
    };
    $scope.cancelBalanceModal=function(){
        $scope.balanceModal=false;
    };
    $scope.doneBalance=function(){
         $scope.balanceModal=false;
    };
    $scope.defaultMember=function(member){
       if(member.user.cellPhone == undefined){
          $scope.noCellPhone=true;
          alert("Selected Member has no cellPhone cant be default member");
          member.default=false;
          return;
       }
        $scope.account.members.forEach(function(m){
            m.default=false;
        });
        member.default=true;
    }

    $scope.sendInvoiceSMS=function(referenceNumber){
       SendInvoiceSMS.get({referenceNumber:referenceNumber},function(data){
           $scope.messageSent=true;
                     $timeout(function () {
                        $scope.messageSent=false;
           }, 3000);
       });
    };
    $scope.sendPINSMS=function(referenceNumber){
       SendPINSMS.get({referenceNumber:referenceNumber},function(data){
            $scope.messageSent=true;
            $timeout(function () {
                  $scope.messageSent=false;
            }, 3000);
       });
    };

    $scope.GetRegions=function(){
           $scope.regions=[];
           if($scope.memberToAdd.countryId !== undefined)
           {
               GetRegions.get({countryId:$scope.memberToAdd.countryId},function(data){
                   $scope.regions=data.regions;
               });
           }
        };
    $scope.GetDistricts=function(){
      $scope.districts=[];
            if($scope.memberToAdd.regionId !== undefined)
            {
               GetDistricts.get({regionId:$scope.memberToAdd.regionId},function(data){
                   $scope.districts=data.districts;
               });
           }
     };

    $scope.showResults = false;
    $scope.currentPage = 1;

    $scope.getAccount=function(referenceNumber){
        Account.get({referenceNumber:referenceNumber},function(data){
            $scope.account=data.account;
        });
    };

    $scope.getMember=function(query){
       Members.get({query:query},function(data){
           $scope.members=data.members;
       });
    }

    $scope.goToAccount=function(referenceNumber){
      $scope.query=referenceNumber;
      $scope.selectedSearchOption={value: "account", name: "Account",placeholder:"Enter Reference Number"};
      $scope.search();
    };
    $scope.saveAccount = function () {

           CreateAccount.update($scope.account,function(data){
               if(data.Account !== undefined){
                    $scope.$parent.showAccountSaveSuccessFull=true;
                    $timeout(function(){
                      $scope.$parent.showAccountSaveSuccessFull=false;
                    },2000)
                    $scope.getAccount();
               }
           });
         console.log($scope.account);
        };

    $scope.addAccountMember=function(){
       var member={};
           member.user=$scope.memberToAdd;
           member.unsaved=true;
           if($scope.account.members.length === 0)
             member.isDefault=true;
             $scope.account.members.push(member);
             $scope.memberToAdd={};
             $scope.newMemberModal=false;
    };

   $scope.showAddModal=function(){
       $scope.newMemberModal=true;
   };

   $scope.cancelAddModal=function(){
       $scope.newMemberModal=false;
   };

    $scope.triggerSearch = function (event) {
        console.log(event.keyCode);
        if (event.keyCode === 13) {
            $scope.getAccount();
        }
    };

    $scope.clearSearch = function () {
        $scope.query = "";
        $scope.totalItems = 0;
        $scope.userList = [];
        $scope.showResults = false;
        angular.element("#searchUser").focus();
    };

    $scope.edit = function (id) {
        var data = {query: $scope.query};
        navigateBackService.setData(data);
        $location.path('edit/' + id);
    };

    $scope.$watch('currentPage', function () {
        if ($scope.currentPage !== 0) {
            //$scope.loadUsers($scope.currentPage, $scope.searchedQuery);
        }
    });

   $scope.openMedicalRecord=function(){
       $scope.mdModal=true;
   }
}


UserSearchController.resolve = {

    user: function ($q, Users, $route, $timeout) {
        var userId = $route.current.params.userId;
        if (!userId) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            Users.get({id: userId}, function (data) {
                deferred.resolve(data.user);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },


    accountTypes: function ($q, GetAccountType, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetAccountType.get({}, function (data) {
                deferred.resolve(data.types);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    countries: function ($q, GetCountries, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetCountries.get({}, function (data) {
                deferred.resolve(data.countries);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },


    accountPackages: function ($q, GetAccountPackages, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {

            GetAccountPackages.get({}, function (data) {
                if(data.allPackages !== null)
                    deferred.resolve(data.allPackages);
                else
                    return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    },
     organizations: function ($q, Organization, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {

            Organization.get({}, function (data) {
                if(data.organizations !== null)
                    deferred.resolve(data.organizations);
                else
                    return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    }
};
