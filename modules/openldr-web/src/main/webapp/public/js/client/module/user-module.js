var user = angular.module('client', ['afyacall','ngGrid', 'ui.bootstrap.modal','ui.bootstrap.dropdownToggle','ngAnimate','ngLetterAvatar', 'ui.bootstrap.dialog','ui.bootstrap.pagination','ngStomp','rorymadden.date-dropdowns']);

user.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/search', {controller: UserSearchController, templateUrl: '/public/pages/client/partials/search.html', resolve: UserSearchController.resolve}).
        when('/create-user', {controller: UserController, templateUrl: '/public/pages/client/partials/create.html', resolve: UserController.resolve,reloadOnSearch: true}).
        when('/profile', {controller: UserProfileController, templateUrl: '/public/pages/client/partials/profile.html', resolve: UserProfileController.resolve}).
        when('/payment', {controller: UserPaymentController, templateUrl: '/public/pages/client/partials/payment.html', resolve: UserPaymentController.resolve}).
        when('/edit/:userId', {controller: UserController, templateUrl: '/public/pages/client/partials/create.html', resolve: UserController.resolve}).

        otherwise({redirectTo: '/search'});
}])

    .directive('onKeyup', function () {
        return function (scope, elm, attrs) {
            elm.bind("keyup", function () {
                scope.$apply(attrs.onKeyup);
            });
        };
    })
    .directive('select2Blur', function () {
        return function (scope, elm, attrs) {
            angular.element("body").on('mousedown', function (e) {
                $('.select2-dropdown-open').each(function () {
                    if (!$(this).hasClass('select2-container-active')) {
                        $(this).data("select2").blur();
                    }
                });
            });
        };
    }).directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
 .run(function ($rootScope, AuthorizationService) {
 $rootScope.userSelected = "selected";
 });

