function UserController($scope,countries,GetRegions,Organization,SaveOrganization,GetDistricts, $window,$location, $dialog,accountPackages,accountTypes,organizations, messageService,CreateAccount, $timeout) {

    $scope.accountTypes = accountTypes;
    $scope.countries=countries;
    $scope.accountPackages = accountPackages;
    $scope.account={members:[],wallet:{balance:0}};
    $scope.memberToAdd={};
    $scope.organizations=organizations;

    $scope.createAccount = function () {

      var noDefault=true;
      $scope.account.members.forEach(function(m){
          if(m.default){
             noDefault=false;
          }
      });
      if(noDefault){
         alert("Please set default member first");
         return;
      }

       var callBack=function(results){
          if(results){
                CreateAccount.update($scope.account,function(data){
                           if(data.Account !== undefined){
                                $scope.$parent.showAccountSaveSuccessFull=true;
                                $scope.$parent.successfulReference=data.Account.referenceNumber;
                                $location.path('/search');
                           }
                       });
           }
        };
       var options = {
                        id: "confirmDialog",
                        header: "label.confirm.create.account",
                        body: "msg.create.account.confirmation"
        };
       OpenLmisDialog.newDialog(options, callBack, $dialog);


    };
    $scope.showBalanceModal=function(){
         $scope.balanceModal=true;
    }
    $scope.cancelBalanceModal=function(){
         $scope.balanceModal=false;
    }
    $scope.doneBalance=function(){
         $scope.balanceModal=false;
    }
    $scope.GetRegions=function(){
       $scope.regions=[];
       if($scope.memberToAdd.countryId !== undefined)
       {
           GetRegions.get({countryId:$scope.memberToAdd.countryId},function(data){
               $scope.regions=data.regions;
           });
       }
    }
    $scope.GetDistricts=function(){
       $scope.districts=[];
        if($scope.memberToAdd.regionId !== undefined)
        {
           GetDistricts.get({regionId:$scope.memberToAdd.regionId},function(data){
               $scope.districts=data.districts;
           });
       }
    }
    $scope.getDistrictToDisplay=function(geoZoneId){
      if(geoZoneId !== undefined)
       var d=_.findWhere($scope.districts,{id:parseInt(geoZoneId)});
       return (d !== undefined)?d.name:undefined;
    }
    $scope.defaultMember=function(member){
           if(member.user.cellPhone == undefined){
              $scope.noCellPhone=true;
              alert("Selected Member has no cellPhone cant be default member");
              member.default=false;
              return;
           }
            $scope.account.members.forEach(function(m){
                m.default=false;
            });
            member.default=true;
    }
    $scope.showAddModal=function(){
         $scope.memberToAdd={};
         $scope.newMemberModal=true;
    };
    $scope.cancelAddModal=function(){
          $scope.newMemberModal=false;
    };

    $scope.showOrganizationModal=function(){
       $scope.newOrganizationModal=true;
    }

    $scope.cancelOrganizationModal=function(){
       $scope.newOrganization=undefined;
       $scope.newOrganizationModal=false;
    }

    $scope.createOrganization=function(){
      if($scope.newOrganization !== undefined && $scope.newOrganization.name !== undefined){
          $scope.newOrganizationModal=false;
          SaveOrganization.update($scope.newOrganization,function(data){
                 $scope.newOrganization=undefined;
                $scope.loadOrganizations();
                $scope.organizationCreated=true;
                $timeout(function(){
                   $scope.organizationCreated=false;
                },1000);
          });
      }
    };
    $scope.loadOrganizations=function(){
         Organization.get({}, function (data) {
             if(data.organizations !== null)
                 $scope.organizations=data.organizations;
          });
    }


    $scope.addAccountMember=function(){
        var member={};
        member.user=$scope.memberToAdd;
         $scope.account.members.push(member);
         $scope.memberToAdd={};
         $scope.newMemberModal=false;
    };


    $scope.cancel=function(){
          $location.path('/search');
    };

    $scope.validateUserName = function () {
        $scope.userNameInvalid = $scope.client.userName !== null && $scope.client.userName.trim().indexOf(' ') >= 0;
    };

     $scope.removeMember=function(member)
        {
             var callBack=function(results){
                if(results){
                    var index = $scope.account.members.indexOf(member);
                    $scope.account.members.splice(index, 1);
                 }
             };
             var options = {
                  id: "confirmDialog",
                  header: "label.confirm.remove.product.action",
                  body: "msg.question.remove.product.confirmation"
               };
               OpenLmisDialog.newDialog(options, callBack, $dialog);

        };


    $scope.confirmFacilityDelete = function () {
        var dialogOpts = {
            id: "deleteFacilityModal",
            header: 'delete.facility.header',
            body: 'confirm.programRole.deletion'
        };
        OpenLmisDialog.newDialog(dialogOpts, $scope.clearSelectedFacility, $dialog);
    };

    $scope.getMessage = function (key) {
        return messageService.get(key);
    };


}

UserController.resolve = {

    user: function ($q, Users, $route, $timeout) {
        var userId = $route.current.params.userId;
        if (!userId) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            Users.get({id: userId}, function (data) {
                deferred.resolve(data.user);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },


    accountTypes: function ($q, GetAccountType, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetAccountType.get({}, function (data) {
                deferred.resolve(data.types);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },

    countries: function ($q, GetCountries, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetCountries.get({}, function (data) {
                deferred.resolve(data.countries);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },


    accountPackages: function ($q, GetAccountPackages, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {

            GetAccountPackages.get({}, function (data) {
                if(data.allPackages !== null)
                    deferred.resolve(data.allPackages);
                else
                    return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    },
     organizations: function ($q, Organization, $route, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {

            Organization.get({}, function (data) {
                if(data.organizations !== null)
                    deferred.resolve(data.organizations);
                else
                    return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    }
};
