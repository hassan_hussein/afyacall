function ListAccountPackageController($scope,GetAccountPackages,$location,DeleteAccountPackage){
    $scope.accountPackages = [];
    GetAccountPackages.get({}, function(data){
        if(!isUndefined(data.allPackages)){
            $scope.accountPackages = data.allPackages;
        }
    });



    var deleteSuccessFunc = function (data) {
        console.log(data);
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.accountPackages = data.packages;
        $scope.showError = false;
        $location.path('');

    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = " Can not be deleted ";
        $scope.showError = true;
    };

    $scope.delete = function (packageId) {
        if (!packageId) return;
        DeleteAccountPackage.delete({id: parseInt(packageId, 10)}, deleteSuccessFunc, error);
    };


}
