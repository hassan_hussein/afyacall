/**
 * Created by hassan on 4/18/16.
 */

function DashBoardController($scope, $filter,TestsResults,Tests,$q, $timeout,GetYearTests,GetYearResults) {

    $scope.defaultYear=new Date().getFullYear();
    $scope.defaultMonth=0; //new Date().getMonth() +1;


    var getDateRange=function (year,month){
             var dateRange={};
              var lastDate = new Date(year, month, 0);
             $scope.selectMonth=_.findWhere($scope.monthsToDisplay,{id:month});
             if(month >=1 && month <=9)
             {
                 dateRange.startDate='01-0'+month+'-'+year;
                 dateRange.endDate=lastDate.getDate()+'-0'+month+'-'+year;
             }
             else if(month >9)
             {
                dateRange.startDate='01-'+month+'-'+year;
                dateRange.endDate=lastDate.getDate()+'-'+month+'-'+year;
             }
             else{
                 dateRange.startDate='01-01-'+year;
                 dateRange.endDate='31-12-'+year;
                 $scope.selectMonth={"name":"Jan-Dec", "id":0};
             }
             return dateRange;

    };

    var applyFilter=function(filter){

       //if by lab

        if(filter.location === undefined || filter.location === 'all')
        {
              $scope.allData=$scope.staticAllData;

        }else{
             var newAllData=[];
             $scope.staticAllData.forEach(function(yearData){
                  var newData={};
                  newData.year=yearData.year;

                  //filter tests
                  var testsByLab=_.groupBy(yearData.tests,function(t){
                          return t.Facility;
                  });

                  var testsByLabArray = $.map(testsByLab, function(value, index) {
                      return [{"location":index.toLowerCase(),"tests":value}];
                  });

                  var labTests=_.findWhere(testsByLabArray, {"location":filter.location});

                  newData.tests=(labTests !== undefined)?labTests.tests:[];

                 //filter results
                  var resultsByLab=_.groupBy(yearData.results,function(r){
                      return r.Location;
                  });

                  var resultsByLabArray = $.map(resultsByLab, function(value, index) {
                      return [{"location":index.toLowerCase(),"results":value}];
                  });

                  var labResults=_.findWhere(resultsByLabArray, {"location":filter.location});
                  newData.results=(labResults !== undefined)?labResults.results:[];
                  newAllData.push(newData);

             });
             $scope.allData=newAllData;


        }



        if($scope.allData !== undefined)
        {
            var byYear=_.findWhere($scope.allData,{year:filter.year});

            var sDate=new Date(filter.dateRange.startDate.split("-").reverse().join("-")).getTime();
            var eDate=new Date(filter.dateRange.endDate.split("-").reverse().join("-")).getTime();

            $scope.totalTestDone=_.filter(byYear.tests,function(test){
                var tDate=new Date(test.RegisteredDate.split("/").reverse().join("-")).getTime();
                return tDate >=sDate && tDate <=eDate;
            });
          // console.log(new Date((byYear.results[0].Timestamp.date).substring(0, 10)));
            $scope.totalResults=_.filter(byYear.results,function(result){
                  // console.log(new Date(result.Timestamp.date));
                   var rDate=new Date((result.Timestamp.date).substring(0, 10)).getTime();

                   return (rDate !==NaN && rDate >=sDate && rDate <=eDate && result.AuthorisedDateTime !=="N/A" && result.AuthorisedDateTime !==null);
            });



            $scope.totalRequests=_.filter(byYear.results,function(result){
                   var rDate=new Date((result.Timestamp.date).substring(0, 10)).getTime();
                   return rDate >=sDate && rDate <=eDate;
            });

        }
    };

    var nationalStatisticCallback=function(filter){
        if($scope.allData !== undefined){
            //get commulative
            var cummulativeTests=0;
            $scope.nationStatistics=[];

            $scope.allData.forEach(function(yearData){
                cummulativeTests=cummulativeTests+yearData.tests.length;
            });
            var statistic={"name":"Cummulative Tests", "value":cummulativeTests};
            $scope.nationStatistics.push(statistic);

            //ART
            var statistic={"name":"Total Patients on ART (By Sept 2015)", "value":731016};
            $scope.nationStatistics.push(statistic);

            //Total Test Done
            var statistic={"name":"Total Tests Done    ( "+$scope.selectMonth.name+" )","value":$scope.totalTestDone.length};
            $scope.nationStatistics.push(statistic);

            //
            //var statistic={"name":"Total Tests Done For Client on 1st Line Month testdone  ( "+$scope.selectMonth.name+" )","value":15000};
           // $scope.nationStatistics.push(statistic);

            //var statistic={"name":"Total Tests Done Month  For Client on 2nd Line Month testdone  ( "+$scope.selectMonth.name+" )","value":15000};
           // $scope.nationStatistics.push(statistic);

            //Suspected failure
            var resultsWithCPL=_.where($scope.totalResults,{TestDescription:"HIV-VIRAL LOAD (copies/mL"});
            var targetNotDetected=[];
            resultsCPLGreaterThan1000= _.filter(resultsWithCPL,
            function(r) {
                   var testResult=r.TestResult.replace(/\D/g,'');
                       testResult=testResult.trim();
                       if(testResult ==='')
                       targetNotDetected.push(testResult);
                       return parseInt(testResult,10) > 1000;
            });
            var statistic={"name":"Suspected treatment failure (>1000cpl/ml)  ( "+$scope.selectMonth.name+" )","value":resultsCPLGreaterThan1000.length};
            $scope.nationStatistics.push(statistic);

            //Total Rejected
            var resultsRejected=_.filter($scope.totalRequests,function(result){
                   return result.Rejected.toLowerCase() === "yes";
            });
            var statistic={"name":"Total Rejected ( "+$scope.selectMonth.name+" )","value":resultsRejected.length};
            $scope.nationStatistics.push(statistic);

           //Total Repeated
            var resultsRepeated=_.filter($scope.totalRequests,function(result){
                   return result.Repeated > 0;
            });
            var statistic={"name":"Total Repeated test ( "+$scope.selectMonth.name+" )","value":resultsRepeated.length};
            $scope.nationStatistics.push(statistic);

            //Total site sending sample
            //TODO compare with request
            var byFacility=_.groupBy($scope.totalRequests,function(r){
                               return r.FacilityName;
            });
            $scope.byFacility = $.map(byFacility, function(value, index) {
                 return [{"facility":index,"results":value}];
            });

            var statistic={"name":"Total sites sending sample  ( "+$scope.selectMonth.name+" )","value": $scope.byFacility.length};
            $scope.nationStatistics.push(statistic);

            //Total ART site
            var statistic={"name":"Total ART site (by Dec 2015)","value":1523 };
            $scope.nationStatistics.push(statistic);
       }
    };

    $scope.resultIsLoading=false;
    var groupByRegion=function(filter){
         if($scope.totalResults !== undefined && $scope.totalResults.length >0)
         {
              var byRegion=_.groupBy($scope.totalResults,function(d){
                  return d.Region;
              });
              $scope.testResultsByRegion = $.map(byRegion, function(value, index) {
                   return [{"region":index,"tests":value}];
              });
         }
    };
    var testsByAgeCallBack=function(filter)
    {

         var byAge=_.countBy($scope.totalResults,function(r){
                if(parseInt(r.AgeInYears,10) < 15)
                return 'lessThan15';
                else if (parseInt(r.AgeInYears,10) >= 15)
                return "adults";
                else
                return 'noAge';
         });
         $scope.testByAge.dataPoints=[byAge];
    };
    var labPerformanceCallback=function(filter){

         var requestByLab=_.groupBy($scope.totalRequests,function(r){
              return r.TestedAt;
         });

         $scope.requestByLab = $.map(requestByLab, function(value, index) {
             return [{"name":index,"requests":value}];
          });

         var resultsByLab=_.groupBy($scope.totalResults,function(r){
              return r.TestedAt;
         });
         $scope.resultsByLab = $.map(resultsByLab, function(value, index) {
             return [{"name":index,"results":value}];
          });
         $scope.resultsByLab.results=($scope.resultsByLab !==undefined)?$scope.resultsByLab.results:[];

         var testsByLab=_.groupBy($scope.totalTestDone,function(t){
              return t.Facility;
         });

         $scope.testsByLab = $.map(testsByLab, function(value, index) {

              var r=_.findWhere($scope.requestByLab,{name:index});
              var request=(r !== undefined)?r.requests:[];
              var res=_.findWhere($scope.resultsByLab,{name:index});
              results=(res !== undefined)?res.results:[];
              var pending=parseInt(request.length,10) - parseInt(results.length,10);
             return [{"name":index,"request":request.length,"test":results.length,"pending":value.length}];
         });

         $scope.labPerformance.dataPoints=$scope.testsByLab;

    };
    var testBySampleCallBack=function(filter){
         if($scope.allData !== undefined){

            var bySampleType=_.groupBy($scope.totalRequests,function(r){
                 return r.SpecimenType;
           });
           $scope.bySampleType = $.map(bySampleType, function(value, index) {
                 var resultsRepeated=_.filter(value,function(result){
                    return result.Repeated > 0;
                 });
                 return [{"sample":index,"request":value.length,"repeated":resultsRepeated.length}];
           });
           $scope.testBySample.dataPoints=$scope.bySampleType;
         }
    };
    var testBreakDownCallBack=function(){
        if($scope.allData !== undefined){
            var dataPoints=[];
            $scope.allData.forEach(function(data){
                var yearDataPoint={};
                yearDataPoint.year=data.year;
                yearDataPoint.test=data.tests.length;

               resultsWithCPL=_.where(data.results,{TestDescription:"HIV-VIRAL LOAD (copies/mL"});
                var targetNotDetected=[];
              resultsCPLGreaterThan1000= _.filter(resultsWithCPL,
                  function(r) {
                        var testResult=r.TestResult.replace(/\D/g,'');
                                         testResult=testResult.trim();
                                         if(testResult ==='')
                                         targetNotDetected.push(testResult);
                                         return parseInt(testResult,10) > 1000;
              });

              resultsCPLLessThan1000= _.filter(resultsWithCPL,
                  function(r) {
                        var testResult=r.TestResult.replace(/\D/g,'');
                                         testResult=testResult.trim();
                                         return parseInt(testResult,10) <= 1000;
              });

              yearDataPoint.test1000=resultsCPLGreaterThan1000.length;
              if(yearDataPoint.test > 0 || yearDataPoint.test1000 >0)
               dataPoints.push(yearDataPoint);
            });
            $scope.testBreakDown.dataPoints=dataPoints;
        }
    }
    $scope.resultIsLoading=false;
    $scope.init=true;

    $scope.filterChange=function(filter){
         if(!$scope.init){
             $scope.resultIsLoading=false;
         };
        if(filter.year !== undefined && filter.month !== undefined && !$scope.resultIsLoading){

            $scope.resultIsLoading=true;
            var dateRange=getDateRange(filter.year,filter.month);
            filter.dateRange=dateRange;
            $scope.filter=filter;
            angular.element('#loader').show();
            $timeout(function(){
               applyFilter($scope.filter);
               testBreakDownCallBack();
               nationalStatisticCallback($scope.filter);
               testsByAgeCallBack($scope.filter);
               labPerformanceCallback($scope.filter);
               testBySampleCallBack($scope.filter);
               groupByRegion($scope.filter);
               angular.element('#loader').hide();
            },100);

        }

    };
    $scope.data={};
    $scope.testBreakDown = {
              dataPoints:[],
              dataColumns: [
                           {"id": "test", "name":"All Test", "type": "bar"},
                           {"id": "test1000", "name":"> 1000cpl/ml", "type": "bar"}
              ],
              dataX: {"id": "year"}
      };

      $scope.testByAge = {
              dataPoints:[],
              dataColumns: [
                           {"id": "lessThan15", "name":"Less than 15 yrs", "type": "pie"},
                           {"id": "adults", "name":"Adults", "type": "pie"}
              ],
              dataX: {"id": "year"}
      };

     $scope.testTrend = {
         dataPoints:[{"month":"Jan", "test":1000,"suspected":300,"rejected":10},
                     {"month":"Feb", "test":1100,"suspected":400,"rejected":15},
                     {"month":"Mar", "test":1200,"suspected":350,"rejected":10},
                     {"month":"Apr", "test":1000,"suspected":470,"rejected":9},
                     {"month":"May", "test":1300,"suspected":500,"rejected":9},
                     {"month":"Jun", "test":1500,"suspected":600,"rejected":11},
                     {"month":"Jul", "test":1600,"suspected":520,"rejected":10},
                     {"month":"Aug", "test":1400,"suspected":510,"rejected":9},
                     {"month":"Sep", "test":1600,"suspected":620,"rejected":9},
                     {"month":"Oct","test":1700,"suspected":700,"rejected":10},
                     {"month":"Nov", "test":1800,"suspected":800,"rejected":10},
                     {"month":"Dec", "test":1900,"suspected":900,"rejected":9}],
         dataColumns: [
                   {"id": "test", "name":"Results", "type": "line"},
                   {"id": "suspected", "name":"Suspected treatment failure", "type": "line"},
                   {"id": "rejected", "name":"Rejected", "type": "line"}
         ],
                  dataX: {"id": "month"}
     };


$scope.nationStatistics=[];

    $scope.labPerformance={
               dataPoints:[],
                dataColumns: [
                                  {"id": "request", "name":"Requests", "type": "bar"},
                                  {"id": "test", "name":"Tests", "type": "bar"},
                                  {"id": "pending", "name":"Pending Tests", "type": "bar"}
                        ],
               dataX: {"id": "name"}
             };

  $scope.testBySample = {
                dataPoints:[],
                dataColumns: [
                             {"id": "request", "name":"All Test", "type": "bar"},
                             {"id": "repeated", "name":"Repeated", "type": "bar"}
                ],
                dataX: {"id": "sample"}
  };

     var loadData=function () {
                var qAll = $q.defer();
                var fetchData=function(year){
                     var deferred = $q.defer();
                     var yearTests={};
                     yearTests.year=year;
                     GetYearTests.query({year:year},function (tests) {
                         GetYearResults.query({year:year}, function(results){
                            yearTests.tests=tests;
                            yearTests.results=results;
                            deferred.resolve(yearTests);
                         });
                     });

                   return deferred.promise;
                }
                var arrayFunctions=[];
                var thisYear=new Date().getFullYear();
                for(i=0;i <=1; i++)
                {
                   var year=thisYear-i;
                   arrayFunctions.push(fetchData(year));
                }
               $q.all(arrayFunctions).then(function(value) {
                        $scope.staticAllData=value;
                        $scope.allData=value;
                        applyFilter($scope.filter);
                        testBreakDownCallBack();
                        nationalStatisticCallback($scope.filter);
                        testsByAgeCallBack($scope.filter);
                        labPerformanceCallback($scope.filter);
                        testBySampleCallBack($scope.filter);
                        groupByRegion($scope.filter);
               }, function(reason) {
                        $scope.loadError=reason;
               });
      };
      loadData();

}

DashBoardController.resolve = {


};