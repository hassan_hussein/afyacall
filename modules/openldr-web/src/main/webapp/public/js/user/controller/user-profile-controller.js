function UserProfileController($scope,$timeout,$location,messageService,accountPackages,UserType, user,updateUserProfile,GetUserType,GetAccountPackages) {
    $scope.user=user;
    $scope.userTypes = UserType;
    $scope.accountPackages = accountPackages;

  /*  GetUserType.get({}, function (data) {

        $scope.userTypes = data.users;

    });

    GetAccountPackages.get({}, function(data){
        $scope.accountPackages = data.allPackages;
    });*/

   // $scope.client = [{"firstName":"Hussein", "lastName":"Hassan","mobileNumber":"2559991"}];

$scope.userProfile  = [];


    $scope.saveUser = function () {

        $scope.client.packageId = $scope.accountPackages[0].id;
        $scope.client.active = $scope.accountPackages[0].status;

        var successHandler = function (msgKey) {
            $scope.showError = false;
            $scope.error = "";
            //$scope.$parent.message = messageService.get(msgKey, $scope.client.firstName, $scope.client.lastName);
           // $scope.$parent.userId = $scope.client.id;
            //console.log(  $scope.$parent.message);
            $scope.message= "Profile Updated successfully";
            console.log($scope.message);
            //$location.path('');
        };

        var saveSuccessHandler = function (response) {
            $scope.client = response.user;
            successHandler(response.success);
        };

        var updateSuccessHandler = function (response) {
            //$scope.client = response.user;

           // alert(JSON.stringify(response));
            successHandler($scope.client);
        };

        var errorHandler = function (response) {
            $scope.showError = true;
            $scope.message = "";
            $scope.error = response.data.error;
        };

        var requiredFieldsPresent = function (user) {
            if ($scope.userProfileForm.$error.required) {
                $scope.error = messageService.get("form.error");
                $scope.showError = true;
                return false;
            } else {
                return true;
            }
        };

       // if (!requiredFieldsPresent($scope.client))  return false;
        if ($scope.client.email === "") {
            $scope.client.email = null;
        }
       // if ($scope.client.email) {
            updateUserProfile.update({id: 2}, $scope.client, updateSuccessHandler, errorHandler);
        //}

        //else {
      //  saveUserProfile.save({}, $scope.client, saveSuccessHandler, errorHandler);
        //}
        return true;
    };



    $scope.saveUserProfile = function(){

        $scope.client.packageId = $scope.accountPackages[0].id;
        $scope.client.active = $scope.accountPackages[0].status;
       console.log($scope.userProfileForm.$error.required);

        if ($scope.userProfileForm.$error.required) {

            $scope.showError = true;
            $scope.error = 'form.error';
            console.log($scope.error);
            return;
        }

        updateUserProfile.update({id:2},$scope.client,  function (data) {
           // alert("Profile Updated Successfully");
            $scope.message = "Profile Updated Successfully";
              //  console.log(data);
            $timeout(function(){ $scope.message = ""; }, 4000);

        }, function () {});

        $scope.error = false;
    };

    $scope.cancel = function(){
        $location.path('');
    };

    $scope.dateOptions = {

        changeYear: true,
/*
        changeMonth: true,
*/
        yearRange: '1900:-0',
        showOn: "both",
        dateFormat: 'dd-MM-yy'
/*
        inline: true,
*/
/*
        buttonImage: "/public/images/calendar-icon.png"
*/
/*
        buttonText: "Calendar"
*/

    };
}

UserProfileController.resolve = {

    user: function ($q, UserContext, $route, $timeout) {
      /*  var userId = $route.current.params.userId;
        if (!userId) return undefined;*/
        var deferred = $q.defer();
        $timeout(function () {
            UserContext.get({}, function (data) {
                deferred.resolve(data);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    },

    UserType: function ($q, GetUserType, $timeout) {
        var deferred = $q.defer();

        $timeout(function () {
            GetUserType.get({}, function (data) {
                deferred.resolve(data.users);
            }, function () {
            });
        }, 100);

        return deferred.promise;
    },


    accountPackages: function ($q, GetAccountPackages, $route, $timeout) {
        /*  var userId = $route.current.params.userId;
         if (!userId) return undefined;*/
        var deferred = $q.defer();
        $timeout(function () {

            GetAccountPackages.get({}, function (data) {
                if(data.allPackages !== null)
                deferred.resolve(data.allPackages);
                else
                return [];
            }, function () {
            });
        }, 100);
        return deferred.promise;

    }
};
