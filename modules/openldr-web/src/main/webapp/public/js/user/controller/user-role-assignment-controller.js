function UserRoleAssignmentController($scope, $dialog, messageService) {

    $("#adminRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteAdminRolesModal",
                header: "create.user.deleteAdminRoleHeader",
                body: "create.user.deleteAdminRoles"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreAdminRole, $dialog);

            window.lastAdminRoleRemoved = e.removed;
        }
    });

    $("#reportRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteReportRolesModal",
                header: messageService.get("create.user.deleteReportRoleHeader"),
                body: messageService.get("create.user.deleteReportRoles")
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreReportRole, $dialog, messageService);

            window.lastReportRoleRemoved = e.removed;
        }
    });

    $scope.restoreReportRole = function (result) {
        if (result) return;

        if (window.lastReportRoleRemoved) {
            $scope.user.reportingRoles.roleIds.push(window.lastReportRoleRemoved.id);
        }
    };

    $scope.restoreAdminRole = function (result) {
        if (result) return;

        if (window.lastAdminRoleRemoved) {
            $scope.user.adminRole.roleIds.push(window.lastAdminRoleRemoved.id);
        }
    };

    $scope.deleteCurrentRow = function (rowNum, roleList) {
        var dialogOpts = {
            id: "deleteRolesModal",
            header: "create.user.deleteRoles",
            body: getBodyMsgKey(roleList)
        };

        OpenLmisDialog.newDialog(dialogOpts, $scope.deleteRole, $dialog);
        $scope.rowNum = rowNum;
        $scope.deleteRoleList = roleList;
    };

    $scope.deleteRole = function (result) {
        if (!result) return;

        $scope.user[$scope.deleteRoleList].splice($scope.rowNum, 1);
        $scope.rowNum = $scope.deleteRoleList = null;
    };

}