

function NavigationController($scope, ConfigSettingsByKey, localStorageService, Locales, $location, $window) {

  ConfigSettingsByKey.get({key: 'LOGIN_SUCCESS_DEFAULT_LANDING_PAGE'}, function (data){
    $scope.homePage =  data.settings.value;
  });

  //$scope.homePage = '/public/pages/dashbord/index.html/#dashbord';
      $scope.loadRights = function () {
    $scope.rights = localStorageService.get(localStorageKeys.RIGHT);

    $(".navigation > ul").show();
  }();

  $scope.showSubmenu = function () {
    $(".navigation li:not(.navgroup)").on("click", function () {
      $(this).children("ul").show();
    });
  }();

  $scope.hasReportingPermission = function () {
    if ($scope.rights !== undefined && $scope.rights !== null) {
      var rights = JSON.parse($scope.rights);
      var rightTypes = _.pluck(rights, 'type');
      return rightTypes.indexOf('REPORTING') > -1;
    }
    return false;
  };
$scope.homeLinkClicked=function(){
    $window.location.href= $scope.homePage;
};
  $scope.hasPermission = function (permission) {
    if ($scope.rights !== undefined && $scope.rights !== null) {
      var rights = JSON.parse($scope.rights);
      var rightNames = _.pluck(rights, 'name');
      return rightNames.indexOf(permission) > -1;
    }
    return false;
  };

  $scope.goOnline = function () {
    Locales.get({}, function (data) {
      if (data.locales) {
        var currentURI = $location.absUrl();
        if (currentURI.endsWith('offline.html')) {
          $window.location = currentURI.replace('public/pages/offline.html', '');
        }
        else {
          $window.location = currentURI.replace('offline.html', 'index.html').replace('#/list', '#/manage');
        }
        $scope.showNetworkError = false;
        return;
      }
      $scope.showNetworkError = true;
    }, {});
  };
}
