function TopTilesController($scope,$window,TopTiles,$rootScope,$timeout,localStorageService,$stomp) {

TopTiles.get(function(data){
  $scope.topTiles=data.topTiles;
  console.log($scope.topTiles);
});
$scope.userId = localStorageService.get(localStorageKeys.USER_ID);
$scope.userName = localStorageService.get(localStorageKeys.USER_ID);
$scope.totalDoctorsOnline=0;
  $stomp
        .connect('/wsOnline', null)
                 .then(function (frame) {
                  $timeout(function(){
                    var userOnline = $stomp.subscribe('/topic/online', function (payload, headers, res) {
                                          $scope.userOnline=payload;
                                           groupByCategory();
                                       }, {})

                        $stomp.send('/app/wsOnline', {
                              'name': '','userId':$scope.userId,'status':'ONLINE'
                         }, {})

                  },900);

     });
   $stomp
         .connect('/ws', null)
                  .then(function (frame) {
                   $timeout(function(){
                       var connectedUser = $stomp.subscribe('/topic/connected-client', function (payload, headers, res) {
                                              $scope.connectedUser=payload;

                                              if($scope.userId == payload.agentId)
                                              {
                                                 $rootScope.consultationId=payload.id;
                                                 $rootScope.showConsultationModal();
                                                 $scope.$apply();
                                                 console.log($rootScope.consultationId);
                                              }
                                           }, {});
                   },900);


      });

$stomp
      .connect('/wsForward', null)
               .then(function (frame) {
               console.log(frame);
               $timeout(function(){
                     var newQueue = $stomp.subscribe('/topic/forward', function (payload, headers, res) {
                                                             $scope.newQueue=payload;
                                                             console.log($scope.newQueue);
                                                             $rootScope.newQueueFound=true;
                                                             $rootScope.doctorId=$scope.userId;
                                                             $scope.$apply();
                     }, {});
               },900);

   });



   var groupByCategory=function(){
     console.log($scope.userOnline);
     var me=_.findWhere($scope.userOnline.agents,{id:parseInt($scope.userId,10)});
      console.log(me);
     if(me !== undefined && me.status == "OFFLINE"){
          console.log("Should offline");
       $window.location = "/public/pages/login.html";
       }

     if( $scope.userOnline !== undefined)
     {
        if( $scope.userOnline.agents.length > 0)
        {
           var groupByCategory=_.groupBy($scope.userOnline.agents, function(agent){
              return agent.jobTitle;
           });

           $scope.groupByCategoryArray=$.map(groupByCategory, function(value, index){
              return [{"category":index, "users":value}];
           });

           var doctors=_.findWhere($scope.groupByCategoryArray,{category:'DOCTOR'});

           var groupDoctorsByStatus=_.groupBy(doctors.users,function(doctor){
              return doctor.status;
           });

           $scope.doctorOnlineArray=$.map(groupDoctorsByStatus,function(value,index){
              return [{"status":index,"doctors":value}];
           });

           var onlineDoctors=_.findWhere($scope.doctorOnlineArray,{status:'ONLINE'});
           if(onlineDoctors !== undefined)
           {
            $rootScope.onlineDoctors=onlineDoctors.doctors;
            $scope.totalDoctorsOnline=$rootScope.onlineDoctors.length;
             $scope.$apply();
           }

        }
     }
  };


  $scope.loadRights = function () {
      $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
  }();

  $scope.hasPermission = function (permission) {
              if ($scope.rights !== undefined && $scope.rights !== null) {
                var rights = JSON.parse($scope.rights);
                var rightNames = _.pluck(rights, 'name');
                return rightNames.indexOf(permission) > -1;
              }
              return false;
  };

}

TopTilesController.resolve={};
