var siteServices=angular.module('site.services', ['ngResource']);
var update = {update: {method: 'PUT'}};

siteServices.factory('RegisterClient', function ($resource) {
  return $resource('/site/register/client.json', {},update);
});

siteServices.factory('GetByUsername', function ($resource) {
  return $resource('/site/client/get-by-username.json', {},{});;
});

siteServices.factory('GetByCellPhone', function ($resource) {
  return $resource('/site/client/get-by-cellphone.json', {},{});;
});

siteServices.factory('GetByEmail', function ($resource) {
  return $resource('/site/client/get-by-email.json', {},{});;
});

siteServices.factory('VerifyAccount', function ($resource) {
  return $resource('/site/client/verify.json', {},{});;
});