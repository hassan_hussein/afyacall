services.factory('messageService', function (Messages, localStorageService, $rootScope, version) {

  var populate = function () {
    if (localStorageService.get('version') != version) {
      localStorageService.add('version', version);
      Messages.get({}, function (data) {
        for (var attr in data.messages) {
          localStorageService.add('message.' + attr, data.messages[attr]);
        }
        $rootScope.$broadcast('messagesPopulated');
      }, {});
    }
  };

  var get = function () {
    var keyWithArgs = Array.prototype.slice.call(arguments);
    var displayMessage =  localStorageService.get('message.' + keyWithArgs[0]);
    if(keyWithArgs.length > 1 && displayMessage) {
      $.each(keyWithArgs, function (index, arg) {
        if (index > 0) {
          displayMessage = displayMessage.replace("{" + (index-1) + "}", arg);
        }
      });
    }
    return displayMessage || keyWithArgs[0];
  };

  return{
    populate:populate,
    get:get
  };
});
