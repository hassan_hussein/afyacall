/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

function HeaderController($scope,OnlineLogOut,LoggedInUser,$q,$timeout,localStorageService,UserContext, loginConfig, ConfigSettingsByKey, $window) {


  LoggedInUser.get({}, function(data){
    $scope.prof = data.user.firstName;
  });


  var userProfile = function(q, scope,timeout,UserContext) {
    var deferred = q.defer();
    timeout(function () {
      UserContext.get({}, function(data){
        deferred.resolve(data);
      });

    },100);
    var promise = deferred.promise;

    promise.then(function(message) {
      scope.message = message;
      $scope.userInfo = message.users;

      $scope.myColor= getRandomColor(message.users);
     // console.log(getRandomColor(message.users));
    /*  $scope.colors = scope.color;*/
      $scope.numbers = [{"id":2, "name":"19"}];
      $scope.firstLetter = message.users.firstName.charAt(0).toUpperCase();

    });
    return promise;
  };
   $scope.u = function(){
     return 'M';
   };
   userProfile($q,$scope,$timeout,UserContext);

  function getRandomColor(users) {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 10; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return  color;
  }


  $scope.loginConfig = loginConfig;
  $scope.user = localStorageService.get(localStorageKeys.FULLNAME);
  $scope.userId = localStorageService.get(localStorageKeys.USER_ID);


  $scope.logout = function () {
//    OnlineLogOut.update({userId:$scope.userId,status:"OFFLINE"},function(data){
//
//    });


    localStorageService.remove(localStorageKeys.RIGHT);
    localStorageService.remove(localStorageKeys.USERNAME);
    localStorageService.remove(localStorageKeys.USER_ID);
    localStorageService.remove(localStorageKeys.FULLNAME);

    $.each(localStorageKeys.REPORTS, function(itm,idx){

          localStorageService.remove(idx);
      });
      $.each(localStorageKeys.PREFERENCE, function(item, idx){
          localStorageService.remove(idx);

      });
      $.each(localStorageKeys.DASHBOARD_FILTERS, function(item, idx){
          localStorageService.remove(idx);

      });
   // document.cookie = 'JSESSIONID' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
    $window.location="";
    $window.location = "/j_spring_security_logout";

  };
}