function RegistrationController($scope,RegisterClient,VerifyAccount,$window, GetByUsername,GetByCellPhone,GetByEmail) {

  $scope.checkUserName=function(){
    $scope.userNameExist=false;
      GetByUsername.get({userName:$scope.client.userName},function(data){
           if($scope.client.userName !== undefined)
           {
               if(data.user !== null)
               $scope.userNameExist=true;
           }
      });
  };
  $scope.checkCellPhone=function(){
      $scope.cellPhoneExist=false;
      GetByCellPhone.get({cellPhone:$scope.client.cellPhone},function(data){
           if($scope.client.cellPhone !== undefined)
           {
               if(data.user !== null)
               $scope.cellPhoneExist=true;
           }
      });
  };
  $scope.register=function(){
     if($scope.clientForm.$invalid || $scope.client.password != $scope.client.confirmPassword || $scope.userNameExist ||  $scope.cellPhoneExist)
     {
       $scope.showError=true;
       return;
     }

     $scope.registrationInProcess=true;
     $scope.showVerification=false;
     RegisterClient.update($scope.client,function(data){
       $scope.registrationInProcess=false;
       if(data.code ==="SUCCESS")
       {
          $scope.showVerification=true;
          $scope.verificationCode=data.code;
       }
       console.log(data);
     });
  };
  $scope.verifyAccount=function(){
      if($scope.token !== undefined){
           VerifyAccount.get({token:$scope.token}, function(data){
                if(data.verification==="SUCCESS")
                {

                    $scope.verificationSuccessful=true;
                    $scope.verificationCode=undefined;
                    $scope.verificationCode=false;
                    $window.location("/public/pages/login.html");
                }
                else{

                }
           });
      }
  };
}

RegistrationController.resolve={};