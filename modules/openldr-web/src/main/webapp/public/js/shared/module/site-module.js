var site = angular.module('site', ['site.services','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngRoute']);

site.config(['$routeProvider', function ($routeProvider) {
         $routeProvider.
             when('/home', {controller: HomeController, templateUrl: '/public/pages/site-home.html',reloadOnSearch:true, resolve: HomeController.resolve}).
             when('/registration', {controller: RegistrationController, templateUrl: '/public/pages/site-registration.html', resolve: RegistrationController.resolve}).
             otherwise({redirectTo: '/home'});
}]);
