
var services = angular.module('afyacall.services', ['ngResource']);
var update = {update: {method: 'PUT'}};

services.value('version', '@version@');

services.factory('Locales', function ($resource) {
  return $resource('/locales.json', {}, {});
});

services.factory('ChangeLocale', function ($resource) {
  return $resource('/changeLocale.json', {}, update);
});

services.factory('UserContext', function ($resource) {
  return $resource('/user-context.json', {}, {});
});

services.factory('Users', function ($resource) {
  var resource = $resource('/users/:id.json', {id: '@id'}, update);

  resource.disable = function (pathParams, success, error) {
    $resource('/users/:id.json', {}, {update: {method: 'DELETE'}}).update(pathParams, {}, success, error);
  };

  return resource;
});

services.factory('leaflet', function ($resource) {
  return $resource('/public/coordinate.json', {}, {});
});

services.factory('Authenticate', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/00000000000000000000000000000000/v1/json/authenticate', {}, {});
});

services.factory('Regions', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/regions', {}, {});
});

services.factory('DistrictsByRegion', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/districts', {}, {});
});

services.factory('FacilitiesByDistrict', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/facilities', {}, {});
});

services.factory('TestTypes', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/tests', {}, {});
});

services.factory('Tests', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/tat', {}, {});
});

services.factory('TestsResults', function ($resource) {
  return $resource('http://mohsw.kaga.co.tz/bcb911e686c642d666d869443a30bd77/v1/json/results', {}, {});
});

services.factory('MapData', function ($resource) {
  return $resource('/public/js/shared/directives/mapData.json', {}, {});
});

services.factory('GetYearTests', function ($resource) {
  return $resource('/public/data/tests/:year.json', {year:'@year'}, {});
});

services.factory('GetYearResults', function ($resource) {
  return $resource('/public/data/results/:year.json', {year:'@year'}, {});
});



services.factory('Messages', function ($resource) {
  return $resource('/messages.json', {}, {});
});

services.factory('ForgotPassword', function ($resource) {
  return $resource('/forgot-password.json', {}, {});
});

services.factory('ConfigSettingsByKey',function($resource){
  return $resource('/settings/:key.json',{},{});
});

services.factory('Rights', function ($resource) {
  return $resource('/rights.json', {}, {});
});

services.factory('Roles', function ($resource) {
  return $resource('/roles/:id.json', {id: '@id'}, update);
});

services.factory('RolesFlat', function ($resource) {
  return $resource('/roles-flat', {id: '@id'}, update);
});

services.factory('UpdatePassword', function ($resource) {
  return $resource('/admin/resetPassword/:userId.json', {}, update);
});

services.factory('updateUserProfile', function ($resource) {
  return $resource('/userProfile/:id.json', {id:'@id'},update);
});

services.factory('SaveDistribution', function ($resource) {
  return $resource('/vaccine/inventory/distribution/save.json', {}, {save:{method:'POST'}});
});

services.factory('GetUserType', function ($resource) {
  return $resource('/userType/getAll.json', {}, {});
});
services.factory('GetAccountPackages', function ($resource) {
  return $resource('/accountPackage/getAll.json', {}, {});
});

services.factory('SaveMedicalRecordTypes', function ($resource) {
  return $resource('/medical-record-type/save', {}, update);
});

services.factory('MedicalRecordTypes', function ($resource) {
  return $resource('/medical-record-type/get-all', {}, {});
});

services.factory('SaveCVTypes', function ($resource) {
  return $resource('/user-cv-type/save', {}, update);
});

services.factory('CVTypes', function ($resource) {
  return $resource('/user-cv-type/get-all', {}, {});
});

services.factory('Account', function ($resource) {
  return $resource('/account/get-by-reference-number', {}, {});
});

services.factory('CreateAccount', function ($resource) {
  return $resource('/account/save', {}, update);
});

services.factory('Organization', function ($resource) {
  return $resource('/organization/get-all', {}, {});
});

services.factory('SaveRights', function ($resource) {
  return  $resource('/rights/save.json', {}, update);

});

services.factory('UpdateRights', function ($resource) {
  return  $resource('/rights/:name.json', {name:'@name'},update);

});

services.factory('AllRights', function ($resource) {
  return $resource('/rights/allRights.json', {}, {});
});

services.factory('GetRightsByName', function ($resource) {
  return $resource('/rights/getByName/:name.json', {name:'@name'}, {});
});

services.factory('GeoLevels', function ($resource) {
  return $resource('/geographicLevels.json', {}, {});
});


services.factory('GeographicZones', function ($resource) {
  return $resource('/geographicZones/:id.json', {id: '@id'}, update);
});

services.factory("GeographicZoneSearch", function ($resource) {
  return $resource('/filtered-geographicZones.json', {}, {});
});


services.factory('GeographicZonesAboveLevel', function ($resource) {
  return $resource('/parentGeographicZones/:geoLevelCode.json', {}, {});
});

services.factory('FlatGeographicZoneList',function ($resource){
  return $resource('/reports//geographic-zones/flat.json', {}, {});
});

services.factory('GeographicLevels', function($resource) {
  return $resource('/geographicLevels.json',{},{});
});

services.factory('SaveGeographicInfo', function($resource){
  return $resource('/geographic-zone/save-gis.json',{}, {post:{method:'POST'}});
});

services.factory('GeographicLevelById', function($resource) {
  return $resource('/geographicLevelBy/:id.json',{id : "@id"},{});
});

services.factory('GeographicLevelsInfo', function ($resource) {
  var resource =  $resource('/geographicLevels/:id.json', {id: '@id'}, update);

  resource.delete = function (pathParams, success, error) {
    $resource('/geoLevel/delete/:id.json', {}, {update: {method: 'DELETE'}}).update(pathParams, {}, success, error);
  };

  return resource;

});

services.factory('DeleteGeoLevelInfo', function($resource){
  return $resource('/geoLevel/delete/:id',{},{update:{method:'DELETE'}});
});

services.factory('SaveRightsInfo', function ($resource) {
  return  $resource('/rights/:name.json', {name:'@name'},update);

});

services.factory('SaveRIGHTSInformation', function($resource){
  return $resource('/rights/save.json',{}, {post:{method:'POST'}});
});

services.factory('DeleteRights', function($resource){
  return $resource('/rights/delete/:name.json',{}, {update:{method:'DELETE'}});
});

services.factory('UpdateUserPassword', function ($resource) {
  return $resource('/user/resetPassword/:token.json', {}, update);
});

services.factory('ValidatePasswordToken', function ($resource) {
  return $resource('/user/validatePasswordResetToken/:token.json', {}, {});
});

services.factory('SaveUserTypes', function ($resource) {
  return $resource('/userType/userType.json', {}, {save:{method:'POST'}});
});

services.factory('UpdateUserTypes', function ($resource) {
  return $resource('/userType/userType/:id.json', {}, {update:{method:'PUT'}});
});

services.factory('GetUserTypeById', function($resource){
  return $resource('/userType/getUserTypesById/:id.json', {id:'@id'},{});
});

services.factory('DeleteClientType', function($resource){
  return $resource('/userType/userType/delete/:id.json',{}, {update:{method:'DELETE'}});
});

/*start of sms Factories*/

services.factory('SMSCompleteList',function($resource){
  return $resource('/sms/MessageList.json',{},{});
});

services.factory('GetSMSInfo', function($resource){
  return $resource('/sms/setDetails',{}, {get:{method:'GET'}});
});

services.factory('GetMessagesForMobile', function($resource){
  return $resource('/sms/MessagesForMobile',{}, {get:{method:'GET'}});
});

services.factory('GetReplyMessages', function($resource){
  return $resource('/sms/getSMS',{}, {get:{method:'GET'}});
});

/*End SMS data Factories*/


services.factory('DeleteExtensionNumbers', function($resource){
  return $resource('/extensionNumbers/delete/:id.json',{}, {update:{method:'DELETE'}});
});

services.factory('GetAllExtensionNumbers', function($resource){
  return $resource('/getAllExtensionNumbers.json',{},{});
});

services.factory('GetExtensionNumbersById', function($resource){
  return $resource('/extensionNumbersBy/:id.json',{id:'@id'},{});
});

services.factory('ExtensionNumbersInfo', function ($resource) {
 return $resource('/extensionNumbers/:id.json', {id: '@id'}, update);
});

services.factory('SaveExtensionNumbersInfo', function($resource){
  return $resource('/extensionNumbers.json',{}, {post:{method:'POST'}});
});

services.factory('UserGroups', function($resource){
  return $resource('/user/userGroups.json',{},{});
});

services.factory('LoggedInUser', function($resource){
  return $resource('/getLoggedInUser.json',{},{});
});
services.factory('GetAccountPackageById', function($resource){
  return $resource('/accountPackage/GetById.json',{},{});
});

services.factory('GetAccountPackageById', function($resource){
  return $resource('/accountPackage/GetById/:id.json',{id:'@id'},{});
});

services.factory('SaveAccountPackageInfo', function($resource){
  return $resource('/accountPackage/accountPackages.json',{}, {post:{method:'POST'}});
});

services.factory('UpdateAccountPackageInfo', function ($resource) {
  return $resource('/accountPackage/accountPackages/:id.json', {id: '@id'}, update);
});


services.factory('DeleteAccountPackage', function ($resource) {
  return $resource('/accountPackage/delete/:id.json', {}, {update: {method: 'DELETE'}});
});
services.factory('Queue', function($resource){
  return $resource('/consultation/get-queue/:doctorId.json',{doctorId:'@doctorId'},{});
});

services.factory('FieldTypes', function($resource){
  return $resource('/medical-record-type-field/get-field-types.json',{}, {});
});

services.factory('FieldGroups', function($resource){
  return $resource('/medical-record-type-field/get-field-groups.json',{}, {});
});

services.factory('OnlineLogOut', function($resource){
  return $resource('/message/logout/:userId/:status.json',{userId:'@userId',status:'@status'},update);
});

services.factory('GetUserConsultationRecords', function($resource){
  return $resource('/consultation/get-user-consultation-records/:consultationId.json',{consultationId:'@consultationId'},{});
});

services.factory('ForwardConsultation', function($resource){
  return $resource('/consultation/forward.json',{},update);
});

services.factory('SaveConsultation', function($resource){
  return $resource('/consultation/save.json',{},update);
});

services.factory('GetAccountType', function ($resource) {
  return $resource('/account/types.json', {}, {});
});

services.factory('GetCountries', function ($resource) {
  return $resource('/geographic-zone/get-countries.json', {}, {});
});

services.factory('GetRegions', function ($resource) {
  return $resource('/geographic-zone/get-regions.json', {}, {});
});

services.factory('GetDistricts', function ($resource) {
  return $resource('/geographic-zone/get-districts.json', {}, {});
});


services.factory('GetLandingPages', function ($resource) {
  return $resource('/landingPages/getAll.json', {}, {});
});

services.factory('GetLandingPageById', function ($resource) {
  return $resource('/landingPageGetBy/:id.json', {}, {});
});


services.factory('SaveLandingPage', function($resource){
  return $resource('/landingPages.json',{},{post:{method:'POST'}});
});

services.factory('UpdateLandingPage', function ($resource) {
  return $resource('/landingPages/:id.json', {id: '@id'}, update);
});

services.factory('DeleteLandingPage', function ($resource) {
  return $resource('/landingPages/delete/:id.json', {id: '@id'}, update);
});

services.factory('Members', function ($resource) {
  return $resource('/account/get-members', {}, {});
});

services.factory('SupportedUploads', function ($resource) {
  return $resource('/supported-uploads.json', {}, {});
});

services.factory('Settings',function($resource){
  return $resource('/settings.json',{},{});
});

services.factory('SettingsByKey',function($resource){
  return $resource('/settings/:key.json',{},{});
});

services.factory('SettingUpdator', function($resource){
  return $resource('/saveSettings.json', {} , { post: {method:'POST'} } );
});

services.factory('SendInvoiceSMS', function($resource){
  return $resource('/account/send-invoice-sms.json', {} , {} );
});

services.factory('SendPINSMS', function($resource){
  return $resource('/account/send-pin-sms.json', {} , {} );
});

services.factory('SaveOrganization', function ($resource) {
  return $resource('/organization/save', {}, update);
});

services.factory('TopTiles', function($resource){
  return $resource('/account/top-tiles.json', {} , {} );
});
