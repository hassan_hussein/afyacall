var consultation = angular.module('consultation', ['afyacall','ngGrid', 'ui.bootstrap.modal','ngLetterAvatar', 'ui.bootstrap.dialog','ui.bootstrap.pagination','ngStomp']);

consultation.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/search', {controller: ConsultationController, templateUrl: '/public/pages/consultation/partials/search.html', resolve: ConsultationController.resolve}).
        otherwise({redirectTo: '/search'});
}])

    .directive('onKeyup', function () {
        return function (scope, elm, attrs) {
            elm.bind("keyup", function () {
                scope.$apply(attrs.onKeyup);
            });
        };
    })
    .directive('select2Blur', function () {
        return function (scope, elm, attrs) {
            angular.element("body").on('mousedown', function (e) {
                $('.select2-dropdown-open').each(function () {
                    if (!$(this).hasClass('select2-container-active')) {
                        $(this).data("select2").blur();
                    }
                });
            });
        };
    }).directive("datepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "dd/mm/yy",
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                elem.datepicker(options);
            }
        }
    })
 .run(function () {


 });

