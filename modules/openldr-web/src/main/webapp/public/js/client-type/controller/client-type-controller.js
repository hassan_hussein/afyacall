function ClientTypeController($scope,$location,UpdateUserTypes,GetUserTypeBy,messageService,SaveUserTypes){

   $scope.clientTypes = {};
   $scope.clientTypes  = GetUserTypeBy;

    $scope.cancel = function(){
        $location.path('/ ');
    };

    var success = function (data) {
        $scope.error = "";
        $scope.$parent.message = data.success;
        console.log(data);
        $scope.$parent.userTypeId = data.success;
        $scope.showError = false;
        $location.path('');
    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.saveClientTypes = function () {

        if ($scope.clientTypeForm.$error.userType || $scope.clientTypeForm.$error.maximumMembers || $scope.clientTypeForm.$error.required) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }


        if ($scope.clientTypes.id) {
            UpdateUserTypes.update({id: $scope.clientTypes.id}, $scope.clientTypes, success, error);
        }
        else {
            SaveUserTypes.save({}, $scope.clientTypes, success, error);
        }

        return true;
    };



}


ClientTypeController.resolve ={

    GetUserTypeBy: function ($q, GetUserTypeById, $route, $timeout) {
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetUserTypeById.get({id: parseInt(id,10)}, function (data) {
                deferred.resolve(data.allById);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }
};
