angular.module('client_type', ['afyacall', 'ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/create', {controller:ClientTypeController, templateUrl:'partials/create.html', resolve:ClientTypeController.resolve}).
            when('/list', {controller:ListClientTypeController, templateUrl:'partials/list.html'}).
            when('/edit/:id', {controller:ClientTypeController, templateUrl:'partials/create.html', resolve:ClientTypeController.resolve}).
            otherwise({redirectTo:'/list'});
    }]).run(function($rootScope, AuthorizationService) {
        $rootScope.clientTypeSelected = "selected";
        //AuthorizationService.preAuthorize('MANAGE_ROLE');
    });

