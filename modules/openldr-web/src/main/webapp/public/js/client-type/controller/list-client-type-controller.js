function ListClientTypeController($scope,GetUserType,DeleteClientType,$location){

    $scope.clientTypes = [];

    GetUserType.get({},function(data){
        if(!isUndefined(data)){
            $scope.clientTypes = data.users;
        }
    });


    var deleteSuccessFunc = function (data) {
        $scope.$parent.message = "";
        $scope.$parent.userTypeId = null;
        $scope.$parent.message = data.success;
        $scope.$parent.deleteUserTypeItem = true;
        $scope.showError = false;

        $location.path('/');
    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = " Can not be deleted ";
        $scope.showError = true;
    };

    $scope.delete = function (result) {
        if (!result) return;
        DeleteClientType.delete({id: result}, deleteSuccessFunc, error);
    };
}
