
angular.module('rights', ['afyacall', 'ngStomp','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/create', {controller:RightsController, templateUrl:'partials/create.html', resolve:RightsController.resolve}).
            when('/list', {controller:ListRightsController, templateUrl:'partials/list.html'}).
            when('/edit/:name', {controller:RightsController, templateUrl:'partials/create.html', resolve:RightsController.resolve}).
            otherwise({redirectTo:'/list'});
    }]).run(function($rootScope, AuthorizationService) {
        $rootScope.rightsSelected = "selected";
        //AuthorizationService.preAuthorize('MANAGE_ROLE');
    });