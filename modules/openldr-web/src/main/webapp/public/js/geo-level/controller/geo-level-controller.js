function GeoLevelController($scope,GeoLevelById,$location,GeographicLevelsInfo){
    $scope.$parent.message = " ";
    $scope.geoLevel = [];

    $scope.geoLevel = GeoLevelById;

    $scope.cancel = function(){
        $scope.$parent.geoLevelId = null;
        $scope.$parent.deleteGeoLevelItem = false;
        $location.path('/');
    };

    var success = function (data) {
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.geoLevelId = data.geoLevel.id;
        $scope.$parent.deleteGeoLevelItem = false;
        $scope.showError = false;
        $location.path('');

    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.save = function () {
        if ($scope.geographicLevelForm.$error.name || $scope.geographicLevelForm.$error.levelNumber || $scope.geographicLevelForm.$error.required || $scope.geographicLevelForm.$error.code) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }

        if ($scope.geoLevel.id !== undefined ) {

            GeographicLevelsInfo.update({id: $scope.geoLevel.id}, $scope.geoLevel, success, error);
        }
        else {
            GeographicLevelsInfo.save({}, $scope.geoLevel, success, error);
        }
    };

}

GeoLevelController.resolve = {

    GeoLevelById: function ($q, GeographicLevelById, $route, $timeout) {
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GeographicLevelById.get({id: parseInt(id,10)}, function (data) {
                // if(!isUndefined(data.rights))
                deferred.resolve(data.geo_levels);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }
};