
function GeoZoneController($scope, geoLevels, geoZone, GeographicZonesAboveLevel, $location, GeographicZones) {
  $scope.levels = geoLevels;
  $scope.geoZone = geoZone;
  $scope.$parent.message = "";

  $scope.loadParents = function (levelCode) {
    GeographicZonesAboveLevel.get({geoLevelCode: levelCode}, function (data) {
      $scope.parentGeoZones = data.geographicZoneList;
      $scope.parentLevels = _.uniq(_.pluck(_.pluck($scope.parentGeoZones, 'level'), 'name'));
    }, {});
  };

  if ($scope.geoZone) {
    $scope.loadParents($scope.geoZone.level.code);
    $scope.editMode = true;
  }

  $scope.cancel = function () {
    $scope.$parent.geoZoneId = undefined;
    $scope.$parent.message = "";
    $location.path('#/search');
  };

  var success = function (data) {
    $scope.error = "";
    $scope.$parent.message = data.success;
    $scope.$parent.geoZoneId = data.geoZone.id;
    $scope.showError = false;
    $location.path('');
  };

  var error = function (data) {
    $scope.$parent.message = "";
    $scope.error = data.data.error;
    $scope.showError = true;
  };

  $scope.save = function () {
    if ($scope.geoZoneForm.$error.pattern || $scope.geoZoneForm.$error.required || !$scope.geoZone.level.code) {
      $scope.showError = true;
      $scope.error = 'form.error';
      $scope.message = "";
      return;
    }

    if (!$scope.parentLevels || $scope.parentLevels.length === 0) {
      $scope.geoZone.parent = undefined;
    }
    if ($scope.geoZone.id) {
      GeographicZones.update({id: $scope.geoZone.id}, $scope.geoZone, success, error);
    }
    else {
      GeographicZones.save({}, $scope.geoZone, success, error);
    }
  };
}

GeoZoneController.resolve = {
  geoLevels: function ($q, $timeout, GeoLevels) {
    var deferred = $q.defer();
    $timeout(function () {
      GeoLevels.get({}, function (data) {
        deferred.resolve(data.geographicLevelList);
      }, {});
    }, 100);
    return deferred.promise;
  },

  geoZone: function ($q, $route, $timeout, GeographicZones) {
    if ($route.current.params.id === undefined) return undefined;

    var deferred = $q.defer();
    var geoZoneId = $route.current.params.id;

    $timeout(function () {
      GeographicZones.get({id: geoZoneId}, function (data) {
        deferred.resolve(data.geoZone);
      }, {});
    }, 100);
    return deferred.promise;
  }
};
