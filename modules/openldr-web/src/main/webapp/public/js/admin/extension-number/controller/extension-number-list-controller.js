function ExtensionNumberListController($scope,GetAllExtensionNumbers,DeleteExtensionNumbers,$location){
    $scope.extensions = [];
    GetAllExtensionNumbers.get({}, function(data){

        if(!isUndefined(data.allExtensionNumbers)){

            $scope.extensions = data.allExtensionNumbers;
        }
    });

    var deleteSuccessFunc = function (data) {
        console.log(data);
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.extension = data.extension_numbers;
        $scope.showError = false;
        $location.path('');

    };

    var error = function (data) {
        $scope.message = "";
        $scope.error = " Can not be deleted ";
        $scope.showError = true;
    };

    $scope.deleteExtensionNumber = function (numberId) {
        if (!numberId) return;
        DeleteExtensionNumbers.delete({id: parseInt(numberId, 10)}, deleteSuccessFunc, error);
    };

    $scope.edit = function(id){
        $location.path('edit/' + id);
    }

}
