

angular.module('sms', ['afyacall','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp']).
    config(['$routeProvider', function ($routeProvider) {
      $routeProvider.
        when('/list', {controller: SmsListController, templateUrl: 'partials/list.html'}).
        when('/create-sms', {controller: SmsController, templateUrl: 'partials/create.html'}).
        when('/create-sms/:userId', {controller: SmsController, templateUrl: 'partials/create.html'}).
        otherwise({redirectTo: '/list'});
    }]).run(function ($rootScope, AuthorizationService) {
        $rootScope.smsSelected = "selected";
    });