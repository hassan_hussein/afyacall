angular.module('extension_number', ['afyacall','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/list', {controller: ExtensionNumberListController, templateUrl: 'partials/list.html'}).
            when('/create-extension', {controller: ExtensionNumberController, templateUrl: 'partials/create.html', resolve:ExtensionNumberController.resolve}).
            when('/edit/:id', {controller: ExtensionNumberController, templateUrl: 'partials/create.html', resolve:ExtensionNumberController.resolve}).
            otherwise({redirectTo: '/list'});
    }]).run(function ($rootScope, AuthorizationService) {
        $rootScope.extensionNumberSelected = "selected";
    });