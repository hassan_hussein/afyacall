
function ListSettingController($scope, $location, Settings, SettingUpdator) {

  $scope.changeTab = function(tab){
    $scope.visibleTab = tab;
  };

  Settings.get(function (data){
     $scope.settings = data.settings;
    $scope.grouped_settings = _.groupBy($scope.settings.list,'groupName');

  });

  $scope.saveSettings = function(){
      SettingUpdator.post({}, $scope.settings, function (){
          $location.path('');
          $scope.$parent.message = "The configuration changes were successfully updated.";
      });
  };
}
