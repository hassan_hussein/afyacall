function LandingPageController($scope,$location,LandingPageBy,SaveLandingPage,UpdateLandingPage){

    $scope.landingPage = {};

    if(LandingPageBy !== null || LandingPageBy !== undefined)
        $scope.landingPage = LandingPageBy;

    var success = function (data) {
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.extension = data.geo_levels;
        $scope.showError = false;
        $location.path('');
    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.saveLandingPage = function () {


        if ($scope.landingPageForm.$error.name || $scope.landingPageForm.$error.url || $scope.landingPageForm.$error.required) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }


        if ($scope.landingPage.id) {
            UpdateLandingPage.update({id: $scope.landingPage.id}, $scope.landingPage, success, error);
        }
        else {
            SaveLandingPage.save({}, $scope.landingPage, success, error);
        }

        return true;
    };

    $scope.cancel = function(){
        $location.path('/');
    };

}
LandingPageController.resolve = {

    LandingPageBy: function ($q, GetLandingPageById, $route, $timeout) {
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetLandingPageById.get({id: parseInt(id,10)}, function (data) {
                deferred.resolve(data.geo_levels);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};
