function UserRoleAssignmentController($scope, $dialog, messageService) {
    $scope.programsToDisplay = [];
    $scope.selectSuperviseProgramMessage = 'label.select.program';
    $scope.selectSupervisoryNodeMessage = 'label.select.node';
    $scope.selectWarehouseMessage = 'label.select.warehouse';

    $("#adminRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteAdminRolesModal",
                header: "create.user.deleteAdminRoleHeader",
                body: "create.user.deleteAdminRoles"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreAdminRole, $dialog);

            window.lastAdminRoleRemoved = e.removed;
        }
    });

    $("#doctorRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteDoctorRolesModal",
                header: "create.user.deleteDoctorRoleHeader",
                body: "create.user.deleteDoctorRoleHeader"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreDoctorRole, $dialog);

            window.lastDoctorRoleRemoved = e.removed;
        }
    });

    $("#nurseRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteNurseRolesModal",
                header: "create.user.deleteNurseRoleHeader",
                body: "create.user.deleteNurseRoleHeader"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreNurseRole, $dialog);

            window.lastNurseRoleRemoved = e.removed;
        }
    });

    $("#clientRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteClientRolesModal",
                header: "create.user.deleteClientRoleHeader",
                body: "create.user.deleteClientRoleHeader"
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreNurseRole, $dialog);

            window.lastNurseRoleRemoved = e.removed;
        }
    });

    $("#reportRoles").on("change", function (e) {
        if (e.removed) {
            var dialogOpts = {
                id: "deleteReportRolesModal",
                header: messageService.get("create.user.deleteReportRoleHeader"),
                body: messageService.get("create.user.deleteReportRoles")
            };
            OpenLmisDialog.newDialog(dialogOpts, $scope.restoreReportRole, $dialog, messageService);

            window.lastReportRoleRemoved = e.removed;
        }
    });

    $scope.restoreDoctorRole = function (result) {
        if (result) return;

        if (window.lastDoctorRoleRemoved) {
            $scope.user.doctorRoles.roleIds.push(window.lastDoctorRoleRemoved.id);
        }
    };

    $scope.restoreClientRole = function (result) {
        if (result) return;

        if (window.lastClientRoleRemoved) {
            $scope.user.clientRoles.roleIds.push(window.lastClientRoleRemoved.id);
        }
    };

    $scope.restoreNurseRole = function (result) {
        if (result) return;

        if (window.lastNurseRoleRemoved) {
            $scope.user.nurseRoles.roleIds.push(window.lastNurseRoleRemoved.id);
        }
    };


    $scope.restoreReportRole = function (result) {
        if (result) return;

        if (window.lastReportRoleRemoved) {
            $scope.user.reportRoles.roleIds.push(window.lastReportRoleRemoved.id);
        }
    };

    $scope.restoreAdminRole = function (result) {
        if (result) return;

        if (window.lastAdminRoleRemoved) {
            $scope.user.adminRole.roleIds.push(window.lastAdminRoleRemoved.id);
        }
    };

    $scope.hasMappingError = function (mappingErrorFlag, field) {
        return mappingErrorFlag && !isPresent(field);
    };

    var isPresent = function (obj) {
        return obj !== undefined && obj !== null && obj !== "" && !(obj instanceof Array && obj.length === 0) ;
    };
}