angular.module('landing-page', ['afyacall','ui.bootstrap.modal', 'ui.bootstrap.dialog','ngLetterAvatar','ngStomp']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/list', {controller: LandingPageListController, templateUrl: 'partials/list.html'}).
            when('/create', {controller: LandingPageController, templateUrl: 'partials/create.html', resolve:LandingPageController.resolve}).
            when('/edit/:id', {controller: LandingPageController, templateUrl: 'partials/create.html', resolve:LandingPageController.resolve}).
            otherwise({redirectTo: '/list'});
    }]).run(function ($rootScope, AuthorizationService) {
        $rootScope.landingPageSelected = "selected";
    });