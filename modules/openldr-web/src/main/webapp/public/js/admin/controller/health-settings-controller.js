function HealthSettingsController($scope,MedicalRecordTypes,SaveMedicalRecordTypes,fieldTypes,FieldGroups ) {

   $scope.fieldTypes=fieldTypes;
   console.log(fieldTypes);

    $scope.loadRecordType=function(){
        MedicalRecordTypes.get({},function(data){
           console.log(data);
           $scope.medicalRecordTypes=data.recordTypes;
        });
    }

   $scope.showOption=function(field)
   {
       $scope.field=field;
       console.log(field);
       $scope.optionModal=true;
       $scope.optionToAdd={};
   };
   $scope.cancelOptions=function(){
      $scope.optionModal=false;
      $scope.optionToAdd={};
   }
   $scope.addOptions=function(){
     $scope.field.fieldOptions.push($scope.optionToAdd);
     $scope.optionToAdd={};
   };
    $scope.loadGroups=function(){
       $scope.showSuccessfulMessage=false;
       $scope.medicalRecordType={};
       $scope.medicalRecordType.fieldGroups=[];
       $scope.selectedMedicalRecordTypeFieldGroupName=undefined;
       FieldGroups.get({medicalRecordTypeId:$scope.medicalRecordTypeId},function(data){
              $scope.medicalRecordTypeFieldGroups=data.fieldGroups;
       });

       if($scope.medicalRecordTypeId != -1 && $scope.medicalRecordTypeId !== undefined )
       {
          var type=_.findWhere($scope.medicalRecordTypes,{id:parseInt($scope.medicalRecordTypeId,10)});

          if(type !== undefined){
             $scope.medicalRecordType=type;

          }
       }

    };

    $scope.loadFields=function(){

       $scope.showSuccessfulMessage=false;
    };

    $scope.addField=function(){
       var selectedGroupExist=_.findWhere($scope.medicalRecordType.fieldGroups,{name:$scope.selectedMedicalRecordTypeFieldGroupName});

       if(selectedGroupExist != null){
          selectedGroupExist.fields.push($scope.fieldToAdd);

       }else{
          $scope.groupToAdd={};
          $scope.groupToAdd.fields=[];
          $scope.groupToAdd.name=$scope.selectedMedicalRecordTypeFieldGroupName;
          $scope.groupToAdd.displayOrder=$scope.selectedMedicalRecordTypeFieldGroupDisplayOrder;
          $scope.groupToAdd.fields.push($scope.fieldToAdd);
          $scope.medicalRecordType.fieldGroups.push($scope.groupToAdd);

       }
        $scope.fieldToAdd={};

    };

    $scope.saveRecordType=function(){
       console.log($scope.medicalRecordType);
       SaveMedicalRecordTypes.update($scope.medicalRecordType,function(data){
            $scope.showSuccessfulMessage=true;
            $scope.medicalRecordTypeId=undefined;
            $scope.medicalRecordType={};
            $scope.loadRecordType();
       });
    };
    $scope.loadRecordType();
}

HealthSettingsController.resolve = {

    fieldTypes: function ($q, FieldTypes, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            FieldTypes.get({}, function (data) {
                deferred.resolve(data.fieldTypes);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }
};
