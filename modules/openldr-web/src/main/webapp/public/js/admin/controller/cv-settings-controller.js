function CVSettingsController($scope,CVTypes,SaveCVTypes ) {

    $scope.loadCVType=function(){
        CVTypes.get({},function(data){
           $scope.cvTypes=data.cvTypes;
        });
    }

    $scope.loadFields=function(){
       $scope.showSuccessfulMessage=false;
       $scope.cvType={};
       $scope.cvType.fields=[];
       if($scope.cvTypeId != -1)
       {
          var type=_.findWhere($scope.cvTypes,{id:parseInt($scope.cvTypeId,10)});

          if(type !== undefined){
             $scope.cvType=type;

          }
       }

    };

    $scope.addField=function(){
        $scope.cvType.fields.push($scope.fieldToAdd);
        $scope.fieldToAdd={};
    };

    $scope.saveCVType=function(){
       SaveCVTypes.update($scope.cvType,function(data){
            $scope.showSuccessfulMessage=true;
            $scope.cvTypeId=undefined;
            $scope.cvType={};
            $scope.loadCVType();
       });
    };
    $scope.loadCVType();
}

CVSettingsController.resolve = {

//    medicalRecordTypes: function ($q, Users, $route, $timeout) {
//        var userId = $route.current.params.userId;
//        if (!userId) return undefined;
//        var deferred = $q.defer();
//        $timeout(function () {
//            Users.get({id: userId}, function (data) {
//                deferred.resolve(data.user);
//            }, function () {
//            });
//        }, 100);
//        return deferred.promise;
//    }
};
