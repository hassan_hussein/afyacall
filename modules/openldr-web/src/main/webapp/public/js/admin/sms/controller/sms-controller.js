function SmsController($scope,Users,$routeParams,GetSMSInfo, GetMessagesForMobile,GetReplyMessages) {

        $scope.sms = {};

        $scope.list = [];
         $scope.Msg= [];

        GetReplyMessages.get($scope.sms);

        Users.get({id: $routeParams.userId},function(data){

            $scope.sms.fullname= data.user.firstName + ' ' + data.user.lastName;

            console.log(data.user);
            $scope.username = data.user.username;
            $scope.sms.mobile = data.user.cellPhone;

            // get the user's mobile interactions
            GetMessagesForMobile.get({mobile: data.user.cellPhone},function(data){
                $scope.list = data.sms;
            });
        });

        $scope.sendSMS = function(){
            GetSMSInfo.get($scope.sms);
            $scope.message = 'New SMS sent successfully';
            $scope.sms.content = '';
            return true;
        } ;

}

