function ExtensionNumberController($scope,ExtensionNumberBy,$location,ExtensionNumbersInfo,SaveExtensionNumbersInfo){
   $scope.extension = {};

    if(ExtensionNumberBy !== null || ExtensionNumberBy !== undefined)
    $scope.extension = ExtensionNumberBy;

    var success = function (data) {
        console.log(data.extension_numbers);
        $scope.error = "";
        $scope.$parent.message = data.success;
        $scope.$parent.extension = data.extension_numbers;
        $scope.showError = false;
        $location.path('');
    };

    var error = function (data) {
        $scope.$parent.message = "";
        $scope.error = data.data.error;
        $scope.showError = true;
    };


    $scope.saveExtension = function () {


        if ($scope.extensionNumberForm.$error.extensionNumber || $scope.extensionNumberForm.$error.required) {
            $scope.showError = true;
            $scope.error = 'form.error';
            $scope.message = "";
            return;
        }


        if ($scope.extension.id) {
            ExtensionNumbersInfo.update({id: $scope.extension.id}, $scope.extension, success, error);
        }
        else {
            SaveExtensionNumbersInfo.save({}, $scope.extension, success, error);
        }

        return true;
    };

    $scope.cancel = function(){
    $location.path('/');
    };

}


ExtensionNumberController.resolve = {

    ExtensionNumberBy: function ($q, GetExtensionNumbersById, $route, $timeout) {
        var id = $route.current.params.id;
        if (!id) return undefined;
        var deferred = $q.defer();
        $timeout(function () {
            GetExtensionNumbersById.get({id: parseInt(id,10)}, function (data) {
                deferred.resolve(data.allExtensionNumbers);
            }, function () {
            });
        }, 100);
        return deferred.promise;
    }

};