package com.openldr.account.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMember extends BaseModel {

    private String memberPinCode;

    private Long accountId;

    private Account account;

    private Long userId;

    private User user;

    private Date expirationDate;

    private Date activationDate;

    private boolean active;

    private boolean isDefault;

}
