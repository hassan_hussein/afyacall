package com.openldr.account.service;

import com.openldr.account.domain.*;
import com.openldr.account.repository.AccountRepository;
import com.openldr.core.domain.AccountPackage;
import com.openldr.core.repository.AccountPackageRepository;
import com.openldr.core.repository.SMSRepository;
import com.openldr.core.repository.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@NoArgsConstructor
public class AccountService {

    @Autowired
    AccountPackageRepository accountPackageRepository;
    @Autowired
    private AccountRepository repository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SMSRepository smsRepository;

    @Transactional
    public void createAccount(Account account, Long userId) {

        if (account.getId() == null) {
            account.setReferenceNumber(getReferenceNumber());
            account.setAccountTypeId(account.getAccountType().getId());
            account.setCreatedBy(userId);
            account.setModifiedBy(userId);
            repository.insertAccount(account);
            saveMembers(account, account.getMembers(), userId);

        } else {
            account.setModifiedBy(userId);
            repository.updateAccount(account);
            saveMembers(account, account.getMembers(), userId);
        }

    }

    public void sendInvoiceSMS(String referenceNumber) {
        StringBuilder smsMessage = new StringBuilder();
        Account account = getByReferenceNumber(referenceNumber);
        AccountMember defaultMember = getDefaultMember(account.getMembers());
        smsMessage.append("Activation Code:" + account.getReferenceNumber() + ";");
        int total = 0;
        for (AccountMember member : account.getMembers()) {
            //If account is expired already
            if (!member.isActive()) {
                total++;
            }
            //TODO if account is about to expire
        }
        Double packageAmount = (account.getAccountPackage() != null && account.getAccountPackage().getPrice() != null) ? account.getAccountPackage().getPrice() : 0;
        //TODO package currency
        String packageCurrency = "TZS";
        Double invoiceAmount = total * packageAmount - account.getWallet().getBalance();
        if (invoiceAmount > 0) {
            smsMessage.append(total + " Member(s) Inactive; Invoice Amount is:" + invoiceAmount.toString() + " " + packageCurrency);
            smsRepository.SaveSMSMessage("1", smsMessage.toString(), defaultMember.getUser().getCellPhone(),
                    new Date(), false);
        }



    }

    public void sendAllMembersPin(String referenceNumber) {
        Account account = getByReferenceNumber(referenceNumber);
        sendMembersPinSMS(account.getMembers(), getDefaultMember(account.getMembers()));
    }

    private void sendMembersPinSMS(List<AccountMember> members, AccountMember defaultMember) {
        StringBuilder smsMessage = new StringBuilder();
        smsMessage.append("New Member(s) pin:");
        // AccountMember defaultMember = getDefaultMember(members);
        for (AccountMember member : members) {
            smsMessage.append(member.getUser().getFirstName() + " "
                    + member.getUser().getLastName() + "(" +
                    member.getMemberPinCode() + ");");
        }
        //TODO check message length;
        smsRepository.SaveSMSMessage("1", smsMessage.toString(), defaultMember.getUser().getCellPhone(),
                new Date(), false);

    }


    private void saveWallet(Account account) {
        AccountWallet existing = getWalletByAccount(account.getId());
        if ((account.getWallet() != null && existing != null) && (existing.getBalance() != account.getWallet().getBalance())) {
            updateWallet(account.getWallet());
            existing = account.getWallet();
        }
        if (existing == null && account.getWallet() == null) {
            AccountWallet wallet = new AccountWallet();
            wallet.setAccountId(account.getId());
            wallet.setBalance(0.00);
            insertWallet(wallet);
        } else if (existing == null && account.getWallet() != null) {
            AccountWallet wallet = account.getWallet();
            wallet.setAccountId(account.getId());
            insertWallet(wallet);
            activateAccountMembers(account);
        } else if (existing != null && account.getWallet() != null && account.getWallet().getBalance() != null) {
            AccountWallet wallet = account.getWallet();
            wallet.setAccountId(account.getId());
            updateWallet(wallet);
            activateAccountMembers(account);
        }
    }

    private void activateAccountMembers(Account account) {
        AccountWallet existingAccount = getWalletByAccount(account.getId());
        com.openldr.core.domain.AccountPackage accountPackage = accountPackageRepository.getById(account.getAccountPackageId());
        List<AccountMember> inActiveMembers = repository.getInactiveMembers(account.getId());
        for (AccountMember member : inActiveMembers) {
            if (existingAccount.getBalance() >= accountPackage.getPrice()) {
                Date activationDate = new Date();
                Calendar expirationDate = Calendar.getInstance();
                int daysToAdd = (accountPackage.getPeriod() != null) ? accountPackage.getPeriod() : 365;
                expirationDate.add(Calendar.DATE, daysToAdd);
                member.setActivationDate(activationDate);
                member.setExpirationDate(expirationDate.getTime());
                repository.activateMember(member);
                existingAccount.setBalance(existingAccount.getBalance() - accountPackage.getPrice());
                updateWallet(existingAccount);
            }
        }
    }

    private void saveMembers(Account account, List<AccountMember> members, Long userId) {
        List<AccountMember> memberToInclude = new ArrayList<>();
        boolean hasNewMember = false;
        for (AccountMember member : members) {
            if (member.getId() == null) {
                if (member.getUser().getUserName() == null)
                    member.getUser().setUserName(member.getUser().getCellPhone());
                userRepository.create(member.getUser());
                member.setMemberPinCode(getMemberId());
                member.setUserId(member.getUser().getId());
                member.setAccountId(account.getId());
                member.setCreatedBy(userId);
                repository.insertMember(member);
                memberToInclude.add(member);
                hasNewMember = true;
            } else {
                repository.updateMember(member);
            }
        }
        //Save Wallet
        saveWallet(account);

        //Send sms
        if (memberToInclude.size() > 0) {
            sendMembersPinSMS(memberToInclude, getDefaultMember(account.getMembers()));
        }
        if (hasNewMember) {
            sendInvoiceSMS(account.getReferenceNumber());
        }
    }

    private String getMemberId() {
        Date date = new Date();
        Long time = date.getTime();
        String timeString = time.toString();
        String pin = timeString.substring(timeString.length() - 6);
        return pin;
    }

    private AccountMember getDefaultMember(List<AccountMember> members) {
        AccountMember defaultMember = null;
        for (AccountMember member : members) {
            if (member.isDefault()) {
                defaultMember = member;
            }
        }
        if (defaultMember != null) {
            return defaultMember;
        } else {
            return members.get(0);
        }
    }

    private Date getRenewDate(AccountPackage accountPackage) {
        return new Date();
    }

    private String getReferenceNumber() {

        Date date = new Date();

        Long time = date.getTime();
        String timeString = time.toString();
        String reference = timeString.substring(timeString.length() - 6);

        return reference;
    }

    private String getSum(String[] numbers) {
        int x = 0;
        for (String number : numbers) {
            x = x + Integer.parseInt(number);
        }
        if (x <= 9) {
            return "0".concat(new Integer(x).toString());
        } else {
            return new Integer(x).toString();
        }
    }

    public Account getByReferenceNumber(String referenceNumber) {
        return repository.getByReferenceNumber(referenceNumber);
    }

    public List<AccountType> getAccountType() {
        return repository.getAccountType();
    }

    public Integer insertWallet(AccountWallet wallet) {
        return repository.insertWallet(wallet);
    }

    public Integer updateWallet(AccountWallet wallet) {
        return repository.updateWallet(wallet);
    }

    public AccountWallet getWalletByAccount(Long accountId) {
        return repository.getWalletByAccount(accountId);
    }

    public List<AccountMember> searchMembers(String query) {
        return repository.searchMembers(query);
    }

    public void createTransaction(AccountWalletTransaction transaction) {
        repository.createTransaction(transaction);
    }

    public Map<String, String> getTopTiles() {
        return repository.getTopTiles();
    }
}
