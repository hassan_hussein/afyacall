package com.openldr.account.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.openldr.core.domain.AccountPackage;
import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.Organization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account extends BaseModel {

    List<AccountMember> members;
    private Long accountPackageId;
    private Long accountTypeId;
    private AccountType accountType;
    private AccountPackage accountPackage;
    private Long organizationId;
    private Organization organization;
    private String referenceNumber;
    private Date registrationDate;
    private Date renewDate;
    private Boolean active;
    private AccountWallet wallet;
}
