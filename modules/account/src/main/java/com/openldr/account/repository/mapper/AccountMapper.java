package com.openldr.account.repository.mapper;

import com.openldr.account.domain.*;
import com.openldr.core.domain.AccountPackage;
import com.openldr.core.domain.Organization;
import com.openldr.core.domain.User;
import com.openldr.core.domain.UserType;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AccountMapper {


    @Insert({"INSERT INTO accounts(accountpackageid, accounttypeid, organizationid,referencenumber," +
            "registrationdate,active,createdby,createddate,modifiedby, modifieddate) " +
            "VALUES (#{accountPackageId}, #{accountTypeId},#{organizationId},#{referenceNumber}," +
            "NOW(),FALSE,#{createdBy},NOW(),#{modifiedBy},NOW())"})
    @Options(useGeneratedKeys = true)
    Integer insertAccount(Account account);

    @Insert({"UPDATE accounts SET accountpackageid=#{accountPackageId},#{active}," +
            "modifiedby=#{modifiedDate},modifieddate=#{modifiedDate} " +
            "WHERE referencenumber=#{referenceNumber}"})
    void activateAccount(Account account);

    @Insert({"INSERT INTO account_members(memberpincode, userid,isdefault, accountid,active,expirationdate,createdby,createddate,modifiedby,modifieddate) " +
            "VALUES (#{memberPinCode},#{userId},#{isDefault},#{accountId},FALSE,NOW(),#{createdBy},NOW(),#{modifiedBy},NOW())"})
    @Options(useGeneratedKeys = true)
    Integer insertAccountMember(AccountMember accountMember);

    @Select({"SELECT * FROM accounts WHERE referencenumber=#{referenceNumber} LIMIT 1"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "accountPackageId", column = "accountpackageid"),
            @Result(property = "accountTypeId", column = "accounttypeid"),
            @Result(property = "organizationId", column = "organizationid"),
            @Result(property = "organization", column = "organizationid", javaType = Organization.class,
                    one = @One(select = "com.openldr.core.repository.mapper.OrganizationMapper.getById")),
            @Result(property = "members", javaType = List.class, column = "id",
                    many = @Many(select = "getAccountMembers")),
            @Result(property = "accountType", column = "accountTypeId", javaType = UserType.class,
                    one = @One(select = "getTypeById")),

            @Result(property = "wallet", column = "id", javaType = AccountWallet.class,
                    one = @One(select = "getByAccount")),
            @Result(property = "accountPackage", column = "accountPackageId", javaType = AccountPackage.class,
                    one = @One(select = "com.openldr.core.repository.mapper.AccountPackageMapper.getAllById"))
    })
    Account getByReferenceNumber(@Param("referenceNumber") String referenceNumber);

    @Select({"SELECT * FROM accounts WHERE id=#{accountId} LIMIT 1"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "accountPackageId", column = "accountpackageid"),
            @Result(property = "accountTypeId", column = "accounttypeid"),
            @Result(property = "organizationId", column = "organizationid"),
            @Result(property = "organization", column = "organizationid", javaType = Organization.class,
                    one = @One(select = "com.openldr.core.repository.mapper.OrganizationMapper.getById")),
            @Result(property = "accountType", column = "accountTypeId", javaType = UserType.class,
                    one = @One(select = "getTypeById")),
            @Result(property = "wallet", column = "id", javaType = AccountWallet.class,
                    one = @One(select = "getByAccount")),
            @Result(property = "accountPackage", column = "accountPackageId", javaType = AccountPackage.class,
                    one = @One(select = "com.openldr.core.repository.mapper.AccountPackageMapper.getAllById"))
    })
    Account getAccountForMember(@Param("accountId") Long accountId);

    @Select({"SELECT * FROM account_members WHERE accountid=#{accountId}"})
    @Results({@Result(property = "user", column = "userId", javaType = User.class,
            one = @One(select = "com.openldr.core.repository.mapper.UserMapper.getById"))})
    List<AccountMember> getAccountMembers(@Param("accountid") Long accountId);

    @Update({"UPDATE accounts set modifiedby=#{modifiedBy}, modifieddate=NOW() WHERE id=#{id}"})
    void updateAccount(Account account);

    @Select("Select * from account_types")
    List<AccountType> getAccountType();

    @Insert({"INSERT INTO account_wallet(accountid,balance) VALUES(#{accountId},#{balance})"})
    Integer insertWallet(AccountWallet wallet);

    @Update({"UPDATE account_wallet set balance=#{balance} WHERE accountid=#{accountId}"})
    Integer updateWallet(AccountWallet wallet);

    @Select("SELECT * FROM account_wallet where accountid=#{accountId} limit 1")
    AccountWallet getByAccount(@Param("accountId") Long accountId);

    @Select("SELECT * FROM account_members where accountid=#{accountId} and active=FALSE order by createddate")
    List<AccountMember> getInactiveMembers(Long accountId);

    @Update("UPDATE account_members set active=TRUE, expirationDate=#{expirationDate},activationDate=#{activationDate} where id=#{id} ")
    void activateMember(AccountMember member);

    @Select("Select m.* from account_members m join users u on u.id=m.userid " +
            " where lower(u.firstname) LIKE '%'||lower(#{query})||'%' or lower(u.lastname) LIKE '%'||lower(#{query})||'%' or m.memberpincode LIKE '%'||#{query}||'%' ")
    @Results({@Result(property = "user", column = "userId", javaType = User.class,
            one = @One(select = "com.openldr.core.repository.mapper.UserMapper.getById")),
            @Result(property = "account", column = "accountId", javaType = Account.class,
                    one = @One(select = "getAccountForMember"))
    })
    List<AccountMember> searchMembers(String query);

    @Insert("Insert into account_wallet_transactions(accountwalletid,transactiontype,transactionamout,createddate) " +
            "values(#{accountWalletId},#{transactionType},#{transactionAmount},NOW())")
    @Options(useGeneratedKeys = true)
    Integer createTransaction(AccountWalletTransaction transaction);

    @Select(" SELECT *  " +
            "  FROM account_types WHERE id = #{Id}")
    AccountType getTypeById(Long Id);

    @Insert({"UPDATE account_members SET isdefault=#{isDefault}" +
            " where id=#{id}"})
    Integer updateMember(AccountMember member);

    @Select("Select * from top_tiles_view limit 1")
    Map<String, String> getTopTiles();
}
