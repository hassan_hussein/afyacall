package com.openldr.account.domain;

import com.openldr.core.domain.AccountPackage;
import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
//@JsonSerialize(include = NON_EMPTY)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountPayment extends BaseModel {

    private Long accountId;

    private Account account;

    private Double amount;

    private String currency;

    private Date paymentDate;

    private Long accountPackageId;

    private AccountPackage accountPackage;

    private Date expirationDate;

}
