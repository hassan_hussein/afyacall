

package com.openldr.upload;

import com.openldr.upload.model.AuditFields;

/**
 * This interface is implemented by all record handlers.
 */

public interface RecordHandler<I extends Importable> {

  public void execute(I importable, int rowNumber, AuditFields auditFields);

  public void postProcess(AuditFields auditFields);
}
