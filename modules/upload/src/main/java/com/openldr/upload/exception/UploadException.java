package com.openldr.upload.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class represents exception occurred while processing upload.
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class UploadException extends RuntimeException {

  private String code;
  private String[] params = new String[0];

  public UploadException(String code) {
    super(code);
    this.code = code;
  }

  public UploadException(String code,String... params){
    this.code = code;
    this.params = params;
  }

  @Override
  public String toString(){
    if(params.length == 0) return code;

    StringBuilder messageBuilder = new StringBuilder("code: "+code+ ", params: { ");
    for(String param : params){
      messageBuilder.append("; ").append(param);
    }
    messageBuilder.append(" }");
    return messageBuilder.toString().replaceFirst("; ","");
  }
}
