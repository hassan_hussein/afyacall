package com.openldr.sms.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by hassan on 7/25/16.
 */


@Getter
@Setter
@NoArgsConstructor
public class SMS {

    private int id;
    private String message;
    private String phoneNumber;
    private String direction;
    private Date dateSaved;
    private Boolean sent;

    private String from;
    private String to;
    private String text;
}
