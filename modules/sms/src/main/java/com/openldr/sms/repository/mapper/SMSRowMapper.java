package com.openldr.sms.repository.mapper;


import com.openldr.sms.domain.SMS;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SMSRowMapper implements RowMapper<SMS> {

  @Override
  public SMS mapRow(ResultSet rs, int rowNum) throws SQLException {
      SMS sms = new SMS();
      sms.setId(rs.getInt("id"));
      sms.setMessage(rs.getString("message"));
      sms.setPhoneNumber(rs.getString("phonenumber"));
      sms.setDirection(rs.getString("direction"));
      sms.setSent(rs.getBoolean("sent"));
      return sms;
  }
}
