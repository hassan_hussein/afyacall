package com.openldr.sms.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Role represents Role entity which is a set of rights. Also provides methods to validate if a role contains related rights.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Sms {
    private int id;
    private String from;
    private String to;
    private String text;

}
