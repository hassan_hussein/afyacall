
package com.openldr.authentication.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This class represents a user token consisting username, id and authenticated flag.
 */
@Data
@AllArgsConstructor
public class UserToken {

    private final String userName;
    private final Long userId;
    private final boolean authenticated;
}
