

package com.openldr.authentication.service;

import lombok.NoArgsConstructor;
import  com.openldr.authentication.domain.UserToken;
import  com.openldr.core.domain.User;
import  com.openldr.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service is responsible for authenticating the given username, credential combination
 */

@Service
@NoArgsConstructor
public class UserAuthenticationService {

  private static final boolean AUTHENTICATION_SUCCESSFUL = true;
  private static final boolean AUTHENTICATION_FAILED = false;

  private UserService userService;

  @Autowired
  public UserAuthenticationService(UserService userService) {
    this.userService = userService;
  }

  public UserToken authenticateUser(User user) {
    User fetchedUser = userService.selectUserByUserNameAndPassword(user.getUserName(),
      user.getPassword());
    if (fetchedUser == null || fetchedUser.getRestrictLogin())
      return new UserToken(user.getUserName(), null, AUTHENTICATION_FAILED);

    return new UserToken(fetchedUser.getUserName(), fetchedUser.getId(), AUTHENTICATION_SUCCESSFUL);
  }
}
