
package com.openldr.authentication.web;

import com.openldr.authentication.domain.UserToken;
import com.openldr.authentication.service.UserAuthenticationService;
import com.openldr.core.domain.User;
import com.openldr.authentication.web.UserAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.HashMap;
import java.util.Map;

/**
 * This class acts as an authentication provider. It authenticates user credentials.
 */

public class UserAuthenticationProvider implements AuthenticationProvider {

  private UserAuthenticationService userAuthenticationService;

  @Autowired
  public UserAuthenticationProvider(UserAuthenticationService userAuthenticationService) {
    this.userAuthenticationService = userAuthenticationService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    String userName = (String) authentication.getPrincipal();
    String password = (String) authentication.getCredentials();
   User user = new  User();
    user.setUserName(userName);
    user.setPassword(password);
    UserToken userToken = userAuthenticationService.authenticateUser(user);

    if (userToken.isAuthenticated()) {
      UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userToken.getUserId(), null, null);
      Map<String, String> userDetails = new HashMap<>();
      userDetails.put(UserAuthenticationSuccessHandler.USER, userToken.getUserName());
      usernamePasswordAuthenticationToken.setDetails(userDetails);
      return usernamePasswordAuthenticationToken;
    } else {
      return null;
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return true;
  }
}
