package com.openldr.core.repository.mapper;

import com.openldr.core.domain.ProductCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by chrispinus on 3/14/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-applicationContext-core.xml")
@Transactional
public class ProductCategoryMapperIT {

    @Autowired
    ProductCategoryMapper mapper;

    @Test
    public void shouldInsert() {
        ProductCategory productCategory = new ProductCategory("code", "name", 1);
        Integer result = mapper.insert(productCategory);
        assertEquals(result.intValue(), 1);
    }

    @Test
    public void shouldGetById() {
        ProductCategory productCategory = new ProductCategory("code", "name", 1);
        mapper.insert(productCategory);
        ProductCategory existing = mapper.getById(productCategory.getId());
        assertNotNull(existing);
    }

    @Test
    public void shouldUpdate() {
        ProductCategory productCategory = new ProductCategory("code", "name", 1);
        mapper.insert(productCategory);
        productCategory.setDisplayOrder(2);
        Integer affected = mapper.update(productCategory);
        ProductCategory updated = mapper.getById(productCategory.getId());

        assertEquals(affected.intValue(), 1);
        assertEquals(updated.getDisplayOrder().intValue(), 2);
    }

    @Test
    public void shouldGetAll() {
        ProductCategory productCategory1 = new ProductCategory("code1", "name1", 1);
        ProductCategory productCategory2 = new ProductCategory("code2", "name2", 2);
        mapper.insert(productCategory1);
        mapper.insert(productCategory2);
        List<ProductCategory> all = mapper.getAll();
        assertEquals(all.size(), 2);
    }

}

