package com.openldr.core.repository;

import com.openldr.core.domain.ProductCategory;
import com.openldr.core.repository.mapper.ProductCategoryMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by chrispinus on 3/15/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductCategoryRepositoryTest {

    @Mock
    private ProductCategoryMapper mapper;

    private ProductCategoryRepository repository;

    @Before
    public void setUp() throws Exception {
        repository = new ProductCategoryRepository(mapper);
    }

    @Test
    public void shouldInsert() {
        ProductCategory productCategory = new ProductCategory();
        when(mapper.insert(productCategory)).thenReturn(1);
        repository.insert(productCategory);
        verify(mapper).insert(productCategory);
    }

}
