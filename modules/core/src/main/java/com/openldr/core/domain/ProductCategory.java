package com.openldr.core.domain;

import lombok.*;

/**
 * Created by chrispinus on 3/14/16.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ProductCategory extends BaseModel {

    private String code;

    private String name;

    private Integer displayOrder;
}
