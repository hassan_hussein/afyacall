
package com.openldr.core.repository;

import com.openldr.core.exception.DataException;
import lombok.NoArgsConstructor;
import  com.openldr.core.domain.Right;
import  com.openldr.core.repository.mapper.RightMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RightsRepository is Repository class for Rights related database operations.
 */

@Repository
@NoArgsConstructor
public class RightRepository {

  @Autowired
  private RightMapper rightsMapper;

  public void insertRight(Right right) {
    rightsMapper.insertRight(right);
  }

  public Boolean hasReportingRight(Long userId) {
    return rightsMapper.totalReportingRightsFor(userId) > 0;
  }

  public List<Right> getAll() {
    return rightsMapper.getAll();
  }

  public void updateRight(Right right) {
    rightsMapper.update(right);
  }

  public Right getByName(String name) {
    return rightsMapper.getByName(name);
  }

  public void save(Right right) {

    try {
      String name = changeRightNameToUpperCase(right.getName());
      right.setName(name);
      Right right1 = rightsMapper.getByName(right.getName());

      if (right1.getName() == null) {
        rightsMapper.insertRight(right);
        return;
      } else {
        rightsMapper.update(right);
      }
    } catch (DuplicateKeyException e) {
      throw new DataException("error.duplicate.right.name");
    } catch (DataIntegrityViolationException e) {
      throw new DataException("error.incorrect.length");
    }

  }

  private String changeRightNameToUpperCase(String name) {

    return name.replaceAll(" ","_").toUpperCase();

  }

  public Right delete(String name) {
    return rightsMapper.delete(name);
  }
}
