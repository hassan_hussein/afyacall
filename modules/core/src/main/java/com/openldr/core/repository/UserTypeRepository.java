package com.openldr.core.repository;

import com.openldr.core.domain.UserType;
import com.openldr.core.repository.mapper.UserTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */
@Repository
public class UserTypeRepository {

    @Autowired
    public UserTypeMapper mapper;

    public Integer Insert(UserType userType) {
        return mapper.Insert(userType);
    }

    public UserType getAllById(Long Id) {
        return mapper.getAllById(Id);
    }

    public void update(UserType userType) {
        mapper.update(userType);
    }

    public List<UserType> getAll() {
        return mapper.getAll();
    }


}
