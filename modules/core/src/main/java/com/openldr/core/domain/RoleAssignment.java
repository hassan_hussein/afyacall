
package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;

/**
 * RoleAssignment represents a Role assigned to the user which has associated rights with it.
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = NON_NULL)
@EqualsAndHashCode(callSuper = false)

public class RoleAssignment extends BaseModel {
  private Long userId;
  private List<Long> roleIds = new ArrayList<>();

  private Long programId;
  private SupervisoryNode supervisoryNode;

  public RoleAssignment(Long userId, Long roleId, Long programId, SupervisoryNode supervisoryNode) {
    this.userId = userId;
    this.roleIds.add(roleId);
    this.programId = programId;
    this.supervisoryNode = supervisoryNode;
  }

  public RoleAssignment(Long userId, List<Long> roleIds, Long programId, SupervisoryNode supervisoryNode) {
    this.userId = userId;
    this.roleIds = roleIds;
    this.programId = programId;
    this.supervisoryNode = supervisoryNode;
  }

  @SuppressWarnings("unused (used for mybatis mapping)")
  public void setRoleId(Long roleId) {
    this.roleIds.add(roleId);
  }

  @SuppressWarnings("unused (used for mybatis mapping)")
  public void setRoleIdsAsString(String roleIds) {
    parseRoleIdsIntoList(roleIds);
  }

  private void parseRoleIdsIntoList(String roleIds) {
    roleIds = roleIds.replace("{", "").replace("}", "");
    String[] roleIdsArray = roleIds.split(",");
    for (String roleId : roleIdsArray) {
      Long id = Long.parseLong(roleId);
      if (!this.roleIds.contains(id))
        this.roleIds.add(id);
    }
  }

}
