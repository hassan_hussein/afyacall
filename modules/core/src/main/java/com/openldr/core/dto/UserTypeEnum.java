package com.openldr.core.dto;

/**
 * Created by hassan on 7/9/16.
 */
public enum UserTypeEnum {
    CLIENT,
    DOCTOR,
    AGENT,
    NURSE,
    ADMIN,
    NORMAL_USER
}
