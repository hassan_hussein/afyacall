package com.openldr.core.repository;

import com.openldr.core.domain.UserLandingPage;
import com.openldr.core.repository.mapper.UserLandingPageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 8/15/16.
 */


@Repository
public class UserLandingPageRepository {

    @Autowired
    private UserLandingPageMapper mapper;

    public void insert(UserLandingPage landingPage) {
        mapper.insert(landingPage);
    }

    public void update(UserLandingPage landingPage) {
        mapper.update(landingPage);
    }

    public UserLandingPage getLandingPageById(Long id) {
        return mapper.getLandingPage(id);
    }

    public UserLandingPage getByUserId(Long userId) {
        return mapper.getByUserId(userId);
    }

    public List<UserLandingPage> getAll() {
        return mapper.getAll();
    }

    public Long delete(Long id) {
        return mapper.delete(id);
    }
}
