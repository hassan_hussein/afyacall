/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.core.repository;

import com.openldr.core.domain.GeographicLevel;
import com.openldr.core.exception.DataException;
import com.openldr.core.repository.mapper.GeographicLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

@Repository
public class GeographicLevelRepository {

    @Autowired
    private GeographicLevelMapper mapper;

    public GeographicLevel getGeographicLevel(Long geographicLevelID) {
        return mapper.getGeographicLevelById(geographicLevelID);
    }

    public void insert(GeographicLevel geographicLevel) {
        mapper.insert(geographicLevel);
    }

    public void update(GeographicLevel geographicLevel) {
        mapper.update(geographicLevel);
    }

    public void save(GeographicLevel geographicLevel) {
        try {
            if (geographicLevel.getId() == null) {
                mapper.insert(geographicLevel);
                return;
            } else {
                mapper.update(geographicLevel);
            }
        } catch (DuplicateKeyException e) {
            throw new DataException("error.duplicate.geographic.level.code");
        } catch (DataIntegrityViolationException e) {
            throw new DataException("error.incorrect.length");
        }

    }

    public Long delete(Long id) {
        return mapper.delete(id);
    }

}
