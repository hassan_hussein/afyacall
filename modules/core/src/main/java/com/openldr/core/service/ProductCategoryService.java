package com.openldr.core.service;

import com.openldr.core.domain.ProductCategory;
import com.openldr.core.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chrispinus on 3/15/16.
 */
@Service
public class ProductCategoryService {

    @Autowired
    private ProductCategoryRepository repository;

    public Integer insert(ProductCategory productCategory) {
        return repository.insert(productCategory);
    }

    public Integer update(ProductCategory productCategory) {
        return repository.update(productCategory);
    }

    public ProductCategory getById(Long id) {
        return repository.getById(id);
    }

    public List<ProductCategory> getAll() {
        return repository.getAll();
    }
}
