package com.openldr.core.repository.mapper;

import com.openldr.core.domain.UserType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */

@Repository
public interface UserTypeMapper {

    @Insert({"INSERT INTO user_types(userType,description,displayorder)  " +
            "    VALUES (#{userType}, #{description}, #{displayOrder}) "})
    @Options(useGeneratedKeys = true)
    Integer Insert(UserType userType);

    @Select(" SELECT *  " +
            "  FROM user_types WHERE id = #{Id}")
    UserType getAllById(Long Id);

    @Update("  UPDATE user_types  " +
            "   SET id=#{id}, usertype=#{type}  " +
            " WHERE id = #{id}, userType = #{type}, description = #{description}, displayOrder = #{displayOrder}  ")
    void update(UserType userType);

    @Select(" select * from user_types ")
    List<UserType> getAll();
}
