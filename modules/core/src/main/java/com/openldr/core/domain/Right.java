

package com.openldr.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

/**
 * Right represents the rights available in the system along with their type.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Right {

  private String name;
  private RightType type;
  private String description;
  private String displayNameKey;
  private Integer displayOrder;


  public Right(String name, RightType type) {
    this.name = name;
    this.type = type;
  }
}
