package com.openldr.core.repository.mapper;

import com.openldr.core.domain.AccountPackage;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountPackageMapper {

    @Insert({"INSERT INTO account_packages (  " +
            "            name, description, createddate, displayorder,terminationDate,status, period,price)  " +
            "    VALUES (#{name}, #{description}, NOW(), #{displayOrder}, #{terminationDate}, #{status},#{period},#{price}) "})
    @Options(useGeneratedKeys = true)
    Integer Insert(AccountPackage accountPackage);

    @Select("SELECT id, name, description, createddate, displayorder,terminationDate,status,period,price\n" +
            "  FROM account_packages WHERE id = #{Id}")
    AccountPackage getAllById(Long Id);

    @Update(" UPDATE account_packages  " +
            "   SET  name=#{name}, description=#{description}, displayorder=#{displayOrder}, " +
            " terminationDate = #{terminationDate} , status = #{status}, period =#{period}, price = #{price}  " +
            " WHERE id = #{id} ")
    Integer update(AccountPackage accountPackage);

    @Select(" select * from account_packages ")
    List<AccountPackage> getAll();

    @Select(" DELETE FROM account_packages WHERE id = #{id} ")
    Long delete(Long id);

    @Select(" select * from account_packages where id=#{id}")
    AccountPackage getById(@Param("id") Long id);
}
