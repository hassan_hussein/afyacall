package com.openldr.core.repository.mapper;

import com.openldr.core.domain.UserLandingPage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLandingPageMapper {

    @Insert({" INSERT INTO user_landing_pages( ",
            "             name, url) ",
            "    VALUES ( #{name}, #{url})"})
    @Options(useGeneratedKeys = true)
    void insert(UserLandingPage landingPage);

    @Update(" UPDATE user_landing_pages\n" +
            "   SET   name=#{name}, url=#{url}\n" +
            " WHERE ID = #{id}    ")
    void update(UserLandingPage landingPage);

    @Select(" select * from user_landing_pages where id = #{id}")
    UserLandingPage getLandingPage(Long id);

    @Select(" SELECT * FROM user_landing_pages where userId = #{userId}")
    UserLandingPage getByUserId(Long userId);

    @Select(" SELECT * FROM user_landing_pages  ")
    List<UserLandingPage> getAll();

    @Select(" SELECT from user_landing_pages where id = #{id} ")
    Long delete(Long id);

}
