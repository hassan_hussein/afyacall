/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package com.openldr.core.exception;

import com.openldr.core.message.OpenLDRMessage;
import lombok.Getter;

/**
 * This is the base exception class for all application level exception. It provides ways to create custom exception
 * with externalised messages and parameters.
 */
public class DataException extends RuntimeException {

  @Getter
  private OpenLDRMessage openLDRMessage;

  public DataException(String code) {
    openLDRMessage = new OpenLDRMessage(code);
  }

  public DataException(String code, Object... params) {
    StringBuilder stringParams = new StringBuilder();
    for (Object param : params) {
      stringParams.append(param.toString()).append("#");
    }
    openLDRMessage = new OpenLDRMessage(code, stringParams.toString().split("#"));
  }

  public DataException(OpenLDRMessage openLmisMessage) {
    this.openLDRMessage = openLmisMessage;
  }

  @Override
  public String toString() {
    return openLDRMessage.toString();
  }

  @Deprecated
  @Override
  public String getMessage() {
    return openLDRMessage.toString();
  }
}
