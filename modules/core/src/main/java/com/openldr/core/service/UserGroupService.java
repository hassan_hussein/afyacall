package com.openldr.core.service;

import com.openldr.core.domain.UserGroup;
import com.openldr.core.repository.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 7/30/16.
 */
@Service
public class UserGroupService {
    @Autowired
    private UserGroupRepository repository;

    public List<UserGroup> getAll() {
        return repository.getAll();
    }
}
