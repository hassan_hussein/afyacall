package com.openldr.core.upload;


import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.GeographicZone;
import com.openldr.core.service.GeographicZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * GeographicZonePersistenceHandler is used for uploads of GeographicZone. It uploads each GeographicZone
 * record by record.
 */
@Component
public class GeographicZonePersistenceHandler extends AbstractModelPersistenceHandler {
  GeographicZoneService geographicZoneService;

  @Autowired
  public GeographicZonePersistenceHandler(GeographicZoneService geographicZoneService) {
    this.geographicZoneService = geographicZoneService;
  }


  @Override
  protected BaseModel getExisting(BaseModel record) {
    return geographicZoneService.getByCode((GeographicZone) record);
  }

  @Override
  protected void save(BaseModel record) {
    geographicZoneService.save((GeographicZone) record);
  }

  @Override
  public String getMessageKey() {
    return "error.duplicate.geographic.zone.code";
  }


}