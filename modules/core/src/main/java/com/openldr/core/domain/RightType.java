
package com.openldr.core.domain;

/**
 * Enum for right types. All rights in the system are divided into four basic types: Admin, Allocation, Requisition and
 * Fulfillment.
 */
public enum RightType {

  ADMIN,
  USER,
  ALLOCATION,
  REQUISITION,
  FULFILLMENT,
  REPORTING,
  REPORT,
  CLIENT,
  DOCTOR,
  NURSE
}
