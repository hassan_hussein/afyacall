package com.openldr.core.repository;

import com.openldr.core.domain.ProductCategory;
import com.openldr.core.repository.mapper.ProductCategoryMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 3/15/16.
 */
@Repository
@NoArgsConstructor
public class ProductCategoryRepository {

    @Autowired
    private ProductCategoryMapper mapper;

    @Autowired
    public ProductCategoryRepository(ProductCategoryMapper mapper) {
        this.mapper = mapper;
    }

    public Integer insert(ProductCategory productCategory) {
        return mapper.insert(productCategory);
    }

    public Integer update(ProductCategory productCategory) {
        return mapper.update(productCategory);
    }

    public ProductCategory getById(Long id) {
        return mapper.getById(id);
    }

    public List<ProductCategory> getAll() {
        return mapper.getAll();
    }
}
