/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2013 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

package com.openldr.core.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;


/**
 * Facility represents a real world Facility on ground. Defines the contract for creation/upload, eg. the mandatory fields, field type
 * and header for upload. Also provides utility methods to operate on facility/facility lists validation, filtering
 * facilities for active products etc.
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize(include = NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Facility extends BaseModel {
  private String code;

  private String name;

  private String description;

  private String gln;

  private String mainPhone;

  private String fax;

  private String address1;

  private String address2;

  private GeographicZone geographicZone;

  private FacilityType facilityType;

  private Long catchmentPopulation;

  private Double latitude;

  private Double longitude;

  private Double altitude;

  private FacilityOperator operatedBy;

  private Double coldStorageGrossCapacity;

  private Double coldStorageNetCapacity;

  private Boolean suppliesOthers;

  private Boolean sdp;

  private Boolean hasElectricity;

  private Boolean online;

  private Boolean hasElectronicSCC;

  private Boolean hasElectronicDAR;

  private Boolean active;

/*
  @JsonDeserialize(using = DateDeserializer.class)
*/
  private Date goLiveDate;

 /* @JsonDeserialize(using = DateDeserializer.class)*/
  private Date goDownDate;

  private Boolean satellite;

  private Long parentFacilityId;

  private String comment;

  private Boolean enabled;

  private Boolean virtualFacility = false;

/*
  private PriceSchedule priceSchedule;

  private List<ELMISInterfaceFacilityMapping> interfaceMappings = new ArrayList<>();

  //TODO : change supportedPrograms to programsSupported
  List<ProgramSupported> supportedPrograms = new ArrayList<>();
*/

  public Facility(Long id) {
    this.id = id;
  }

  public Facility(Long id, String code, String name, FacilityOperator operatedBy, GeographicZone geographicZone, FacilityType facilityType, boolean virtualFacility) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.operatedBy = operatedBy;
    this.geographicZone = geographicZone;
    this.facilityType = facilityType;
    this.virtualFacility = virtualFacility;
  }

  public Facility(Long id, boolean enabled, boolean active, Long modifiedBy) {
    this.id = id;
    this.enabled = enabled;
    this.active = active;
    this.modifiedBy = modifiedBy;
  }

 /* @Override
  public boolean equals(Object o) {

    return reflectionEquals(this, o, false, Facility.class, "supportedPrograms", "geographicZone") &&
      reflectionEquals(this.geographicZone, ((Facility) o).geographicZone, false, GeographicZone.class, "parent", "level");
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(17, 37, this, false, Facility.class, "supportedPrograms", "geographicZone");
  }
*/
  public Facility basicInformation() {
    return new Facility(id, code, name, operatedBy, geographicZone, facilityType, virtualFacility);
  }

  public static Facility createFacilityToBeDeleted(Long facilityId, Long modifiedBy) {
    return new Facility(facilityId, false, false, modifiedBy);
  }

  public static Facility createFacilityToBeRestored(Long facilityId, Long modifiedBy) {
    return new Facility(facilityId, true, false, modifiedBy);
  }

  public void validate() {
  /*  for (ProgramSupported programSupported : supportedPrograms) {
      programSupported.isValid();
    }*/
  }

  public boolean isValid(Facility parentFacility) {
    if (parentFacility == null)
      return active && enabled;

    return active && enabled && parentFacility.active && parentFacility.enabled;
  }

  @SuppressWarnings("unused")
  public String getStringGoLiveDate() throws ParseException {
    return this.goLiveDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.goLiveDate);
  }

  @SuppressWarnings("unused")
  public String getStringGoDownDate() throws ParseException {
    return this.goDownDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(this.goDownDate);
  }

 /* @JsonIgnore
  public Double getWhoRatioFor(String productCode) {
    return this.getSupportedPrograms().get(0).getWhoRatioFor(productCode);
  }*/

 /* public static List<Facility> filterForActiveProducts(List<Facility> facilities) {
    for (Facility facility : facilities) {
      for (ProgramSupported programSupported : facility.getSupportedPrograms()) {
        programSupported.setProgramProducts(FacilityProgramProduct.filterActiveProducts(programSupported.getProgramProducts()));
      }
    }
    return facilities;
  }

  @JsonIgnore
  public Integer getPackSizeFor(String productCode) {
    return this.getSupportedPrograms().get(0).getPackSizeFor(productCode);
  }*/
}
