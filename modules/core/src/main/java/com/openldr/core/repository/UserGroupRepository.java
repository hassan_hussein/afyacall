package com.openldr.core.repository;

import com.openldr.core.domain.UserGroup;
import com.openldr.core.repository.mapper.UserGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hassan on 7/30/16.
 */
@Repository
public class UserGroupRepository {

    @Autowired
    private UserGroupMapper mapper;

    public List<UserGroup> getAll() {
        return mapper.getAll();
    }
}
