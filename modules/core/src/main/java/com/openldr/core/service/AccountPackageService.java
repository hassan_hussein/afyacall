package com.openldr.core.service;

import com.openldr.core.domain.AccountPackage;
import com.openldr.core.repository.AccountPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hassan on 7/9/16.
 */
@Service
public class AccountPackageService {

    @Autowired
    AccountPackageRepository repository;

    public void Insert(AccountPackage accountPackage) {
        if (accountPackage.getId() == null)
            repository.Insert(accountPackage);
        else
            repository.update(accountPackage);
    }

    public List<AccountPackage> getAll() {
        return repository.getAll();
    }

    public AccountPackage getAllById(Long id) {
        return repository.getAllById(id);
    }


    public void save(AccountPackage accountPackage) {
        repository.save(accountPackage);
    }

    public Long delete(Long id) {
        return repository.delete(id);
    }
}

