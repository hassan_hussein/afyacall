package com.openldr.core.service;

import lombok.NoArgsConstructor;
import  com.openldr.core.domain.Right;
import  com.openldr.core.repository.RightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Exposes the services for handling Right entity.
 */

@Service
@NoArgsConstructor
public class RightService {

  @Autowired
  private RightRepository rightRepository;

  public void insertRight(Right right) {
    right.setName(right.getName().replaceAll(" ","_").toUpperCase());
    rightRepository.insertRight(right);

  }

  public void save(Right right) {
    rightRepository.save(right);
  }
  public void update(Right right) {
    right.setName(right.getName().replaceAll(" ","_").toUpperCase());
    rightRepository.updateRight(right);
  }

  public Boolean hasReportingRight(Long userId) {
    return rightRepository.hasReportingRight(userId);
  }

  public List<Right> getAll(){
    return rightRepository.getAll();
  }

  public Right getByName(String name) {

    return rightRepository.getByName(name);
  }

  public Right delete(String name) {
    return rightRepository.delete(name);
  }
}