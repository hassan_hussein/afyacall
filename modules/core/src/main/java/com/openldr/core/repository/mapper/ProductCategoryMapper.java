package com.openldr.core.repository.mapper;

import com.openldr.core.domain.ProductCategory;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chrispinus on 3/14/16.
 */
@Repository
public interface ProductCategoryMapper {

    @Insert({"INSERT INTO product_categories",
            "(code, name, displayOrder, createdBy, modifiedBy, modifiedDate)",
            "VALUES",
            "(#{code}, #{name}, #{displayOrder}, #{createdBy}, #{modifiedBy}, COALESCE(#{modifiedDate}, NOW()))"})
    @Options(useGeneratedKeys = true)
    Integer insert(ProductCategory productCategory);

    @Update("UPDATE product_categories SET " +
            "code=#{code}," +
            "name=#{name}," +
            "displayOrder=#{displayOrder}," +
            "modifiedBy=#{modifiedBy}, " +
            "modifiedDate= COALESCE(#{modifiedDate}, NOW()) WHERE id=#{id}")
    Integer update(ProductCategory productCategory);

    @Select("SELECT * FROM product_categories WHERE id=#{id}")
    ProductCategory getById(@Param("id") Long id);

    @Select("SELECT * FROM product_categories")
    List<ProductCategory> getAll();


}
