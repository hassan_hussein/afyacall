package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.UserCVFieldValue;
import com.openldr.medical_record.repository.mapper.UserCVFieldValueMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserCVFieldValueRepository {

    @Autowired
    private UserCVFieldValueMapper mapper;

    public Integer insert(UserCVFieldValue userCVFieldValue) {
        return mapper.insert(userCVFieldValue);
    }

    public Integer update(UserCVFieldValue userCVFieldValue) {
        return mapper.update(userCVFieldValue);
    }

    public UserCVFieldValue getById(Long id) {
        return mapper.getById(id);
    }

    public List<UserCVFieldValue> getByCVId(Long userCVId) {
        return mapper.getByCVId(userCVId);
    }
}
