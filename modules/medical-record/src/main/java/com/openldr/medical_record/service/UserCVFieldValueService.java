package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.UserCVFieldValue;
import com.openldr.medical_record.repository.UserCVFieldValueRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class UserCVFieldValueService {

    @Autowired
    private UserCVFieldValueRepository repository;

    public Integer insert(UserCVFieldValue userCVFieldValue) {
        return repository.insert(userCVFieldValue);
    }

    public Integer update(UserCVFieldValue userCVFieldValue) {
        return repository.update(userCVFieldValue);
    }

    public UserCVFieldValue getById(Long id) {
        return repository.getById(id);
    }

    public List<UserCVFieldValue> getByCVId(Long userCVId) {
        return repository.getByCVId(userCVId);
    }

    public Integer save(UserCVFieldValue userCVFieldValue) {
        if (userCVFieldValue.getId() == null) {
            return insert(userCVFieldValue);
        } else {
            return update(userCVFieldValue);
        }
    }
}
