package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.UserCVTypeField;
import com.openldr.medical_record.repository.mapper.UserCVTypeFieldMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserCVTypeFieldRepository {

    @Autowired
    private UserCVTypeFieldMapper mapper;

    public Integer insert(UserCVTypeField userCVTypeField) {
        return mapper.insert(userCVTypeField);
    }

    public Integer update(UserCVTypeField userCVTypeField) {
        return mapper.update(userCVTypeField);
    }

    public List<UserCVTypeField> getByCVType(Long id) {
        return mapper.getByCVType(id);
    }

    public UserCVTypeField getById(Long id) {
        return mapper.getById(id);
    }
}
