package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.MedicalRecordTypeFieldGroup;
import com.openldr.medical_record.repository.MedicalRecordTypeFieldGroupRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class MedicalRecordTypeFieldGroupService {

    @Autowired
    private MedicalRecordTypeFieldGroupRepository repository;

    public Integer insert(MedicalRecordTypeFieldGroup medicalRecordTypeFieldGroup) {
        return repository.insert(medicalRecordTypeFieldGroup);
    }

    public Integer update(MedicalRecordTypeFieldGroup medicalRecordTypeFieldGroup) {
        return repository.update(medicalRecordTypeFieldGroup);
    }

    public MedicalRecordTypeFieldGroup getById(Long id) {
        return repository.getById(id);
    }

    public List<MedicalRecordTypeFieldGroup> getByRecordType(Long id) {
        return repository.getByRecordType(id);
    }

    public Integer save(MedicalRecordTypeFieldGroup medicalRecordTypeFieldGroup) {
        if (medicalRecordTypeFieldGroup.getId() == null) {
            return insert(medicalRecordTypeFieldGroup);
        } else {
            return update(medicalRecordTypeFieldGroup);
        }
    }
}
