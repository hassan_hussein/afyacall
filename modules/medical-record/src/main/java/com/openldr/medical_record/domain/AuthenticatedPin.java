package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.Data;

/**
 * Created by chrispinus on 8/2/16.
 */
@Data
public class AuthenticatedPin extends BaseModel {
    private String pin;
    private String uniqueId;

}
