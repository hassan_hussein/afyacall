package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.MedicalRecord;
import com.openldr.medical_record.domain.MedicalRecordFieldValue;
import com.openldr.medical_record.repository.MedicalRecordRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@NoArgsConstructor
public class MedicalRecordService {

    @Autowired
    private MedicalRecordRepository repository;

    @Autowired
    private MedicalRecordFieldValueService fieldValueService;

    public Integer insert(MedicalRecord medicalRecord) {
        return repository.insert(medicalRecord);
    }

    public Integer update(MedicalRecord medicalRecord) {
        return repository.update(medicalRecord);
    }

    public MedicalRecord getById(Long id) {
        return repository.getById(id);
    }

    public List<MedicalRecord> getByUserId(Long userId) {
        return repository.getByUserId(userId);
    }

    @Transactional
    public Integer save(MedicalRecord medicalRecord) {

        if (medicalRecord.getId() == null) {
            Integer result = insert(medicalRecord);
            System.out.println("save medical record");
            saveFieldValues(medicalRecord);
            System.out.println("save values");
            return result;

        } else {
            Integer result = update(medicalRecord);
            saveFieldValues(medicalRecord);
            return result;
        }
    }

    private void saveFieldValues(MedicalRecord medicalRecord) {
//        System.out.println(medicalRecord.getFieldValues().toString());
        for (MedicalRecordFieldValue fieldValue : medicalRecord.getFieldValues()) {
            fieldValue.setMedicalRecordId(medicalRecord.getId());
            Long fieldId = (fieldValue.getMedicalRecordField() != null) ? fieldValue.getMedicalRecordField().getId() : fieldValue.getMedicalRecordFieldId();
            fieldValue.setMedicalRecordFieldId(fieldId);
            System.out.println(fieldValue);
            fieldValueService.save(fieldValue);
        }
    }

    public List<MedicalRecord> getByUserAndType(Long userId, Long recordTypeId) {
        return repository.getByUserAndType(userId, recordTypeId);
    }
}
