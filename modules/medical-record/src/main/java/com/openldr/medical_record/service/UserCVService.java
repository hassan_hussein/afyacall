package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.UserCV;
import com.openldr.medical_record.domain.UserCVFieldValue;
import com.openldr.medical_record.repository.UserCVRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@NoArgsConstructor
public class UserCVService {

    @Autowired
    private UserCVRepository repository;

    @Autowired
    private UserCVFieldValueService fieldValueService;

    public Integer insert(UserCV userCV) {
        return repository.insert(userCV);
    }

    public Integer update(UserCV userCV) {
        return repository.update(userCV);
    }

    public UserCV getById(Long id) {
        return repository.getById(id);
    }

    public List<UserCV> getByUserId(Long userId) {
        return repository.getByUserId(userId);
    }

    @Transactional
    public Integer save(UserCV userCV) {
        if (userCV.getId() == null) {
            Integer result = insert(userCV);
            saveFieldValues(userCV);
            return result;

        } else {
            Integer result = update(userCV);
            saveFieldValues(userCV);
            return result;
        }
    }

    private void saveFieldValues(UserCV userCV) {
        for (UserCVFieldValue userCVFieldValue : userCV.getFieldValues()) {
            userCVFieldValue.setUserCVId(userCV.getId());
            Long fieldId = (userCVFieldValue.getUserCVField() != null) ? userCVFieldValue.getUserCVField().getId() : userCVFieldValue.getUserCVFieldId();
            userCVFieldValue.setUserCVFieldId(fieldId);
            fieldValueService.save(userCVFieldValue);
        }
    }

    public List<UserCV> getByUserAndType(Long userId, Long recordTypeId) {
        return repository.getByUserAndType(userId, recordTypeId);
    }
}
