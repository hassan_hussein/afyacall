package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.UserCVTypeField;
import com.openldr.medical_record.repository.UserCVTypeFieldRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class UserCVTypeFieldService {

    @Autowired
    private UserCVTypeFieldRepository repository;

    public Integer insert(UserCVTypeField userCVTypeField) {
        return repository.insert(userCVTypeField);
    }

    public Integer update(UserCVTypeField userCVTypeField) {
        return repository.update(userCVTypeField);
    }

    public UserCVTypeField getById(Long id) {
        return repository.getById(id);
    }

    public List<UserCVTypeField> getByCVType(Long id) {
        return repository.getByCVType(id);
    }

    public Integer save(UserCVTypeField userCVTypeField) {
        if (userCVTypeField.getId() == null) {
            return insert(userCVTypeField);
        } else {
            return update(userCVTypeField);
        }
    }
}
