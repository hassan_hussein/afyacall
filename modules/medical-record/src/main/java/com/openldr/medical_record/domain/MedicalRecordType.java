

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MedicalRecordType extends BaseModel {

    private String name;
    private String code;
    private int displayOrder;
    private List<MedicalRecordTypeFieldGroup> fieldGroups;

}
