package com.openldr.medical_record.repository.mapper;

import com.openldr.core.domain.User;
import com.openldr.core.domain.UserGeographicZone;
import com.openldr.medical_record.domain.AuthenticatedPin;
import com.openldr.medical_record.domain.Consultation;
import com.openldr.medical_record.domain.MedicalRecord;
import com.openldr.medical_record.dto.ConsultationRecordDTO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultationMapper {

    @Insert({"INSERT INTO consultations(clientid,agentid,status,createdby, createddate) " +
            "values(#{clientId},#{agentId},#{status},#{createdBy},NOW())"})
    @Options(useGeneratedKeys = true)
    Integer initiateAgentConsultation(Consultation consultation);

    @Select("Select u.* from users u join account_members m on u.id=m.userid where  m.memberpincode=#{pin} LIMIT 1 ")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "geographicZone", column = "geographicZoneId", javaType = UserGeographicZone.class,
                    one = @One(select = "com.openldr.core.repository.mapper.GeographicZoneMapper.getUserGeographicZone"))

    })
    User getUserByPIN(@Param("pin") String pin);

    @Select("Select * from users where extensionnumber=#{extension}::INT LIMIT 1 ")
    User getUserExtension(@Param("extension") String extension);


    @Update("UPDATE medical_records SET userid=#{userId},recordtypeid=#{recordTypeId} WHERE id=#{id}")
    Integer update(MedicalRecord medicalRecord);

    @Select("Select * from medical_records where id=#{id} ")
    MedicalRecord getById(@Param("id") Long id);

    @Select("Select * from medical_records where userid=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getByUserId(@Param("userId") Long userId);

    @Select("Select * from medical_records where userid=#{userId} and recordtypeid=#{recordTypeId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "fieldValues", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper.getByMedicalRecordId"))
    })
    List<MedicalRecord> getByUserAndType(@Param("userId") Long userId, @Param("recordTypeId") Long recordTypeId);

    @Update("UPDATE consultations set doctorid=#{doctorId}, status='DOCTOR_QUEUE' where id=#{consultationId}")
    Integer forwardConsultation(@Param("consultationId") Long consultationId, @Param("doctorId") Long doctorId);

    @Update("UPDATE consultations set status='DOCTOR_FINALIZED', consultationdate=NOW() where id=#{consultationId}")
    Integer saveConsultation(@Param("consultationId") Long consultationId);

    @Select("Select * from consultations  where id=#{consultationId} LIMIT 1")
    @Results(value = {
            @Result(property = "clientId", column = "clientId"),
            @Result(property = "client", column = "clientid", javaType = User.class,
                    one = @One(select = "com.openldr.core.repository.mapper.UserMapper.getById")),
            @Result(property = "id", column = "id"),
            @Result(property = "medicalRecords", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordMapper.getByConsultationId")),
            @Result(property = "medicalHistory", javaType = List.class, column = "id",
                    many = @Many(select = "com.openldr.medical_record.repository.mapper.MedicalRecordMapper.getHistory"))

    })
    ConsultationRecordDTO getConsultationById(@Param("consultationId") Long consultationId);

    @Select("Select * from consultations  where id=#{consultationId} LIMIT 1")
    Consultation getByIdLess(@Param("consultationId") Long consultationId);

    @Select("Select * from consultations Where doctorid=#{doctorId} and status='DOCTOR_QUEUE'")
    @Results(value = {
            @Result(property = "clientId", column = "clientId"),
            @Result(property = "client", column = "clientid", javaType = User.class,
                    one = @One(select = "com.openldr.core.repository.mapper.UserMapper.getById"))

    })
    List<ConsultationRecordDTO> getQueueConsultation(@Param("doctorId") Long doctorId);

    @Insert("INSERT INTO call_logs(pin, uniqueId) VALUES(#{pin},#{uniqueId})")
    @Options(useGeneratedKeys = true)
    void savePIN(AuthenticatedPin authenticatedPin);

    @Select("Select * from call_logs where uniqueid=#{uniqueId} order by createddate desc limit 1")
    AuthenticatedPin getPIN(@Param("uniqueId") String uniqueId);
}
