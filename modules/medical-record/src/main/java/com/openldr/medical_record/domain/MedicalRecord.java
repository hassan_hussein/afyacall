

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import com.openldr.core.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MedicalRecord extends BaseModel {

    private Long userId;

    private Long recordTypeId;

    private Long consultationId;

    private List<MedicalRecordFieldValue> fieldValues;

    private User doctor;

    private Consultation consultation;

}
