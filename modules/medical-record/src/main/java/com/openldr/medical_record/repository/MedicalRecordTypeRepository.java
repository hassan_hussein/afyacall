package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.MedicalRecordType;
import com.openldr.medical_record.repository.mapper.MedicalRecordTypeMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class MedicalRecordTypeRepository {

    @Autowired
    private MedicalRecordTypeMapper mapper;

    public Integer insert(MedicalRecordType medicalRecordType) {
        return mapper.insert(medicalRecordType);
    }

    public Integer update(MedicalRecordType medicalRecordType) {
        return mapper.update(medicalRecordType);
    }

    public MedicalRecordType getById(Long id) {
        return mapper.getById(id);
    }

    public List<MedicalRecordType> getAll() {
        return mapper.getAll();
    }
}
