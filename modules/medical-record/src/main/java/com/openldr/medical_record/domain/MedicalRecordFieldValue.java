

package com.openldr.medical_record.domain;

import com.openldr.core.domain.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MedicalRecordFieldValue extends BaseModel {

    private Long medicalRecordId;

    private MedicalRecord medicalRecord;

    private Long medicalRecordFieldId;

    private MedicalRecordTypeField medicalRecordField;

    private String fieldValue;

    private String fieldType;

    private String group;

}
