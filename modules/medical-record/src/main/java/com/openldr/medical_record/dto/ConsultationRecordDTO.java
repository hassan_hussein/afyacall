

package com.openldr.medical_record.dto;

import com.openldr.core.domain.User;
import com.openldr.medical_record.domain.Consultation;
import com.openldr.medical_record.domain.MedicalRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ConsultationRecordDTO extends Consultation {

    private User client;

    private List<MedicalRecord> medicalRecords;

    private List<MedicalRecord> medicalHistory;

}
