package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.MedicalRecordType;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldGroup;
import com.openldr.medical_record.repository.MedicalRecordTypeRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@NoArgsConstructor
public class MedicalRecordTypeService {

    @Autowired
    private MedicalRecordTypeRepository repository;

    @Autowired
    private MedicalRecordTypeFieldService fieldService;

    @Autowired
    private MedicalRecordTypeFieldGroupService groupService;

    public Integer insert(MedicalRecordType medicalRecordType) {
        return repository.insert(medicalRecordType);
    }

    public Integer update(MedicalRecordType medicalRecordType) {
        return repository.update(medicalRecordType);
    }

    public MedicalRecordType getById(Long id) {
        return repository.getById(id);
    }

    public List<MedicalRecordType> getAll() {
        return repository.getAll();
    }

    @Transactional
    public Integer save(MedicalRecordType medicalRecordType) {
        if (medicalRecordType.getId() == null) {
            Integer result = insert(medicalRecordType);
            saveGroups(medicalRecordType);
            return result;
        } else {
            Integer result = update(medicalRecordType);
            saveGroups(medicalRecordType);
            return result;
        }
    }

    private void saveGroups(MedicalRecordType medicalRecordType) {
        for (MedicalRecordTypeFieldGroup group : medicalRecordType.getFieldGroups()) {
            group.setMedicalRecordTypeId(medicalRecordType.getId());
            groupService.save(group);
            saveFields(group);
        }
    }

    private void saveFields(MedicalRecordTypeFieldGroup group) {
        for (MedicalRecordTypeField field : group.getFields()) {
            field.setMedicalRecordTypeId(group.getMedicalRecordTypeId());
            field.setFieldGroupId(group.getId());
            fieldService.save(field);
        }
    }
}
