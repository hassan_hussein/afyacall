package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.UserCVType;
import com.openldr.medical_record.repository.mapper.UserCVTypeMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserCVTypeRepository {

    @Autowired
    private UserCVTypeMapper mapper;

    public Integer insert(UserCVType userCVType) {
        return mapper.insert(userCVType);
    }

    public Integer update(UserCVType userCVType) {
        return mapper.update(userCVType);
    }

    public UserCVType getById(Long id) {
        return mapper.getById(id);
    }

    public List<UserCVType> getAll() {
        return mapper.getAll();
    }
}
