/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.UserCVTypeField;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCVTypeFieldMapper {

    @Insert({"INSERT INTO user_cv_type_fields(usercvtypeid, fieldname, fieldtype) " +
            "values(#{userCVTypeId},#{fieldName},#{fieldType})"})
    @Options(useGeneratedKeys = true)
    Integer insert(UserCVTypeField userCVTypeField);

    @Update("UPDATE user_cv_type_fields SET fieldname=#{fieldName}, fieldtype=#{fieldType} WHERE id=#{id}")
    Integer update(UserCVTypeField userCVTypeField);

    @Select("Select * from user_cv_type_fields Where usercvtypeid=#{id} ")
    List<UserCVTypeField> getByCVType(@Param("id") Long id);

    @Select("Select * from user_cv_type_fields where id=#{id}")
    UserCVTypeField getById(@Param("id") Long id);

}
