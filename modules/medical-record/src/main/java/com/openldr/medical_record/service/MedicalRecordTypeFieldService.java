package com.openldr.medical_record.service;

import com.openldr.medical_record.domain.FieldType;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldOption;
import com.openldr.medical_record.repository.MedicalRecordTypeFieldRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class MedicalRecordTypeFieldService {

    @Autowired
    private MedicalRecordTypeFieldRepository repository;

    public Integer insert(MedicalRecordTypeField medicalRecordTypeField) {
        return repository.insert(medicalRecordTypeField);
    }

    public Integer update(MedicalRecordTypeField medicalRecordTypeField) {
        return repository.update(medicalRecordTypeField);
    }

    public MedicalRecordTypeField getById(Long id) {
        return repository.getById(id);
    }

    public List<MedicalRecordTypeField> getByRecordType(Long id) {
        return repository.getByRecordType(id);
    }

    public void save(MedicalRecordTypeField medicalRecordTypeField) {
        if (medicalRecordTypeField.getId() == null) {
            insert(medicalRecordTypeField);
            saveOptions(medicalRecordTypeField);
        } else {
            update(medicalRecordTypeField);
            saveOptions(medicalRecordTypeField);
        }
    }

    private void saveOptions(MedicalRecordTypeField medicalRecordTypeField) {
        if (medicalRecordTypeField.getFieldOptions() != null) {
            for (MedicalRecordTypeFieldOption option : medicalRecordTypeField.getFieldOptions()) {
                if (option.getId() == null) {
                    option.setFieldId(medicalRecordTypeField.getId());
                    repository.saveOption(option);
                } else {
                    repository.updateOption(option);
                }
            }
        }
    }

    public List<FieldType> getFieldTypes() {
        return repository.getFieldTypes();
    }
}
