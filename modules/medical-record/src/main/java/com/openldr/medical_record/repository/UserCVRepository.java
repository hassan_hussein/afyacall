package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.UserCV;
import com.openldr.medical_record.repository.mapper.UserCVMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserCVRepository {

    @Autowired
    private UserCVMapper mapper;

    public Integer insert(UserCV userCV) {
        return mapper.insert(userCV);
    }

    public Integer update(UserCV userCV) {
        return mapper.update(userCV);
    }

    public UserCV getById(Long id) {
        return mapper.getById(id);
    }

    public List<UserCV> getByUserId(Long userId) {
        return mapper.getByUserId(userId);
    }

    public List<UserCV> getByUserAndType(Long userId, Long recordTypeId) {
        return mapper.getByUserAndType(userId, recordTypeId);
    }
}
