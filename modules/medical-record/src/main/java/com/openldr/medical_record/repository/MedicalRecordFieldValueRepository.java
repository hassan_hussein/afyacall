package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.MedicalRecordFieldValue;
import com.openldr.medical_record.repository.mapper.MedicalRecordFieldValueMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class MedicalRecordFieldValueRepository {

    @Autowired
    private MedicalRecordFieldValueMapper mapper;

    public Integer insert(MedicalRecordFieldValue medicalRecordFieldValue) {
        return mapper.insert(medicalRecordFieldValue);
    }

    public Integer update(MedicalRecordFieldValue medicalRecordFieldValue) {
        return mapper.update(medicalRecordFieldValue);
    }

    public MedicalRecordFieldValue getById(Long id) {
        return mapper.getById(id);
    }

    public List<MedicalRecordFieldValue> getByMedicalRecordId(Long medicalRecordId) {
        return mapper.getByMedicalRecordId(medicalRecordId);
    }
}
