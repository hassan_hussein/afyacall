/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.MedicalRecordFieldValue;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalRecordFieldValueMapper {

    @Insert({"INSERT INTO medical_record_field_value(medicalrecordid,medicalrecordfieldid,fieldvalue)" +
            " values(#{medicalRecordId},#{medicalRecordFieldId},#{fieldValue})"})
    @Options(useGeneratedKeys = true)
    Integer insert(MedicalRecordFieldValue fieldValue);

    @Update("UPDATE medical_record_field_value SET fieldvalue=#{fieldValue} WHERE id=#{id}")
    Integer update(MedicalRecordFieldValue fieldValue);

    @Select("Select fv.*,g.name as group, f.fieldtype as fieldType from medical_record_field_value fv " +
            " join medical_record_type_fields f on f.id=fv.medicalrecordfieldid " +
            " join medical_record_type_field_groups g on g.id=f.fieldgroupid" +
            " where id=#{id} ")
    MedicalRecordFieldValue getById(@Param("id") Long id);

    @Select("Select fv.*,g.name as group,f.fieldtype as fieldType from medical_record_field_value fv" +
            " join medical_record_type_fields f on f.id=fv.medicalrecordfieldid " +
            " join medical_record_type_field_groups g on g.id=f.fieldgroupid" +
            " where medicalrecordid=#{medicalRecordId}")
    @Results({@Result(property = "medicalRecordFieldId", column = "medicalrecordfieldid"),
            @Result(property = "medicalRecordField", column = "medicalrecordfieldid", javaType = MedicalRecordTypeField.class,
                    one = @One(select = "getFieldById"))})
    List<MedicalRecordFieldValue> getByMedicalRecordId(@Param("medicalRecordId") Long medicalRecordId);


    @Select("Select * from medical_record_type_fields where id=#{id}")
    MedicalRecordTypeField getFieldById(@Param("id") Long id);


}
