package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.FieldType;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldOption;
import com.openldr.medical_record.repository.mapper.MedicalRecordTypeFieldMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class MedicalRecordTypeFieldRepository {

    @Autowired
    private MedicalRecordTypeFieldMapper mapper;

    public Integer insert(MedicalRecordTypeField medicalRecordTypeField) {
        return mapper.insert(medicalRecordTypeField);
    }

    public Integer update(MedicalRecordTypeField medicalRecordTypeField) {
        return mapper.update(medicalRecordTypeField);
    }

    public List<MedicalRecordTypeField> getByRecordType(Long id) {
        return mapper.getByGroupType(id);
    }

    public MedicalRecordTypeField getById(Long id) {
        return mapper.getById(id);
    }

    public List<FieldType> getFieldTypes() {
        return mapper.getFieldTypes();
    }

    public void saveOption(MedicalRecordTypeFieldOption option) {
        mapper.insertOptions(option);
    }

    public void updateOption(MedicalRecordTypeFieldOption option) {
        mapper.updateOption(option);
    }
}
