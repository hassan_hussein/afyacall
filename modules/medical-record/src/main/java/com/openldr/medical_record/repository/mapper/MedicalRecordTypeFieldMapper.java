/*
 * Electronic Logistics Management Information System (eLMIS) is a supply chain management system for health commodities in a developing country setting.
 *
 * Copyright (C) 2015  John Snow, Inc (JSI). This program was produced for the U.S. Agency for International Development (USAID). It was prepared under the USAID | DELIVER PROJECT, Task Order 4.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.openldr.medical_record.repository.mapper;

import com.openldr.medical_record.domain.FieldType;
import com.openldr.medical_record.domain.MedicalRecordTypeField;
import com.openldr.medical_record.domain.MedicalRecordTypeFieldOption;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalRecordTypeFieldMapper {

    @Insert({"INSERT INTO medical_record_type_fields(medicalrecordtypeid,fieldgroupid, fieldname, fieldtype,displayorder,style) " +
            "values(#{medicalRecordTypeId},#{fieldGroupId}, #{fieldName},#{fieldType},#{displayOrder},#{style})"})
    @Options(useGeneratedKeys = true)
    Integer insert(MedicalRecordTypeField medicalRecordTypeField);

    @Update("UPDATE medical_record_type_fields SET fieldname=#{fieldName},fieldgroupid=#{fieldGroupId}, fieldtype=#{fieldType},displayOrder=#{displayOrder},style=#{style} WHERE id=#{id}")
    Integer update(MedicalRecordTypeField medicalRecordTypeField);

    @Select("Select f.*,g.name as group from medical_record_type_fields f " +
            " left join medical_record_type_field_groups g on g.id=f.fieldgroupid Where fieldgroupid=#{id} order by displayorder ")
    @Results({@Result(property = "id", column = "id"),
            @Result(property = "fieldOptions", javaType = List.class, column = "id",
                    many = @Many(select = "getOptionsByFieldId"))})
    List<MedicalRecordTypeField> getByGroupType(@Param("id") Long id);

    @Select("Select f.*,g.name as group from medical_record_type_fields f" +
            " left join medical_record_type_field_groups g on g.id=f.fieldgroupid  where id=#{id} order by displayorder")
    MedicalRecordTypeField getById(@Param("id") Long id);

    @Select("Select name from field_types")
    List<FieldType> getFieldTypes();

    @Select("Select * from medical_record_type_field_options WHERE fieldid=#{fieldId}")
    List<MedicalRecordTypeFieldOption> getOptionsByFieldId(@Param("fieldId") Long fieldId);

    @Insert({"INSERT INTO medical_record_type_field_options(fieldid,name) " +
            "values(#{fieldId},#{name})"})
    @Options(useGeneratedKeys = true)
    Integer insertOptions(MedicalRecordTypeFieldOption fieldOption);

    @Update("UPDATE medical_record_type_field_options SET name=#{name} WHERE id=#{id}")
    void updateOption(MedicalRecordTypeFieldOption option);
}
