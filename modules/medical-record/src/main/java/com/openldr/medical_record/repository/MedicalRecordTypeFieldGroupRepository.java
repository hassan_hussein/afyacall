package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.MedicalRecordTypeFieldGroup;
import com.openldr.medical_record.repository.mapper.MedicalRecordTypeFieldGroupMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class MedicalRecordTypeFieldGroupRepository {

    @Autowired
    private MedicalRecordTypeFieldGroupMapper mapper;

    public Integer insert(MedicalRecordTypeFieldGroup fieldGroup) {
        return mapper.insert(fieldGroup);
    }

    public Integer update(MedicalRecordTypeFieldGroup fieldGroup) {
        return mapper.update(fieldGroup);
    }

    public List<MedicalRecordTypeFieldGroup> getByRecordType(Long id) {
        return mapper.getByRecordType(id);
    }

    public MedicalRecordTypeFieldGroup getById(Long id) {
        return mapper.getById(id);
    }
}
