package com.openldr.medical_record.repository;

import com.openldr.core.domain.User;
import com.openldr.medical_record.domain.AuthenticatedPin;
import com.openldr.medical_record.domain.Consultation;
import com.openldr.medical_record.dto.ConsultationRecordDTO;
import com.openldr.medical_record.repository.mapper.ConsultationMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class ConsultationRepository {

    @Autowired
    private ConsultationMapper mapper;


    public void initiateAgentConsultation(Consultation consultation) {
        mapper.initiateAgentConsultation(consultation);
    }

    public User getUserByPIN(String cellPhone) {
        return mapper.getUserByPIN(cellPhone);
    }

    public User getUserByExtension(String extension) {
        return mapper.getUserExtension(extension);
    }

    public void forwardConsultation(Long consultationId, Long doctorId) {
        mapper.forwardConsultation(consultationId, doctorId);
    }

    public void saveConsultation(Long consultationId) {
        mapper.saveConsultation(consultationId);
    }

    public ConsultationRecordDTO getConsultationById(Long consultationId) {
        return mapper.getConsultationById(consultationId);
    }

    public List<ConsultationRecordDTO> getQueueConsultation(Long doctorId) {
        return mapper.getQueueConsultation(doctorId);
    }

    public void savePIN(AuthenticatedPin authenticatedPin) {
        mapper.savePIN(authenticatedPin);
    }

    public AuthenticatedPin getPIN(String uniqueId) {
        return mapper.getPIN(uniqueId);
    }
}
