package com.openldr.medical_record.repository;

import com.openldr.medical_record.domain.MedicalRecord;
import com.openldr.medical_record.repository.mapper.MedicalRecordMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@NoArgsConstructor
public class MedicalRecordRepository {

    @Autowired
    private MedicalRecordMapper mapper;

    public Integer insert(MedicalRecord medicalRecord) {
        return mapper.insert(medicalRecord);
    }

    public Integer update(MedicalRecord medicalRecord) {
        return mapper.update(medicalRecord);
    }

    public MedicalRecord getById(Long id) {
        return mapper.getById(id);
    }

    public List<MedicalRecord> getByUserId(Long userId) {
        return mapper.getByUserId(userId);
    }

    public List<MedicalRecord> getByUserAndType(Long userId, Long recordTypeId) {
        return mapper.getByUserAndType(userId, recordTypeId);
    }
}
